# Your Great Library Portal

## Generating the API
The API is generated using the [OpenAPI Generator](https://github.com/OpenAPITools/openapi-generator). It runs with minimal adjustment when generated like this:
```
java -jar openapi-generator-cli-3.3.4.jar generate -i https://greatlibrary.orbitgames.nl/api-spec/your-great-library-api-spec.yml -g typescript-angular -o your-great-library-angular-api
```

## Building the portal
`ng build` seems to be having troubles with limited memory, so it is advisable to increase the memory limit:
```
node --max-old-space-size=12000 node_modules\@angular\cli\bin\ng build --prod
```