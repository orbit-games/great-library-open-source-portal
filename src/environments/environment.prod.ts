/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
export const environment = {
  production: true,
  apiBaseUri: '/api/rest/v1',
  cacheTimeout: 300,
  displayDateFormat: 'DD-MM-YYYY',
  displayDateTimeFormat: 'DD-MM-YYYY HH:mm:ss',
  displayTimeFormat: 'HH:mm:ss',
};
