import {ExtraOptions, RouterModule, Routes} from '@angular/router';
import {NgModule} from '@angular/core';
import {NbAuthComponent, NbLogoutComponent} from '@nebular/auth';
import {GlAuthGuard} from './auth/gl-auth-guard.service';
import {GlLoginComponent} from './auth/gl-login-page/gl-login.component';
import {GlRequestPasswordComponent} from './auth/gl-request-password/gl-request-password.component';
import {GlResetPasswordComponent} from './auth/gl-reset-password/gl-reset-password.component';
import {GlRegisterComponent} from './auth/gl-register/gl-register.component';

const routes: Routes = [
  { path: 'pages', loadChildren: 'app/pages/pages.module#PagesModule', canActivate: [GlAuthGuard] },
  {
    path: 'auth',
    component: NbAuthComponent,
    children: [
      {
        path: '',
        component: GlLoginComponent,
      },
      {
        path: 'login',
        component: GlLoginComponent,
      },
      {
        path: 'register',
        component: GlRegisterComponent,
      },
      {
        path: 'logout',
        component: NbLogoutComponent,
      },
      {
        path: 'request-password',
        component: GlRequestPasswordComponent,
      },
      {
        path: 'reset-password',
        component: GlResetPasswordComponent,
      },
    ],
  },
  { path: '', redirectTo: 'pages', pathMatch: 'full' },
  { path: '**', redirectTo: 'pages' },
];

const config: ExtraOptions = {
  useHash: true,
};

@NgModule({
  imports: [RouterModule.forRoot(routes, config)],
  exports: [RouterModule],
})
export class AppRoutingModule {
}
