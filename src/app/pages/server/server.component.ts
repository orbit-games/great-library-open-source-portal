import { Component, OnInit } from '@angular/core';
import { ServerService } from '../../@core/data/server.service';
import { GlErrorHandlerService } from '../../@theme/error-handler.service';
import { ServerInfo } from '../../@core/api';
import { environment } from '../../../environments/environment';

@Component({
  selector: 'ngx-server',
  templateUrl: './server.component.html',
  styleUrls: ['./server.component.scss'],
})
export class ServerComponent implements OnInit {

  clearingCache: boolean = false;
  refreshingServerInfo: boolean = false;
  info: ServerInfo;

  url = environment.apiBaseUri;

  constructor(private serverService: ServerService, private errorHandler: GlErrorHandlerService) { }

  ngOnInit() {
    this.serverService.getServerInfo()
      .then(i => this.info = i);
  }

  refreshServerInfo() {
    this.refreshingServerInfo = true;
    this.serverService.getServerInfo()
      .then(i => {
        this.refreshingServerInfo = false;
        this.info = i;
      })
      .catch(e => {
        this.refreshingServerInfo = false;
        this.errorHandler.handle(e);
      });
  }

  clearCache() {

    this.clearingCache = true;
    this.serverService.clearCache()
        .then(() => this.clearingCache = false)
        .catch(e => this.errorHandler.handle(e));

  }

}
