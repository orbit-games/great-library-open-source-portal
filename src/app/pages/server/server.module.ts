import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NbSpinnerModule, NbButtonModule } from '@nebular/theme';
import { ThemeModule } from '../../@theme/theme.module';
import { ServerComponent } from './server.component';
import { NgArrayPipesModule, NgMathPipesModule } from 'angular-pipes';

@NgModule({
  declarations: [
    ServerComponent,
  ],
  imports: [
    ThemeModule,
    CommonModule,
    NbSpinnerModule,
    NbButtonModule,
    NgMathPipesModule,
    NgArrayPipesModule,
  ],
})
export class ServerModule { }
