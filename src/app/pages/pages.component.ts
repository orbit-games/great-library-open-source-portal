import { Component, OnInit, OnDestroy } from '@angular/core';

import { MENU_ITEMS } from './pages-menu';
import { SessionService } from '../@core/session.service';
import { User, Role } from '../@core/api';
import { NbMenuItem } from '@nebular/theme';
import { Subscription } from 'rxjs';

@Component({
  selector: 'ngx-pages',
  template: `
    <gl-main-layout>
      <nb-menu [items]="menu"></nb-menu>
      <router-outlet></router-outlet>
    </gl-main-layout>
    <gl-admin-chat></gl-admin-chat>
  `,
  styleUrls: ['./pages.component.scss']
})
export class PagesComponent implements OnInit, OnDestroy {

  loggedInUserRole: Role;
  private loggedInUserChangedSubscription: Subscription;
  menu: NbMenuItem[] = [];

  constructor(private sessionService: SessionService) {

  }

  ngOnInit() {
    this.sessionService.getLoggedInUser().subscribe(u => {
      this.loggedInUserRole = u ? u.role : null;
      this.refreshMenuItems();
    });
    this.loggedInUserChangedSubscription = this.sessionService.loggedInUserChanged.subscribe(u => {
      this.loggedInUserRole = u ? u.role : null;
      this.refreshMenuItems();
    });
  }

  ngOnDestroy() {
    this.loggedInUserChangedSubscription.unsubscribe();
  }

  refreshMenuItems() {
    if (this.loggedInUserRole === Role.ADMINISTRATOR) {

      this.menu = MENU_ITEMS;

    } else {

      // Create a deep copy of the menu items, so we can set them to hidden as required
      let filteredMenuItems = JSON.parse(JSON.stringify(MENU_ITEMS));
      this.filterMenuItems(filteredMenuItems);
      this.menu = filteredMenuItems;
    }
  }

  filterMenuItems(menuItems: NbMenuItem[]) {
    for (let menuItem of menuItems) {
      menuItem.hidden = menuItem.data && menuItem.data.roles && !menuItem.data.roles.includes(this.loggedInUserRole);
      if (menuItem.children) {
        this.filterMenuItems(menuItem.children);
      }
    }
  }

}
