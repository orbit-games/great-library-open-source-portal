import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ThemeModule } from '../../@theme/theme.module';
import {
  NbAccordionModule,
  NbDatepickerModule,
  NbListModule,
  NbRadioModule,
  NbSelectModule,
  NbSpinnerModule
} from '@nebular/theme';
import { Ng2SmartTableModule } from 'ng2-smart-table';
import { ReviewStepsComponent } from './review-steps/review-steps.component';
import { ReviewStepDetailComponent } from './review-steps/review-step-detail/review-step-detail.component';
import { ArgumentTypeListComponent } from './review-steps/review-step-detail/argument-type-list.component';
import { RubricsDetailComponent } from './rubrics/rubrics-detail/rubrics-detail.component';
import { SectionsComponent } from './sections/sections.component';
import { SectionListComponent } from './sections/section-list/section-list.component';
import { SectionEditorComponent } from './sections/section-editor/section-editor.component';
import { RoutedSectionEditorComponent } from './sections/routed-section-editor/routed-section-editor.component';
import { RouterModule } from '@angular/router';
import { ReactiveFormsModule } from '@angular/forms';
import { QuillModule } from 'ngx-quill';
import { TreeModule } from 'angular-tree-component';
import { ResourceSelectionComponent } from './resources/resource-selection/resource-selection.component';
import { ResourceEditorComponent } from './resources/resource-editor/resource-editor.component';
import { ResourcesComponent } from './resources/resources.component';
import { NewSectionEditorComponent } from './sections/new-section-editor/new-section-editor.component';
import { RoutedResourceEditorComponent } from './resources/routed-resource-editor/routed-resource-editor.component';
import { NewResourceEditorComponent } from './resources/new-resource-editor/new-resource-editor.component';
import { ResourceListComponent } from './resources/resource-list/resource-list.component';
import { RubricEditorComponent } from './rubrics/rubric-editor/rubric-editor.component';
import { SharedModule } from '../shared/shared.module';
import { RxReactiveFormsModule } from '@rxweb/reactive-form-validators';
import { SkillSelectionComponent } from './skills/skill-selection/skill-selection.component';
import { SkillEditorComponent } from './skills/skill-editor/skill-editor.component';
import { DeadlinesComponent } from './review-steps/deadlines/deadlines.component';
import { CourseEditorComponent } from './course/course-editor/course-editor.component';
import { ChaptersComponent } from './chapters/chapters.component';
import { ChapterListComponent } from './chapters/chapter-list/chapter-list.component';
import { RoutedChapterEditorComponent } from './chapters/routed-chapter-editor/routed-chapter-editor.component';
import { NewChapterEditorComponent } from './chapters/new-chapter-editor/new-chapter-editor.component';
import { ChapterEditorComponent } from './chapters/chapter-editor/chapter-editor.component';
import { SectionSelectionComponent } from './sections/section-selection/section-selection.component';
import { SectionViewerComponent } from './sections/section-viewer/section-viewer.component';
import { ReviewStepEditorComponent } from './review-steps/review-step-editor/review-step-editor.component';
import { RoutedReviewStepEditorComponent } from './review-steps/routed-review-step-editor/routed-review-step-editor.component';

@NgModule({
  declarations: [
    ReviewStepsComponent,
    ReviewStepDetailComponent,
    ArgumentTypeListComponent,
    RubricsDetailComponent,
    SectionsComponent,
    SectionListComponent,
    SectionEditorComponent,
    RoutedSectionEditorComponent,
    ResourceSelectionComponent,
    ResourceEditorComponent,
    ResourcesComponent,
    NewSectionEditorComponent,
    RoutedResourceEditorComponent,
    NewResourceEditorComponent,
    ResourceListComponent,
    RubricEditorComponent,
    SkillSelectionComponent,
    SkillEditorComponent,
    DeadlinesComponent,
    CourseEditorComponent,
    ChaptersComponent,
    ChapterListComponent,
    RoutedChapterEditorComponent,
    NewChapterEditorComponent,
    ChapterEditorComponent,
    SectionSelectionComponent,
    SectionViewerComponent,
    ReviewStepEditorComponent,
    RoutedReviewStepEditorComponent,
  ],
  entryComponents: [
    SkillEditorComponent,
  ],
  imports: [
    CommonModule,
    RouterModule,
    ReactiveFormsModule,
    // Needed for the RxWeb validators to work
    RxReactiveFormsModule,
    ThemeModule,
    NbSpinnerModule,
    NbRadioModule,
    Ng2SmartTableModule,
    QuillModule.forRoot(),
    TreeModule.forRoot(),
    NbAccordionModule,
    NbListModule,
    SharedModule,
    NbSelectModule,
    NbDatepickerModule,
  ],
})
export class StructureModule { }
