import { Component, Input, OnChanges, OnDestroy, OnInit, SimpleChanges } from '@angular/core';
import { ArgumentType, Chapter, Course, ResourceRelation, ReviewStep, Section } from '../../../../@core/api';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { FormControlUtil } from '../../../../@theme/util/FormControlUtil';
import { SessionService } from '../../../../@core/session.service';
import { CourseService } from '../../../../@core/data/course.service';
import { GlErrorHandlerService } from '../../../../@theme/error-handler.service';
import { Observable, Subscription } from 'rxjs';
import { RxwebValidators } from '@rxweb/reactive-form-validators';
import { RoutedReviewStepEditorComponent } from '../routed-review-step-editor/routed-review-step-editor.component';
import { ReviewStepHelper } from '../review-step-helper';
import { DisplayArgumentType } from '../review-step-detail/argument-type-list.component';

@Component({
  selector: 'ngx-review-step-editor',
  templateUrl: './review-step-editor.component.html',
  styleUrls: ['./review-step-editor.component.scss'],
})
export class ReviewStepEditorComponent implements OnInit, OnChanges, OnDestroy {

  @Input() chapterRef: string;
  @Input() reviewStepRef: string;

  saving: boolean = false;
  editRubrics: boolean = false;

  course: Course;
  chapter: Chapter;
  chapterIndex: number;
  reviewStep: ReviewStep;
  reviewStepIndex: number;

  nextReviewStep: ReviewStep;
  previousReviewStep: ReviewStep;

  displayArgumentTypes: DisplayArgumentType[];

  reviewStepForm: FormGroup;


  private courseChangedSubscription: Subscription;

  readonly controlStatus = FormControlUtil.controlStatus;
  readonly humanReadablePeerType = ReviewStepHelper.humanReadablePeerType;

  constructor(
    private sessionService: SessionService,
    private courseService: CourseService,
    private fb: FormBuilder,
    private errorHandler: GlErrorHandlerService,
  ) { }

  ngOnInit() {
    this.sessionService.getSelectedCourse().subscribe(c => this.setCourse(c));
    this.courseChangedSubscription = this.sessionService.selectedCourseChanged.subscribe(c => this.setCourse(c));
    this.constructReviewStepForm();

    this.sessionService.setCurrentPage({
      icon: 'fas fa-cogs',
      title: 'Review Steps',
    });
  }

  ngOnDestroy(): void {
    if (this.courseChangedSubscription) this.courseChangedSubscription.unsubscribe();
  }

  ngOnChanges(changes: SimpleChanges): void {

    const reviewStepRefChange = changes['reviewStepRef'];
    if (reviewStepRefChange && reviewStepRefChange.currentValue !== reviewStepRefChange.previousValue) {
      this.refreshReviewStep();
    }
  }

  setCourse(course: Course) {
    this.course = course;

    this.chapter = this.course.chapters.find(c => c.ref === this.chapterRef);
    this.chapterIndex = this.course.chapters.indexOf(this.chapter);

    this.refreshReviewStep();

  }

  constructReviewStepForm() {

    const reviewStep = (this.reviewStep || {
      quiz: null,
      allowedArguments: [],
    } as ReviewStep);

    // Only fields that are currently available for editing are included in this form, the #setFormValuesToReviewStep() method copies the
    // form values to the actual review step.
    this.reviewStepForm = this.fb.group({
      'name': this.fb.control(reviewStep.name, [Validators.required]),
      'description': reviewStep.description,
      'disabled': reviewStep.disabled,
      // TODO Validate that the deadline is between the previous and next deadline in the chapter/course
      'deadline': this.fb.control(new Date(reviewStep.deadline), [RxwebValidators.date()]),
      'fileUploadName': reviewStep.fileUploadName,
      'allowedArguments': this.fb.array(reviewStep.allowedArguments),
    });

    if (reviewStep.quiz) {
      this.reviewStepForm.addControl('quiz', this.fb.control(reviewStep.quiz));
    }
  }

  private refreshReviewStep(forceRefreshFromServer: boolean = false) {

    if (this.course == null || this.chapterRef == null || this.reviewStepRef == null) {
      this.reviewStep = null;
    } else {
      this.reviewStep = null;
      this.courseService.getReviewStep(this.course.ref, this.chapterRef, this.reviewStepRef, forceRefreshFromServer)
        .then((r: ReviewStep) => this.setReviewStep(r));
    }
  }

  private setReviewStep(reviewStep: ReviewStep) {
    this.reviewStep = reviewStep;
    this.constructReviewStepForm();
    this.refreshDisplayArgumentTypes();

    this.reviewStepIndex = this.chapter.reviewSteps.findIndex(rs => rs.ref === this.reviewStepRef);

    this.nextReviewStep = this.reviewStepIndex < this.chapter.reviewSteps.length - 1 ? this.chapter.reviewSteps[this.reviewStepIndex + 1] : null;
    this.previousReviewStep = this.reviewStepIndex > 0 ? this.chapter.reviewSteps[this.reviewStepIndex - 1] : null;
  }

  saveChanges() {

    this.saving = true;

    // Clone the existing review step:
    const reviewStep: ReviewStep = JSON.parse(JSON.stringify(this.reviewStep));
    this.setFormValuesToReviewStep(reviewStep);
    this.courseService.updateReviewStep(this.course.ref, this.chapterRef, this.reviewStepRef, reviewStep)
      .then((r: ReviewStep) => {
        this.saving = false;
        this.setReviewStep(r);
        this.sessionService.refreshCourse();
      })
      .catch(e => {
        this.saving = false;
        this.errorHandler.handle(e);
      });
  }

  private setFormValuesToReviewStep(reviewStep: ReviewStep) {
    // Copy the editable fields:
    const reviewStepFormValue = this.reviewStepForm.value;
    reviewStep.name = reviewStepFormValue.name;
    reviewStep.description = reviewStepFormValue.description;
    reviewStep.disabled = reviewStepFormValue.disabled;
    reviewStep.deadline = reviewStepFormValue.deadline;
    reviewStep.allowedArguments = reviewStepFormValue.allowedArguments;
    reviewStep.quiz = reviewStepFormValue.quiz;
  }

  canDeactivate(): boolean | Observable<boolean> {
    return this.reviewStepForm.pristine;
  }

  private refreshDisplayArgumentTypes() {

    if (!this.reviewStep.allowedArguments) {
      this.displayArgumentTypes = [];
    } else {
      this.displayArgumentTypes = this.reviewStep.allowedArguments
        .filter(a => a.parentRef == null)
        .map(a => this.convertToDisplayArgumentType(a, this.reviewStep.allowedArguments));
    }
  }

  private convertToDisplayArgumentType(argumentType: ArgumentType, allArgumentTypes: ArgumentType[]): DisplayArgumentType {

    return {
      ref: argumentType.ref,
      name: argumentType.name,
      description: argumentType.description,
      required: argumentType.required,
      children: allArgumentTypes
        .filter(a => a.parentRef === argumentType.ref)
        .map(a => this.convertToDisplayArgumentType(a, this.reviewStep.allowedArguments)),
    };
  }

  onEditRubricsClicked() {
    this.editRubrics = !this.editRubrics;
  }
}
