import { Component, OnInit, SecurityContext } from '@angular/core';
import { ReviewStep, Chapter, Course, ArgumentType } from '../../../../@core/api';
import { SessionService } from '../../../../@core/session.service';
import { ActivatedRoute, Router } from '@angular/router';
import { ReviewStepHelper } from '../review-step-helper';
import { DomSanitizer } from '@angular/platform-browser';
import { DisplayArgumentType } from './argument-type-list.component';

@Component({
  selector: 'ngx-review-step-detail',
  templateUrl: './review-step-detail.component.html',
  styleUrls: ['./review-step-detail.component.scss'],
})
export class ReviewStepDetailComponent implements OnInit {

  chapterRef: string;
  reviewStepRef: string;

  course: Course;
  chapterIndex: number;
  chapter: Chapter;
  reviewStepIndex: number;
  reviewStep: ReviewStep;
  displayArgumentTypes: DisplayArgumentType[];

  constructor(
    private sessionService: SessionService,
    private router: Router, private route: ActivatedRoute,
    private sanitizer: DomSanitizer,
  ) { }

  ngOnInit() {

    this.route.params.subscribe(p => {
      this.chapterRef = p['chapterRef'];
      this.reviewStepRef = p['reviewStepRef'];
      this.refreshReviewStep();
    });

    this.sessionService.getSelectedCourse().subscribe(c => {
      this.course = c;
      this.refreshReviewStep();
    });

    this.sessionService.setCurrentPage({
      icon: 'fa fa-cogs',
      title: 'Review Steps',
      list: {
        visible: true,
        tooltip: 'Back to review steps',
        clickHandler: () => this.router.navigate(['/pages', 'structure', 'review-steps']),
      },
    });
  }

  refreshReviewStep() {
    // TODO actually retrieve the specific review step from the database, for now we rely on getting the whole course
    if (this.course == null || this.chapterRef == null || this.reviewStepRef == null) {
      return;
    }

    this.chapter = this.course.chapters.find(c => c.ref === this.chapterRef);
    if (this.chapter == null) {
      return;
    }
    this.chapterIndex = this.course.chapters.indexOf(this.chapter);
    this.reviewStep = this.chapter.reviewSteps.find(c => c.ref === this.reviewStepRef);
    this.reviewStepIndex = this.chapter.reviewSteps.indexOf(this.reviewStep);
    this.refreshDisplayArgumentTypes();
  }

  private refreshDisplayArgumentTypes() {

    if (!this.reviewStep.allowedArguments) {
      this.displayArgumentTypes = [];
    } else {
      this.displayArgumentTypes = this.reviewStep.allowedArguments
        .filter(a => a.parentRef == null)
        .map(a => this.convertToDisplayArgumentType(a, this.reviewStep.allowedArguments));
    }
  }

  private convertToDisplayArgumentType(argumentType: ArgumentType, allArgumentTypes: ArgumentType[]): DisplayArgumentType {

    return {
      ref: argumentType.ref,
      name: argumentType.name,
      description: argumentType.description,
      required: argumentType.required,
      children: allArgumentTypes
        .filter(a => a.parentRef === argumentType.ref)
        .map(a => this.convertToDisplayArgumentType(a, this.reviewStep.allowedArguments)),
    };
  }

  sanitizeHtml(value: any) {
    return this.sanitizer.sanitize(SecurityContext.HTML, value);
  }

  humanReadablePeerType = ReviewStepHelper.humanReadablePeerType;
  humanReadableDiscussionType = ReviewStepHelper.humanReadableDiscussionType;

}
