import { Component, OnInit, Input } from '@angular/core';

export interface DisplayArgumentType {
  ref: string;
  name: string;
  description: string;
  required: boolean;
  children: DisplayArgumentType[];
}

@Component({
  selector: 'ngx-argument-type-list',
  template: `
  <ul>
    <li *ngFor="let argumentType of argumentTypes">
      <p><b>{{argumentType.name}}</b> <span class="required-star" *ngIf="argumentType.required">*</span>
      <span *ngIf="argumentType.description"><br>{{argumentType.description}}</span></p>
      <ngx-argument-type-list *ngIf="argumentType.children" [argumentTypes]="argumentType.children"></ngx-argument-type-list>
    </li>
  </ul>
  `,
  styles: ['.required-star { color: red; }'],

})
export class ArgumentTypeListComponent implements OnInit {

  @Input() argumentTypes: DisplayArgumentType[];

  constructor() { }

  ngOnInit() {
  }

}
