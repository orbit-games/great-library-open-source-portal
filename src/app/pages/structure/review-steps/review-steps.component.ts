import { Component, OnInit } from '@angular/core';
import { SessionService } from '../../../@core/session.service';
import { CourseService } from '../../../@core/data/course.service';
import { GlErrorHandlerService } from '../../../@theme/error-handler.service';
import { Course, ReviewStep, PeerType, Chapter, DiscussionType } from '../../../@core/api';
import { FormatHelper } from '../../../@theme/pipes/format-helper';
import { LocalDataSource } from 'ng2-smart-table';
import { Router } from '@angular/router';
import { ReviewStepHelper } from './review-step-helper';

interface DisplayReviewStep {

  ref: string;
  chapterRef: string;
  order: string;
  chapterName: string;
  name: string;
  peerType: string;
  deadline: Date;
  discussion: string;
  rubrics: number;
  fileUpload: boolean;
  disabled: boolean;

}

@Component({
  selector: 'ngx-review-steps',
  templateUrl: './review-steps.component.html',
  styleUrls: ['./review-steps.component.scss'],
})
export class ReviewStepsComponent implements OnInit {

  settings = {
    actions: {
      add: false,
      edit: true,
      delete: false,
    },
    pager: {
      perPage: 50,
    },
    edit: {
      editButtonContent: 'Edit',
    },
    filter: {
      inputClass: 'gl-filter',
    },
    mode: 'external',
    columns: {
      chapterName: {
        title: 'Chapter',
      },
      order: {
        title: 'Index',
      },
      name: {
        title: 'Name',
      },
      peerType: {
        title: 'Executed by',
        valuePrepareFunction: ReviewStepHelper.humanReadablePeerType,
      },
      deadline: {
        title: 'Deadline',
        valuePrepareFunction: FormatHelper.toHumanReadableDateTime,
      },
      discussion: {
        title: 'Discussion',
      },
      rubrics: {
        title: 'Rubrics',
      },
      fileUpload: {
        title: 'File',
        valuePrepareFunction: v => v ? 'Yes' : 'No',
      },
    },
    rowClassFunction: v => v.data.disabled ? 'strikethrough' : '',
  };
  source: LocalDataSource = new LocalDataSource();

  course: Course;

  constructor(
    private sessionService: SessionService,
    private courseService: CourseService,
    private errorHandler: GlErrorHandlerService,
    private router: Router,
  ) { }

  ngOnInit() {

    this.sessionService.getSelectedCourse().subscribe(c => this.setCourse(c));
    this.sessionService.selectedCourseChanged.subscribe(c => this.setCourse(c));

    this.sessionService.setCurrentPage({
      icon: 'fa fa-cogs',
      title: 'Review Steps',
    });
  }

  setCourse(course: Course) {
    this.course = course;
    this.refreshTableDataSource();
  }

  refreshTableDataSource() {
    if (this.course == null) {
      this.source.load([]);
    }

    const reviewSteps: Array<DisplayReviewStep> = [];
    let chapterIndex = 0;
    for (const chapter of this.course.chapters) {

      let reviewStepIndex = 0;
      for (const reviewStep of chapter.reviewSteps) {
        reviewSteps.push(this.toDisplayReviewStep(reviewStep, reviewStepIndex, chapterIndex, chapter));
        reviewStepIndex++;
      }

      chapterIndex++;
    }

    this.source.load(reviewSteps);

  }

  private toDisplayReviewStep(reviewStep: ReviewStep, reviewStepIndex: number, chapterIndex: number, chapter: Chapter): DisplayReviewStep {
    return {
      ref: reviewStep.ref,
      chapterRef: chapter.ref,
      chapterName: (chapterIndex + 1) + '. ' + chapter.name,
      order: (chapterIndex + 1) + '.' + (reviewStepIndex + 1),
      name: reviewStep.name,
      peerType: reviewStep.peerType,
      deadline: reviewStep.deadline,
      discussion: ReviewStepHelper.humanReadableDiscussionType(reviewStep),
      fileUpload: reviewStep.fileUpload,
      rubrics: reviewStep.quiz != null ? reviewStep.quiz.elements.length : 0,
      disabled: reviewStep.disabled,
    };
  }

  onSelectReviewStep(row: any) {

    const rowData: DisplayReviewStep = row.data;
    this.router.navigate(['/pages/structure', 'chapters', rowData.chapterRef, 'review-step', rowData.ref]);
  }

}
