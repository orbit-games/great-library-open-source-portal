import { Component, HostListener, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ReviewStepEditorComponent } from '../review-step-editor/review-step-editor.component';

@Component({
  selector: 'ngx-routed-review-step-editor',
  template: `<ngx-review-step-editor
          [reviewStepRef]="reviewStepRef"
          [chapterRef]="chapterRef"
          #reviewStepEditorComponent>
  </ngx-review-step-editor>`,
  styles: [],
})
export class RoutedReviewStepEditorComponent implements OnInit {

  chapterRef: string;
  reviewStepRef: string;

  @ViewChild('reviewStepEditorComponent', {static: false}) reviewStepEditorComponent: ReviewStepEditorComponent;

  constructor(
    private route: ActivatedRoute,
  ) {
    this.route.params.subscribe(params => {
      this.chapterRef = params['chapterRef'];
      this.reviewStepRef = params['reviewStepRef'];
    });
  }

  ngOnInit() {

  }

  // @HostListener allows us to also guard against browser refresh, close, etc.
  @HostListener('window:beforeunload')
  canDeactivate = () => this.reviewStepEditorComponent.canDeactivate()

}
