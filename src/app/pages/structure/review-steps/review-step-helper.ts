import { PeerType, ReviewStep, DiscussionType } from '../../../@core/api';


export class ReviewStepHelper {

    public static humanReadablePeerType(peerType: PeerType): string {
        return peerType === PeerType.REVIEWER ? 'Reviewer' : 'Submitter';
    }

    public static humanReadableDiscussionType(reviewStep: ReviewStep): string {

        switch (reviewStep.discussionType) {
            case DiscussionType.NONE: return 'No';
            case DiscussionType.FREEFORMARGUMENTS: return 'Free form text';
            case DiscussionType.RESTRICTEDARGUMENTS: return 'Arguments (' + reviewStep.allowedArguments.filter(a => a.required).length + ' required)';
            case DiscussionType.REPLY: return 'Reply';
          }
    }
}
