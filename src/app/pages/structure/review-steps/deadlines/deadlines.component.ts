import { Component, HostListener, OnDestroy, OnInit } from '@angular/core';
import { SessionService } from '../../../../@core/session.service';
import { CourseService } from '../../../../@core/data/course.service';
import { GlErrorHandlerService } from '../../../../@theme/error-handler.service';
import { Chapter, Course, ReviewStep } from '../../../../@core/api';
import { Observable, Subscription } from 'rxjs';
import { NgbTimeStruct } from '@ng-bootstrap/ng-bootstrap';
import { FormArray, FormBuilder, FormGroup } from '@angular/forms';
import { RxFormBuilder, RxwebValidators } from '@rxweb/reactive-form-validators';

@Component({
  selector: 'ngx-deadlines',
  templateUrl: './deadlines.component.html',
  styleUrls: ['./deadlines.component.scss'],
})
export class DeadlinesComponent implements OnInit, OnDestroy {

  course: Course;
  reviewStepNames: string[];
  deadlinesForm: FormGroup;
  reviewSteps: ReviewStep[][] = [];

  saving: boolean;

  deadlineTime: NgbTimeStruct = { hour: 22, minute: 0, second: 0 };

  private courseChangedSubscription: Subscription;


  constructor(
    private sessionService: SessionService,
    private courseService: CourseService,
    private fb: FormBuilder,
    private errorHandler: GlErrorHandlerService,
  ) { }

  ngOnInit() {

    this.courseChangedSubscription = this.sessionService.getSelectedCourse().subscribe(c => {
      this.course = c;
      this.refreshCourse();
    });

    this.sessionService.setCurrentPage({
      icon: 'fa fa-cogs',
      title: 'Deadlines',
    });
  }

  ngOnDestroy(): void {
    this.courseChangedSubscription.unsubscribe();
  }

  refreshCourse() {

    if (!this.course || !this.course.chapters) {
      this.reviewStepNames = [];
      this.reviewSteps = [];
      return;
    }

    const reviewStepNames = [];
    this.course.chapters.flatMap(c => c.reviewSteps)
      .forEach(r => {
        if (!reviewStepNames.includes(r.name)) {
          reviewStepNames.push(r.name);
        }
      });

    this.reviewStepNames = reviewStepNames;

    this.deadlinesForm = this.fb.group({
      chapters: this.fb.array(
        this.course.chapters.map((c, i) =>
          this.fb.group({
            chapterRef: this.fb.control(c.ref),
            reviewSteps: this.fb.array(
              reviewStepNames.map((rsn, j) => {
                const rs = this.chapterStep(c, rsn);
                // TODO: Handle missing review steps
                return this.fb.group({
                  reviewStepRef: rs.ref,
                  deadline: new Date(rs.deadline),
                });
              })),
          }),
        ),
      ),
    });

    this.reviewSteps = [];
    for (let i = 0; i < this.course.chapters.length; i++) {
      this.reviewSteps.push([]);
      for (let j = 0; j < this.reviewStepNames.length; j++) {
        this.reviewSteps[i].push(this.chapterStep(this.course.chapters[i], this.reviewStepNames[j]));
      }
    }

    const firstDeadline = new Date(this.course.chapters[0].reviewSteps[0].deadline);
    this.deadlineTime = {
      hour: firstDeadline.getHours(),
      minute: firstDeadline.getMinutes(),
      second: firstDeadline.getSeconds(),
    };
  }

  private chapterStep(chapter: Chapter, stepName: string): ReviewStep {

    return chapter.reviewSteps.find(r => r.name === stepName);
  }

  setAllDeadlineTimes() {
    const formValue = this.deadlinesForm.value;
    for (let i = 0; i < formValue.chapters.length; i++) {
      for (let j = 0; j < formValue.chapters[i].reviewSteps.length; j++) {
        const date = new Date(formValue.chapters[i].reviewSteps[j].deadline);
        formValue.chapters[i].reviewSteps[j].deadline =
          new Date(date.setHours(this.deadlineTime.hour, this.deadlineTime.minute, this.deadlineTime.second));
      }
    }
    this.deadlinesForm.patchValue(formValue);
    this.deadlinesForm.markAsDirty();
  }

  onSaveClicked() {

    this.saving = true;

    this.courseService.updateDeadlines(this.course.ref, this.deadlinesForm.value)
      .then(d => {
        this.saving = false;
        this.deadlinesForm.patchValue(d);
        this.sessionService.refreshCourse();
        this.deadlinesForm.markAsPristine();
      })
      .catch(e => {
        this.saving = false;
        this.errorHandler.handle(e);
      });
  }

  static datesEqual(d1: Date, d2: Date): boolean {
    return Math.abs(d1.valueOf() - d2.valueOf()) > 0;
  }

  // @HostListener allows us to also guard against browser refresh, close, etc.
  @HostListener('window:beforeunload')
  canDeactivate(): boolean | Observable<boolean> {
    return this.deadlinesForm.pristine ;
  }
}
