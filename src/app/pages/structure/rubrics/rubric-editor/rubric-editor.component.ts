import {Component, forwardRef, Input, OnInit} from '@angular/core';
import {
  AbstractControl,
  ControlValueAccessor,
  FormArray,
  FormBuilder,
  FormGroup, NG_VALIDATORS,
  NG_VALUE_ACCESSOR, ValidationErrors,
  Validator,
  Validators,
} from '@angular/forms';
import {QuestionType, Quiz, QuizElement, QuizElementOption} from '../../../../@core/api';
import {NbDialogService} from '@nebular/theme';
import {ConfirmDeleteDialogComponent} from '../../../../@theme/components/confirm-delete-dialog/confirm-delete-dialog.component';
import {FormControlUtil} from '../../../../@theme/util/FormControlUtil';
import { RxwebValidators } from '@rxweb/reactive-form-validators';

@Component({
  selector: 'ngx-rubric-editor',
  templateUrl: './rubric-editor.component.html',
  styleUrls: ['./rubric-editor.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => RubricEditorComponent),
      multi: true,
    },
    {
      provide: NG_VALIDATORS,
      useExisting: forwardRef(() => RubricEditorComponent),
      multi: true,
    },
  ],
})
export class RubricEditorComponent implements OnInit, ControlValueAccessor, Validator {

  quizForm: FormGroup;

  quiz: Quiz;

  onChange: (_: any) => void;
  disabled: boolean = false;

  @Input() required: boolean = true;

  constructor(
    private fb: FormBuilder,
    private dialogService: NbDialogService,
    ) { }

  ngOnInit() {
    this.constructQuizForm();
  }

  private constructQuizForm() {

    const quiz: Quiz = this.quiz || {
      name: '',
      description: '',
      maxScore: 0,
      elements: [],
    };

    if (!this.quizForm) {
      this.quizForm = this.fb.group({
        ref: this.fb.control(quiz.ref),
        name: this.fb.control(quiz.name, [Validators.required]),
        description: this.fb.control(quiz.description),
        maxScore: this.fb.control(quiz.maxScore, [RxwebValidators.numeric({allowDecimal: false})]),
        elements: this.fb.array([]),
      });

      this.quizForm.valueChanges.subscribe(q => {
        if (this.onChange) {
          this.onChange(q);
        }
      });

      this.setDisabledState(this.disabled);

    } else {
      (<FormArray>this.quizForm.controls.elements).clear();
    }

    const elementsArray = <FormArray>this.quizForm.controls.elements;

    for (const element of quiz.elements) {
      elementsArray.push(this.createQuizElementFormGroup(element));
    }

    if (this.quiz) {
      this.quizForm.patchValue(this.quiz);
    }
  }

  createQuizElementFormGroup(element: QuizElement): FormGroup {
    return this.fb.group({
      ref: this.fb.control(element.ref),
      order: this.fb.control(element.order, [Validators.pattern('\-?[0-9]+'), Validators.required]),
      text: this.fb.control(element.text, [Validators.required, Validators.minLength(3)]),
      questionType: this.fb.control(element.questionType, [Validators.required, Validators.pattern('SELECT_ONE|SELECT_MULTIPLE|NONE')]),
      options: this.fb.array((element.options || []).map((o: QuizElementOption) => this.createOptionFormGroup(o))),
    });
  }

  createOptionFormGroup(o: QuizElementOption): FormGroup {
    return this.fb.group({
      ref: this.fb.control(o.ref),
      order: this.fb.control(o.order, [Validators.pattern('\-?[0-9]+'), Validators.required]),
      text: this.fb.control(o.text, [Validators.required]),
      points: this.fb.control(o.points, [Validators.pattern('\-?[0-9]+'), Validators.required]),
    });
  }

  get quizFormElementControls(): AbstractControl[] {
    return (<FormArray>this.quizForm.controls.elements).controls;
  }

  getQuizElementFormOptions(element: AbstractControl): AbstractControl[] {
    return (<FormArray>element.get('options')).controls;
  }

  getQuizElementQuestionType(element: AbstractControl): QuestionType {

    switch (element.get('questionType').value) {
      case QuestionType.SELECTONE: return QuestionType.SELECTONE;
      case QuestionType.SELECTMULTIPLE: return QuestionType.SELECTMULTIPLE;
      default: return QuestionType.SELECTONE;
    }
  }

  registerOnChange(fn: (_: any) => void): void {
    this.onChange = fn;
  }

  registerOnTouched(fn: any): void {
    // TODO implement
  }

  setDisabledState(isDisabled: boolean): void {
    this.disabled = isDisabled;
    if (this.quizForm) {
      if (this.disabled) {
        this.quizForm.disable();
      } else {
        this.quizForm.enable();
      }
    }
  }

  writeValue(obj: any): void {

    this.quiz = obj;
    this.constructQuizForm();
  }

  registerOnValidatorChange(fn: () => void): void {

  }

  validate(control: AbstractControl): ValidationErrors | null {

    if (!this.required) {
      return null;
    }

    if (!this.quizForm.valid) {
      return {
        'invalid-quiz': 'The quiz is invalid',
      };
    } else {
      return null;
    }

  }

  controlStatus = FormControlUtil.controlStatus;

  onAddQuestionClicked() {

    const elementControls = (<FormArray>this.quizForm.controls.elements);
    const lastElement = elementControls.controls[elementControls.length - 1];
    const order = lastElement ? (lastElement.get('order').value + 10) : 0;
    elementControls.push(this.createQuizElementFormGroup({
      ref: null,
      text: '',
      order,
      questionType: QuestionType.SELECTONE,
      options: [],
    }));
    elementControls.markAsDirty();
  }


  onRemoveQuestionClicked(i: number) {

    const dialog = this.dialogService.open(ConfirmDeleteDialogComponent, {
      context: {
        'title': `Confirm deleting question ${i + 1}`,
        'content': `Are you sure you want to remove question ${i + 1}?`,
      },
    });
    dialog.onClose.subscribe(d => {
      if (d) {
        const elementControls = (<FormArray>this.quizForm.controls.elements);
        elementControls.removeAt(i);
        elementControls.markAsDirty();
      }
    });
  }

  onDeleteOptionClicked(element: AbstractControl, optionIndex: number) {
    const optionsArray = (<FormArray>element.get('options'));
    optionsArray.removeAt(optionIndex);
  }

  onAddOptionClicked(element: AbstractControl) {

    const optionsArray = (<FormArray>element.get('options'));
    const lastOption = optionsArray.controls[optionsArray.length - 1];
    const order = lastOption ? lastOption.get('order').value + 1 : 0;

    optionsArray.push(this.createOptionFormGroup({
      ref: null,
      text: '',
      order: order,
      points: 0,
    }));

    optionsArray.markAsDirty();
  }

  onMoveOptionUpClicked(element: AbstractControl, j: number) {

    if (j === 0) return;

    RubricEditorComponent.swap(element, j, j - 1);
  }

  onMoveOptionDownClicked(element: AbstractControl, j: number) {

    if (j >= this.getQuizElementFormOptions(element).length) return;

    RubricEditorComponent.swap(element, j, j + 1);
  }

  static swap(element: AbstractControl, i1: number, i2: number) {

    const optionsArray = (<FormArray>element.get('options')).controls;

    // Swap the options:
    const c1 = optionsArray[i1];
    const c2 = optionsArray[i2];
    const c1Order = c1.get('order').value;
    const c2Order = c2.get('order').value;
    c1.patchValue({order: c2Order});
    c2.patchValue({order: c1Order});

    optionsArray[i1] = c2;
    optionsArray[i2] = c1;

    c1.markAsDirty();
    c2.markAsDirty();
  }

}

