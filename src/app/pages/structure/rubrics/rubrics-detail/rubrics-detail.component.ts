import { Component, OnInit, Input, SecurityContext } from '@angular/core';
import { Quiz } from '../../../../@core/api';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'ngx-rubrics-detail',
  templateUrl: './rubrics-detail.component.html',
  styleUrls: ['./rubrics-detail.component.scss'],
})
export class RubricsDetailComponent implements OnInit {

  @Input() quiz: Quiz;

  constructor(private sanitizer: DomSanitizer) { }

  ngOnInit() {
  }

  sanitizeHtml(content: any) {
    return this.sanitizer.sanitize(SecurityContext.HTML, content);
  }

}
