import { Component, Input, OnChanges, OnInit, SimpleChanges, TemplateRef, ViewChild } from '@angular/core';
import { Chapter, Course, Section, SectionRelation } from '../../../../@core/api';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Observable, Subscription } from 'rxjs';
import {FormControlUtil} from '../../../../@theme/util/FormControlUtil';
import { SessionService } from '../../../../@core/session.service';
import { CourseService } from '../../../../@core/data/course.service';
import { GlErrorHandlerService } from '../../../../@theme/error-handler.service';
import { RxwebValidators } from '@rxweb/reactive-form-validators';
import { NbDialogRef, NbDialogService } from '@nebular/theme';

interface SectionCollection {
  [sectionRef: string]: Section;
}

@Component({
  selector: 'ngx-chapter-editor',
  templateUrl: './chapter-editor.component.html',
  styleUrls: ['./chapter-editor.component.scss'],
})
export class ChapterEditorComponent implements OnInit, OnChanges {

  @Input() chapterRef: string;
  @Input() isNew: boolean = false;

  sections: SectionCollection;
  course: Course;
  chapter: Chapter;
  saving: boolean = false;

  selectedNewSectionRef: string;

  chapterForm: FormGroup;

  @ViewChild('confirmDeleteSectionRelationDialog', {static: true}) confirmDeleteSectionRelationDialog: TemplateRef<any>;
  @ViewChild('selectSectionDialog', {static: true}) selectSectionDialog: TemplateRef<any>;

  private courseChangedSubscription: Subscription;

  readonly controlStatus = FormControlUtil.controlStatus;

  constructor(
    private sessionService: SessionService,
    private courseService: CourseService,
    private fb: FormBuilder,
    private dialogService: NbDialogService,
    private errorHandler: GlErrorHandlerService,
  ) { }

  ngOnInit() {
    this.constructChapterForm();

    this.sessionService.getSelectedCourse().subscribe(c => this.setCourse(c));
    this.courseChangedSubscription = this.sessionService.selectedCourseChanged.subscribe(c => this.setCourse(c));
  }

  ngOnChanges(changes: SimpleChanges): void {

    if (changes['chapterRef'] && changes['chapterRef'].previousValue !== changes['chapterRef'].currentValue) {
      this.refreshChapter();
    }
  }

  setCourse(c: Course) {
    this.course = c;
    this.sections = {};
    c.sections.forEach(s => this.sections[s.ref] = s);

    this.refreshChapter();
  }

  private refreshChapter() {

    if (this.course == null || this.chapterRef == null) {
      return;
    }

    this.chapter = this.course.chapters.find(c => c.ref === this.chapterRef);
    if (this.chapter) {
      this.constructChapterForm();
      this.chapterForm.markAsPristine();
    }
  }

  private constructChapterForm() {

    const chapter: Chapter = (this.chapter || {
      name: '',
      sectionRelations: [],
      reviewSteps: [],
    });

    this.chapterForm = this.fb.group({
      ref: chapter.ref,
      name: this.fb.control(chapter.name, [Validators.required]),
      order: chapter.order,
      description: this.fb.control(chapter.description),
      sectionRelations: this.fb.array(chapter.sectionRelations.map(s => this.constructSectionRelationFormGroup(s))),
      // Review steps are not editable from the chapter editor
      reviewSteps: chapter.reviewSteps,
      reviewerCount: this.fb.control(chapter.reviewerCount, [RxwebValidators.numeric({allowDecimal: false})]),
      reviewerRelationGenerationType: chapter.reviewerRelationGenerationType,
    });
  }

  constructSectionRelationFormGroup(s: SectionRelation): FormGroup {
    return this.fb.group({
      sectionRef: s.sectionRef,
      description: s.description,
      sortOrder: s.sortOrder,
      optional: s.optional,
    });
  }

  get sectionRelationsArray(): FormArray {
    return this.chapterForm.get('sectionRelations') as FormArray;
  }

  onAddSectionRelationClicked() {
    this.selectedNewSectionRef = null;
    this.dialogService.open(this.selectSectionDialog, {
      context: {},
      closeOnBackdropClick: false,
      closeOnEsc: true,
    });
  }

  onConfirmAddSectionRelation(selectedSectionRef: string, dialogRef: NbDialogRef<any>) {

    this.sectionRelationsArray.push(this.constructSectionRelationFormGroup({
      sectionRef: selectedSectionRef,
      description: '',
      optional: true,
      sortOrder: this.sectionRelationsArray.length * 10,
    }));
    this.sectionRelationsArray.markAsDirty();
    dialogRef.close();
    this.selectedNewSectionRef = null;
  }

  onDeleteSectionRelationClicked(sectionRelationIndex: number) {

    this.dialogService.open(this.confirmDeleteSectionRelationDialog, {
      context: {
        sectionRelationIndex,
        sectionRef: this.sectionRelationsArray.at(sectionRelationIndex).value.sectionRef,
      },
    });
  }

  onConfirmDeleteSectionRelation(sectionRelationIndex: number, dialogRef: NbDialogRef<any>) {

    this.sectionRelationsArray.removeAt(sectionRelationIndex);
    this.sectionRelationsArray.markAsDirty();
    dialogRef.close();
  }

  saveChanges() {

    this.saving = true;

    if (this.isNew) {

      throw Error('Creating chapters is not yet supported');
      // const newChapter: Chapter = {
      //   ref: null,
      //   name: null,
      //   description: null,
      //   sectionRelations: [],
      //   reviewSteps: [],
      //   order: this.course.chapters.length * 10,
      // };
      // this.setFormValuesToChapter(newChapter);
      //
      // this.courseService.createChapter(this.course.ref, newChapter)
      //   .then((c: Chapter) => {
      //
      //     this.saving = false;
      //     this.chapterForm.markAsPristine();
      //     this.sessionService.refreshCourse();
      //     // this.chapterStructureService.addChapter(s);
      //
      //   }).catch(e => {
      //     this.saving = false;
      //     this.errorHandler.handle(e);
      // });

    } else {

      // To ensure that we don't override the order and parent of this section when updating it, we first get the latest section.
      this.courseService.getChapter(this.course.ref, this.chapterRef, true)
        .then((c: Chapter) => {

          // TODO: Show an error/warning if someone edited the one of the form fields while we were on this page
          this.setFormValuesToChapter(c);

          return this.courseService.updateChapter(this.course.ref, this.chapterRef, c);

        })
        .then((c: Chapter) => {
          this.saving = false;

          this.chapterRef = c.ref;
          this.chapter = c;

          this.chapterForm.markAsPristine();
          // this.chapterStructureService.updateChapter(s);
          this.sessionService.refreshCourse();

        })
        .catch(e => {
          this.saving = false;
          this.errorHandler.handle(e);
        });
    }
  }

  private setFormValuesToChapter(chapter: Chapter) {
    // Copy the editable fields:
    const chapterFormValue = this.chapterForm.value;
    chapter.name = chapterFormValue.name;
    chapter.description = chapterFormValue.description;
    chapter.sectionRelations = chapterFormValue.sectionRelations;
  }

  canDeactivate(): boolean | Observable<boolean> {
    return this.chapterForm.pristine;
  }

}
