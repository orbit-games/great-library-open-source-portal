import { Component, OnInit } from '@angular/core';
import { SessionService } from '../../../@core/session.service';
import { CourseService } from '../../../@core/data/course.service';
import { ChapterStructureService } from './chapter-structure.service';
import { Chapter, Course } from '../../../@core/api';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'ngx-chapters',
  templateUrl: './chapters.component.html',
  styleUrls: ['./chapters.component.scss'],
  providers: [
    ChapterStructureService,
  ],
})
export class ChaptersComponent implements OnInit {

  course: Course;

  chapterRef: string;

  constructor(
    private sessionService: SessionService,
    private courseService: CourseService,
    private chapterStructureService: ChapterStructureService,
    private router: Router,
    private route: ActivatedRoute,
  ) { }

  ngOnInit() {
    this.sessionService.getSelectedCourse().subscribe(c => this.setCourse(c));
    this.sessionService.selectedCourseChanged.subscribe(c => this.setCourse(c));

    this.sessionService.setCurrentPage({
      icon: 'fas fa-cogs',
      title: 'Chapters',
    });
  }

  private setCourse(course: Course) {
    this.course = course;
  }

  onNewChapterClicked() {
    // TODO implement
  }

  onChapterSelected(chapter: Chapter) {

    this.tryOpenChapter(chapter.ref);
  }

  tryOpenChapter(chapterRef: string) {

    const oldChapterRef = this.chapterRef;
    this.router.navigate([chapterRef == null ? 'new' : chapterRef], { relativeTo: this.route })
      .then(routed => {
        if (routed) {
          this.chapterRef = chapterRef;
        } else {
          this.chapterRef = oldChapterRef;
        }
      });
  }
}
