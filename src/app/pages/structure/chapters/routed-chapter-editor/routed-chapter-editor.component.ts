import {Component, HostListener, OnInit, ViewChild} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {ChapterEditorComponent} from '../chapter-editor/chapter-editor.component';
import { ComponentCanDeactivate } from '../../../../@theme/guards/pending-changes.guard';

@Component({
  selector: 'ngx-routed-chapter-editor',
  template: `<ngx-chapter-editor [chapterRef]="chapterRef" #chapterEditor></ngx-chapter-editor>`,
  styles: [],
})
export class RoutedChapterEditorComponent implements OnInit, ComponentCanDeactivate {

  chapterRef: string;

  @ViewChild('chapterEditor', {static: false}) chapterEditor: ChapterEditorComponent;

  constructor(
    private route: ActivatedRoute,
  ) {
    this.route.params.subscribe(params => {
      this.chapterRef = params['chapterRef'];
    });
  }

  ngOnInit() {

  }

  // @HostListener allows us to also guard against browser refresh, close, etc.
  @HostListener('window:beforeunload')
  canDeactivate = () => this.chapterEditor.canDeactivate()
}
