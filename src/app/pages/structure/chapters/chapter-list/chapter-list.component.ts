import { Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges } from '@angular/core';
import { SelectableItem } from '../../../../@theme/components/selectable-item-list/selectable-item-list.component';
import { Chapter } from '../../../../@core/api';
import { arrayBufferToBase64 } from 'angular-file/file-upload/fileTools';
import { ArrayUtils } from '../../../../@core/utils/array-utils';

@Component({
  selector: 'ngx-chapter-list',
  templateUrl: './chapter-list.component.html',
  styleUrls: ['./chapter-list.component.scss'],
})
export class ChapterListComponent implements OnInit, OnChanges {

  @Input() chapters: Chapter[];
  @Input() selectedChapterRef: string;

  @Input() size: string;

  @Output() chapterSelected = new EventEmitter<Chapter>();

  disabled = false;

  chapterItems: SelectableItem<Chapter>[];

  constructor() { }

  ngOnInit() {
  }

  ngOnChanges(changes: SimpleChanges): void {

    if (changes['chapters'] && !ArrayUtils.arraysEqual(changes['chapters'].previousValue, changes['chapters'].currentValue)) {
      this.chapterItems = this.chapters.map((c, i) => {
          return {
            label: (i + 1) + '. ' + c.name,
            ref: c.ref,
            originalItem: c,
          };
        });
    }
  }

  onItemClicked(item: SelectableItem<Chapter>) {
    this.chapterSelected.emit(item.originalItem);
  }
}
