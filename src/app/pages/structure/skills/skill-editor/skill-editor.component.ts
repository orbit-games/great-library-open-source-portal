import { Component, Input, OnChanges, OnDestroy, OnInit, Optional, SimpleChanges } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Course, Skill } from '../../../../@core/api';
import { NbDialogRef, NbDialogService } from '@nebular/theme';
// tslint:disable-next-line:max-line-length
import { ConfirmLeaveWithoutSavePopupComponent } from '../../../../@theme/components/confirm-leave-without-save-popup/confirm-leave-without-save-popup.component';
import { CourseService } from '../../../../@core/data/course.service';
import { SessionService } from '../../../../@core/session.service';
import { Subscription } from 'rxjs';
import { GlErrorHandlerService } from '../../../../@theme/error-handler.service';

@Component({
  selector: 'ngx-skill-editor',
  templateUrl: './skill-editor.component.html',
  styleUrls: ['./skill-editor.component.scss'],
})
export class SkillEditorComponent implements OnInit, OnDestroy, OnChanges {

  @Input() skill: Skill;
  @Input() isNew: boolean = false;

  saving: boolean = false;

  private courseRef: string;
  private courseChangedSubscription: Subscription;

  skillForm: FormGroup;

  constructor(
    private fb: FormBuilder,
    private sessionService: SessionService,
    private courseService: CourseService,
    private errorHandler: GlErrorHandlerService,
    @Optional() public dialogRef: NbDialogRef<any>,
    private dialogService: NbDialogService,
  ) {

  }

  ngOnInit() {
    this.createSkillForm();

    this.courseChangedSubscription = this.sessionService.getSelectedCourse().subscribe(c => this.courseRef = c.ref);
  }

  ngOnDestroy() {
    this.courseChangedSubscription.unsubscribe();
  }


  ngOnChanges(changes: SimpleChanges): void {
    if (changes.skill && changes.skill.currentValue !== changes.skill.previousValue) {
      this.skillForm.patchValue(this.skill);
    }
  }

  createSkillForm() {

    const skill = this.skill || {name: ''};

    this.skillForm = this.fb.group({
      'ref': skill.ref,
      'name': this.fb.control(skill.name, [ Validators.required ]),
      'description': this.fb.control(skill.description),
      'icon': skill.icon,
    });
  }

  onSaveClicked() {

    this.saving = true;

    if (this.isNew || this.skill == null) {

      this.courseService.createSkill(this.courseRef, this.skillFromForm())
        .then((s: Skill) => {
          this.saving = false;
          this.dialogRef.close(s);
        })
        .catch(e => {
          this.saving = false;
          this.errorHandler.handle(e);
        });

    } else {

      this.courseService.updateSkill(this.courseRef, this.skill.ref, this.skillFromForm())
        .then((s: Skill) => {
          this.saving = false;
          this.dialogRef.close(s);
        })
        .catch(e => {
          this.saving = false;
          this.errorHandler.handle(e);
        });
    }
  }

  skillFromForm() {

    return this.skillForm.value;
  }

  onCancelClicked() {

    if (this.skillForm.pristine) {
      this.dialogRef.close();
    } else {
      const confirmCancelDialog = this.dialogService.open(ConfirmLeaveWithoutSavePopupComponent);
      confirmCancelDialog.onClose.subscribe(d => {
        if (confirmCancelDialog.componentRef.instance.confirmed) {
          this.dialogRef.close();
        }
      });
    }
  }
}
