import {Component, ElementRef, forwardRef, Input, OnChanges, OnInit, SimpleChanges, ViewChild} from '@angular/core';
import {ControlValueAccessor, NG_VALUE_ACCESSOR} from '@angular/forms';
import {Skill} from '../../../../@core/api';
import { NbAdjustment, NbDialogService, NbPosition, NbPositionBuilderService, NbSelectComponent } from '@nebular/theme';
import { CompleterData, CompleterService } from 'ng2-completer';
import { Subject } from 'rxjs';
import { map, tap } from 'rxjs/operators';
import { SkillEditorComponent } from '../skill-editor/skill-editor.component';

@Component({
  selector: 'ngx-skill-selection',
  templateUrl: './skill-selection.component.html',
  styleUrls: ['./skill-selection.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => SkillSelectionComponent),
      multi: true,
    },
  ],
})
export class SkillSelectionComponent implements OnInit, OnChanges, ControlValueAccessor {

  @Input() allSkills: Skill[];
  @Input() status: string;

  @ViewChild('selectComponent', {static: true}) selectComponent: NbSelectComponent<Skill>;

  skillsChanges = new Subject<Skill[]>();

  completerData: CompleterData;

  selectedSkill: Skill;
  selectedSkillRef: string;

  onChange: (_: any) => void;
  disabled: boolean = false;

  constructor(
    private dialogService: NbDialogService,
    private completerService: CompleterService,
  ) {
    this.completerData = this.completerService.local(this.skillsChanges.asObservable(), 'name', 'name');
  }

  ngOnInit() {
  }

  ngOnChanges(changes: SimpleChanges): void {

    if (changes['allSkills']) {
      this.skillsChanges.next(this.allSkills);
      this.selectedSkill = this.allSkills.find(s => s.ref === this.selectedSkillRef);
    }
  }

  registerOnChange(fn: (_: any) => void): void {
    this.onChange = fn;
  }

  registerOnTouched(fn: any): void {
  }

  setDisabledState(isDisabled: boolean): void {
    this.disabled = isDisabled;
  }

  writeValue(obj: any): void {
    this.selectedSkillRef = obj;
    if (this.allSkills) {
      this.selectedSkill = this.allSkills.find(s => s.ref === this.selectedSkillRef);
    }
  }

  onSelectedSkillChanged(skill: Skill) {

    if (skill !== this.selectedSkill) {
      this.onChange(skill.ref);

      this.selectedSkill = skill;
      this.selectedSkillRef = skill.ref;
    }
  }

  onEditSkillClicked() {
    const editDialogRef = this.dialogService.open(SkillEditorComponent, {
      context:
        {
          skill: this.selectedSkill,
        },
      autoFocus: true,
      closeOnBackdropClick: false,
      closeOnEsc: false,
    });
    editDialogRef.onClose.subscribe(e => {
      if (e) {
        const skillIndex = this.allSkills.findIndex(s => s.ref === this.selectedSkillRef);
        this.allSkills[skillIndex] = e;
        this.skillsChanges.next(this.allSkills);

        this.selectedSkill = e;
      }
    });
  }

  onAddSkillClicked() {

    const addDialogRef = this.dialogService.open(SkillEditorComponent, {
      context:
        {
          isNew: true,
        },
      autoFocus: true,
      closeOnBackdropClick: false,
      closeOnEsc: false,
    });
    addDialogRef.onClose.subscribe(e => {
      if (e) {
        this.allSkills.push(e);

        this.onSelectedSkillChanged(e);
      }
    });
  }
}
