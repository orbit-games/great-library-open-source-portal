import { Component, HostListener, OnDestroy, OnInit } from '@angular/core';
import { CourseService } from '../../../../@core/data/course.service';
import { SessionService } from '../../../../@core/session.service';
import { GlErrorHandlerService } from '../../../../@theme/error-handler.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Observable, Subscription } from 'rxjs';
import { Course } from '../../../../@core/api';
import { RxwebValidators } from '@rxweb/reactive-form-validators';
import { FormControlUtil } from '../../../../@theme/util/FormControlUtil';

@Component({
  selector: 'ngx-course-editor',
  templateUrl: './course-editor.component.html',
  styleUrls: ['./course-editor.component.scss'],
})
export class CourseEditorComponent implements OnInit, OnDestroy {

  course: Course;
  courseUpdateForm: FormGroup;
  saving: boolean = false;

  courseChangedSubscription: Subscription;

  readonly controlStatus = FormControlUtil.controlStatus;

  constructor(
    private courseService: CourseService,
    private sessionService: SessionService,
    private fb: FormBuilder,
    private errorHandler: GlErrorHandlerService,
  ) { }

  ngOnInit() {

    this.sessionService.getSelectedCourse().subscribe(c => this.refreshCourse(c));
    this.courseChangedSubscription =  this.sessionService.selectedCourseChanged.subscribe(c => this.refreshCourse(c));

    this.buildCourseUpdateForm();

    this.sessionService.setCurrentPage({
      icon: 'fa fa-cogs',
      title: 'Course',
    });
  }

  private buildCourseUpdateForm() {

    const course = this.course || {};

    this.courseUpdateForm = this.fb.group({
      'name': this.fb.control(course.name, [Validators.required]),
      'year': this.fb.control(course.year, [Validators.required, RxwebValidators.numeric({allowDecimal: false})]),
      'quarter': this.fb.control(course.quarter, [
        Validators.required,
        Validators.min(0),
        Validators.max(6),
        RxwebValidators.numeric({
          allowDecimal: false,
        })]),
      'code': this.fb.control(course.code, [Validators.required]),
      'anonymousUsers': this.fb.control(course.anonymousUsers, [Validators.required]),
      'anonymousReviews': this.fb.control(course.anonymousReviews, [Validators.required]),
    });
  }

  ngOnDestroy(): void {
    this.courseChangedSubscription.unsubscribe();
  }

  refreshCourse(course: Course) {
    this.course = course;

    if (this.courseUpdateForm) {
      this.courseUpdateForm.patchValue({
        name: course.name,
        year: course.year,
        quarter: course.quarter,
        code: course.code,
        anonymousUsers: course.anonymousUsers,
        anonymousReviews: course.anonymousReviews,
      });
    }
  }

  saveChanges() {
    // TODO implement
    this.saving = true;
    this.courseService.updateCourse(this.course.ref, this.courseUpdateForm.value)
      .then(c => {
        this.saving = false;
        this.refreshCourse(c);
        this.sessionService.refreshCourse();
        this.sessionService.refreshLoggedInUser();
        this.courseUpdateForm.markAsPristine();
      })
      .catch(e => {
        this.saving = false;
        this.errorHandler.handle(e);
      });
  }


  // @HostListener allows us to also guard against browser refresh, close, etc.
  @HostListener('window:beforeunload')
  canDeactivate(): boolean | Observable<boolean> {
    return this.courseUpdateForm.pristine ;
  }

}
