import { Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges } from '@angular/core';
import { Course, CourseResource, Quiz, Section } from '../../../../@core/api';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { SessionService } from '../../../../@core/session.service';
import { CourseService } from '../../../../@core/data/course.service';
import { NbDialogService } from '@nebular/theme';
import { GlErrorHandlerService } from '../../../../@theme/error-handler.service';
import { ComponentCanDeactivate } from '../../../../@theme/guards/pending-changes.guard';
import { Observable } from 'rxjs';
import { ResourceStructureService } from '../resource-structure.service';
import { ConfirmDeleteDialogComponent } from '../../../../@theme/components/confirm-delete-dialog/confirm-delete-dialog.component';
import { GlHelper } from '../../../../@core/data/gl-helper.class';
import { RxwebValidators } from '@rxweb/reactive-form-validators';
import { FormControlUtil } from '../../../../@theme/util/FormControlUtil';
import { ServerService } from '../../../../@core/data/server.service';

enum ResourceType {
  TEXT = 'TEXT',
  URL = 'URL',
  FILE_UPLOAD = 'FILE_UPLOAD',
}

@Component({
  selector: 'ngx-resource-editor',
  templateUrl: './resource-editor.component.html',
  styleUrls: ['./resource-editor.component.scss'],
})
export class ResourceEditorComponent implements OnInit, OnChanges, ComponentCanDeactivate {

  static readonly SIZE_SMALL = 'small';
  static readonly SIZE_MEDIUM = 'medium';
  static readonly SIZE_LARGE = 'large';

  @Input('resourceRef') resourceRef: string;
  @Input('isNew') isNew: boolean = false;
  @Input() size: string = ResourceEditorComponent.SIZE_LARGE;

  @Output() saved = new EventEmitter<Section>();

  saving = false;

  course: Course;
  resource: CourseResource;

  resourceForm: FormGroup;

  maxContentFileSize: number;

  get resourceType(): ResourceType {
    return this.resourceForm
      ? this.resourceForm.get('resourceType').value
      : ResourceEditorComponent.tryDetermineResourceType(this.resource);
  }

  get small() { return this.size === ResourceEditorComponent.SIZE_SMALL; }
  get medium() { return this.size === ResourceEditorComponent.SIZE_MEDIUM; }
  get large() { return this.size === ResourceEditorComponent.SIZE_LARGE; }

  constructor(
    private sessionService: SessionService,
    private courseService: CourseService,
    private serverService: ServerService,
    private fb: FormBuilder,
    private resourceStructureService: ResourceStructureService,
    private dialogService: NbDialogService,
    private errorHandler: GlErrorHandlerService,
  ) { }

  ngOnInit() {

    this.sessionService.getSelectedCourse().subscribe(c => {
      this.course = c;
      this.refreshResource();
    });
    this.serverService.getServerInfo()
      .then(s => this.maxContentFileSize = s.contentUploadMaxSize);
    this.constructResourceFormGroup();
  }

  ngOnChanges(changes: SimpleChanges): void {

    if (changes.resourceRef && changes.resourceRef.previousValue !== changes.resourceRef.currentValue) {
      this.refreshResource();
    }
  }

  refreshResource() {

    if (this.course == null || this.resourceRef == null) {

      this.resource = null;
      if (this.resourceForm) {
        this.resourceForm.get('resourceType').setValue(ResourceEditorComponent.tryDetermineResourceType(null));
      }

    } else {
      this.resource = null;
      this.courseService.getCourseResource(this.course.ref, this.resourceRef, true)
        .then(r => {
          this.resource = r;

          if (this.resourceForm) {
            this.resourceForm.get('resourceType').setValue(ResourceEditorComponent.tryDetermineResourceType(r));
          }

          this.constructResourceFormGroup();
        });
    }
  }

  static tryDetermineResourceType(resource: CourseResource) {

    if (resource == null) {
      return ResourceType.TEXT;
    }

    if (resource.fileUploadMetadata != null) {
      return ResourceType.FILE_UPLOAD;
    } else if (resource.url != null) {
      return ResourceType.URL;
    } else {
      return ResourceType.TEXT;
    }
  }

  constructResourceFormGroup() {

    const resource = (this.resource || {} as CourseResource);
    this.resourceForm = this.fb.group({
      'ref': this.fb.control(resource.ref),
      'name': this.fb.control(resource.name, [Validators.required]),
      'resourceType': ResourceEditorComponent.tryDetermineResourceType(resource),
      'description': resource.description,
      'url': this.fb.control(ResourceEditorComponent.tryDetermineResourceType(resource) === ResourceType.FILE_UPLOAD ? null : resource.url,
        [
          RxwebValidators.required({ conditionalExpression: x => {
            return x.resourceType === ResourceType.URL;
            }}),
          Validators.pattern(/https?:\/\/(www\.)?[-a-zA-Z0-9@:%._\+~#=]{1,256}\.[a-zA-Z0-9()]{1,6}\b([-a-zA-Z0-9()@:%_\+.~#?&//=]*)/),
        ]),
      'fileUploadMetadata': this.fb.control(resource.fileUploadMetadata,
        [
          RxwebValidators.required({ conditionalExpression: x => x.resourceType === ResourceType.FILE_UPLOAD}),
        ]),
      // 'quiz': resource.quiz,
      'quizMinimalScore': this.fb.control(resource.quizMinimalScore,
        [
          RxwebValidators.numeric({allowDecimal: false}),
        ]),
      'skillRef': this.fb.control(resource.skills && resource.skills.length > 0 ? resource.skills[0] : null),
    }, { validators: [ validateMinimalQuizScore ]});

    if (resource.quiz) {
      this.addQuizControl(resource.quiz);
    }
  }

  onAddQuizClicked() {
    this.addQuizControl({
      name: '',
      description: '',
      maxScore: 0,
      elements: [],
    });
    this.resourceForm.updateValueAndValidity();
  }

  addQuizControl(quiz: Quiz) {
    this.resourceForm.addControl('quiz', this.fb.control(quiz));
  }

  onRemoveQuizClicked() {

    const dialog = this.dialogService.open(ConfirmDeleteDialogComponent, {
      context: {
        'title': `Confirm deleting the quiz`,
        'content': `Are you sure you want to delete the quiz associated with this resource?`,
      },
    });
    dialog.onClose.subscribe(d => {
      if (d) {
        this.resourceForm.removeControl('quiz');
        this.resourceForm.markAsDirty();
      }
    });
    this.resourceForm.updateValueAndValidity();
  }

  get maxQuizScore(): number {
    const quiz = this.resourceForm.controls.quiz.value;
    return GlHelper.calculateQuizMaxScore(quiz);
  }

  saveChanges() {

    this.saving = true;

    if (this.isNew) {

      const newResource: CourseResource = {
        order: null,
        name: '',
      };

      this.setFormValuesToResource(newResource);

      this.courseService.createCourseResource(this.course.ref, newResource)
        .then((r: CourseResource) => {

          this.saving = false;
          this.resourceForm.markAsPristine();
          this.sessionService.refreshCourse();
          this.resourceStructureService.addResource(r);
          this.saved.emit(r);

        })
        .catch(e => {
          this.saving = false;
          this.errorHandler.handle(e);
        });

    } else {

      this.setFormValuesToResource(this.resource);

      this.courseService.updateCourseResource(this.course.ref, this.resourceRef, this.resource)
        .then((r: CourseResource) => {
          this.saving = false;
          this.saved.emit(r);
          this.refreshResource();
          this.resourceStructureService.updateResource(r);
          this.sessionService.refreshCourse();
        })
        .catch(e => {
          this.saving = false;
          this.errorHandler.handle(e);
        });
    }
  }

  private setFormValuesToResource(resource: CourseResource) {

    // Copy the editable fields:
    const resourceFormValue = this.resourceForm.value;
    resource.name = resourceFormValue.name;
    resource.description = resourceFormValue.description;

    resource.url = null;
    resource.fileUploadMetadata = null;

    if (resourceFormValue.resourceType === ResourceType.FILE_UPLOAD) {

      resource.fileUploadMetadata = resourceFormValue.fileUploadMetadata;

    } else if (resourceFormValue.resourceType === ResourceType.URL) {

      resource.url = resourceFormValue.url;
    }

    resource.quiz = resourceFormValue.quiz;
    resource.quizMinimalScore = resourceFormValue.quizMinimalScore;
    resource.skills = resourceFormValue.skillRef ? [resourceFormValue.skillRef] : [];
  }

  get invalidMinimalQuizScore(): boolean {
    return this.resourceForm.errors && 'validateMinimalQuizScore' in this.resourceForm.errors;
  }

  canDeactivate(): (boolean | Observable<boolean>) {
    return this.resourceForm.pristine || this.resourceForm.value === this.resource;
  }

  controlStatus = FormControlUtil.controlStatus;
}

function validateMinimalQuizScore(resourceForm: FormGroup): {[error: string]: any } {

  if (!resourceForm || !resourceForm.controls.quiz || !resourceForm.controls.quizMinimalScore) {
    return;
  }

  const quiz = resourceForm.controls.quiz.value;
  const quizMinimalScore = resourceForm.controls.quizMinimalScore.value;

  if (!quiz || (quizMinimalScore != null && quizMinimalScore !== '' && Number(quizMinimalScore) <= GlHelper.calculateQuizMaxScore(quiz))) {
    return null;
  }
  return {
    validateMinimalQuizScore: {
      valid: false,
    },
  };
}
