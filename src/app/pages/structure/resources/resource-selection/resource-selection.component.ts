import {Component, forwardRef, Input, OnInit} from '@angular/core';
import {ControlValueAccessor, NG_VALUE_ACCESSOR} from '@angular/forms';
import {CourseResource} from '../../../../@core/api';
import {ResourceStructureService} from '../resource-structure.service';

@Component({
  selector: 'ngx-resource-selection',
  templateUrl: './resource-selection.component.html',
  styleUrls: ['./resource-selection.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => ResourceSelectionComponent),
      multi: true,
    },
    ResourceStructureService,
  ],
})
export class ResourceSelectionComponent implements OnInit, ControlValueAccessor {

  @Input('resources') resources: CourseResource[];

  selectedResourceRef: string;
  disabled = false;

  propagateChange = (_: any) => {};

  constructor() { }

  ngOnInit() {
  }

  registerOnChange(fn: any): void {
    this.propagateChange = fn;
  }

  registerOnTouched(fn: any): void {
  }

  setDisabledState(isDisabled: boolean): void {
    this.disabled = isDisabled;
  }

  writeValue(obj: any): void {
    this.selectedResourceRef = obj;
  }

  onSelectResource(resource: CourseResource) {
    this.selectedResourceRef = resource.ref;
    this.propagateChange(resource.ref);
  }

}
