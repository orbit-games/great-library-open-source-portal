import {Component, HostListener, OnInit, ViewChild} from '@angular/core';
import {ResourceEditorComponent} from '../resource-editor/resource-editor.component';
import {CourseResource} from '../../../../@core/api';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'ngx-new-resource-editor',
  template: `
    <ngx-resource-editor [isNew]="true" (saved)="onResourceSaved($event)" #resourceEditorComponent></ngx-resource-editor>
  `,
  styles: [],
})
export class NewResourceEditorComponent implements OnInit {

  @ViewChild('resourceEditorComponent', {static: false}) resourceEditor: ResourceEditorComponent;

  constructor(private router: Router,
              private route: ActivatedRoute) { }

  ngOnInit() {
  }

  onResourceSaved(resource: CourseResource) {
    this.router.navigate(['..', resource.ref], { relativeTo: this.route });
  }

  // @HostListener allows us to also guard against browser refresh, close, etc.
  @HostListener('window:beforeunload')
  canDeactivate = () => this.resourceEditor.canDeactivate()

}
