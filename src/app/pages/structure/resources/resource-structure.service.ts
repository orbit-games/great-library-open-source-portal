import {Injectable} from '@angular/core';
import {Subject} from 'rxjs';
import {CourseResource} from '../../../@core/api';

@Injectable()
export class ResourceStructureService {

  // Observable sources
  private resourceAddedSource = new Subject<CourseResource>();
  private resourceUpdatedSource = new Subject<CourseResource>();

  // Observable streams
  resourceAdded$ = this.resourceAddedSource.asObservable();
  resourceUpdated$ = this.resourceUpdatedSource.asObservable();

  constructor() { }

  addResource(resource: CourseResource) {
    this.resourceAddedSource.next(resource);
  }

  updateResource(resource: CourseResource) {
    this.resourceUpdatedSource.next(resource);
  }
}
