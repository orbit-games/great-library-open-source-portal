import {ChangeDetectorRef, Component, OnInit} from '@angular/core';
import {Course, CourseResource} from '../../../@core/api';
import {GlComparer} from '../../../@core/data/gl-comparer';
import {SessionService} from '../../../@core/session.service';
import {CourseService} from '../../../@core/data/course.service';
import {GlErrorHandlerService} from '../../../@theme/error-handler.service';
import {ActivatedRoute, Router} from '@angular/router';
import {ResourceStructureService} from './resource-structure.service';

@Component({
  selector: 'ngx-resources',
  templateUrl: './resources.component.html',
  styleUrls: ['./resources.component.scss'],
  providers: [ ResourceStructureService ],
})
export class ResourcesComponent implements OnInit {

  course: Course;
  resourceRef: string;
  editable = false;

  _allResources: CourseResource[];

  set allResources(resources: CourseResource[]) {
    this._allResources = resources.sort((r1, r2) => GlComparer.compareResources(r1, r2));
  }
  get allResources(): CourseResource[] {
    return this._allResources;
  }

  constructor(private sessionService: SessionService,
              private courseService: CourseService,
              private resourceStructureService: ResourceStructureService,
              private errorHandler: GlErrorHandlerService,
              private changeDetectorRef: ChangeDetectorRef,
              private router: Router, private route: ActivatedRoute) {

    this.route.url.subscribe(() => {
      if (this.route.snapshot.children && this.route.snapshot.children[0]) {
        this.resourceRef = this.route.snapshot.children[0].params['resourceRef'];
      }
    });
  }

  ngOnInit() {
    this.sessionService.getSelectedCourse().subscribe(c => this.setCourse(c));
    this.sessionService.selectedCourseChanged.subscribe(c => this.setCourse(c));

    this.sessionService.getLoggedInUser().subscribe(u => {
      this.editable = u.role !== 'LEARNER';
    });

    this.sessionService.setCurrentPage({
      icon: 'fas fa-book-open',
      title: 'Resources',
    });

    this.resourceStructureService.resourceAdded$.subscribe((r: CourseResource) => {
      if (this.allResources) {

        this.allResources = [r].concat(this.allResources);

        // HACK: manually trigger change detection because angular doesn't notice the array changes for some reason...
        this.changeDetectorRef.detectChanges();
        this.resourceRef = r.ref;
      }
    });

    this.resourceStructureService.resourceUpdated$.subscribe((r: CourseResource) => {

      // We need a cloned array in order for change detection to work
      const updatedResourceList = [].concat(this.allResources);
      const existingResourceIndex = updatedResourceList.findIndex(er => er.ref === r.ref);
      updatedResourceList[existingResourceIndex] = r;
      this.allResources = updatedResourceList;

      // HACK: manually trigger change detection because angular doesn't notice the array changes for some reason...
      this.changeDetectorRef.detectChanges();
      this.resourceRef = r.ref;
    });
  }

  setCourse(course: Course) {
    this.course = course;
    this.allResources = course.resources;
  }

  onNewResourceClicked() {
    this.tryOpenResource(null);
  }

  onResourceSelected(resource: CourseResource) {
    this.tryOpenResource(resource.ref);
  }

  tryOpenResource(resourceRef: string) {

    const oldResourceRef = this.resourceRef;
    this.resourceRef = resourceRef;
    this.router.navigate([resourceRef == null ? 'new' : resourceRef], { relativeTo: this.route })
      .then(routed => {
        if (routed) {
          this.resourceRef = resourceRef;
        } else {
          this.resourceRef = oldResourceRef;
        }
      });
  }
}
