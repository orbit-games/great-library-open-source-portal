import {Component, HostListener, OnInit, ViewChild} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {ResourceEditorComponent} from '../resource-editor/resource-editor.component';

@Component({
  selector: 'ngx-routed-resource-editor',
  template: `<ngx-resource-editor [resourceRef]="resourceRef" #resourceEditorComponent></ngx-resource-editor>`,
  styles: [],
})
export class RoutedResourceEditorComponent implements OnInit {

  resourceRef: string;

  @ViewChild('resourceEditorComponent', {static: false}) resourceEditor: ResourceEditorComponent;

  constructor(
    private route: ActivatedRoute,
  ) {}

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.resourceRef = params['resourceRef'];
    });
  }

  // @HostListener allows us to also guard against browser refresh, close, etc.
  @HostListener('window:beforeunload')
  canDeactivate = () => this.resourceEditor.canDeactivate()

}
