import {
  AfterViewInit,
  Component, ElementRef,
  EventEmitter,
  Input,
  OnChanges,
  OnInit,
  Output,
  QueryList,
  SimpleChanges,
  ViewChildren,
} from '@angular/core';
import {CourseResource} from '../../../../@core/api';

@Component({
  selector: 'ngx-resource-list',
  templateUrl: './resource-list.component.html',
  styleUrls: ['./resource-list.component.scss'],
})
export class ResourceListComponent implements OnInit, OnChanges, AfterViewInit {

  static readonly SIZE_SMALL = 'small';
  static readonly SIZE_MEDIUM = 'medium';
  static readonly SIZE_LARGE = 'large';

  @Input() resources: CourseResource[];
  @Input() selectedResourceRef: string;

  @Input() size: string = 'large';

  @Output() resourceSelected = new EventEmitter<CourseResource>();

  @ViewChildren('resourceListItem') resourceListItems: QueryList<ElementRef>;

  disabled = false;
  filter: string = '';

  filteredResources: CourseResource[];

  get small() { return this.size === ResourceListComponent.SIZE_SMALL; }
  get medium() { return this.size === ResourceListComponent.SIZE_MEDIUM; }
  get large() { return this.size === ResourceListComponent.SIZE_LARGE; }

  constructor() { }

  ngOnInit() {
  }

  ngOnChanges(changes: SimpleChanges): void {
    this.updateFilteredResources();

    if (changes['selectedResourceRef'] != null) {
      this.tryScrollToSelectedItem();
    }
  }

  ngAfterViewInit() {
    this.tryScrollToSelectedItem();
  }

  tryScrollToSelectedItem() {

    if (this.resourceListItems == null || this.selectedResourceRef == null) return;

    const selectedResourceIndex = this.filteredResources.findIndex(s => s.ref === this.selectedResourceRef);
    if (selectedResourceIndex > 0 && selectedResourceIndex < this.resourceListItems.length) {
      this.resourceListItems.toArray()[selectedResourceIndex].nativeElement.scrollIntoView({block: 'center'});
    }
  }


  private updateFilteredResources() {
    if (this.filter == null) {
      this.filteredResources = this.resources;
    } else {
      const lowerCaseFilter = this.filter.toLowerCase();
      this.filteredResources = this.resources.filter(r => r.name.toLowerCase().includes(lowerCaseFilter));
    }
  }

  onSelectResource(resource: CourseResource) {
    this.resourceSelected.emit(resource);
  }
}
