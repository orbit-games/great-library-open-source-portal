import {Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges, TemplateRef, ViewChild} from '@angular/core';
import {Course, CourseResource, ResourceRelation, Section} from '../../../../@core/api';
import {CourseService} from '../../../../@core/data/course.service';
import {SessionService} from '../../../../@core/session.service';
import {FormArray, FormBuilder, FormGroup, Validators} from '@angular/forms';
import {QuillEditorComponent} from 'ngx-quill';
import {NbDialogRef, NbDialogService} from '@nebular/theme';
import {GlErrorHandlerService} from '../../../../@theme/error-handler.service';
import {SectionsStructureService} from '../sections-structure.service';
import {ComponentCanDeactivate} from '../../../../@theme/guards/pending-changes.guard';
import {Observable} from 'rxjs';
import {FormControlUtil} from '../../../../@theme/util/FormControlUtil';
import { ConfirmDeleteDialogComponent } from '../../../../@theme/components/confirm-delete-dialog/confirm-delete-dialog.component';

@Component({
  selector: 'ngx-section-editor',
  templateUrl: './section-editor.component.html',
  styleUrls: ['./section-editor.component.scss'],
})
export class SectionEditorComponent implements OnInit, OnChanges, ComponentCanDeactivate {

  @Input('sectionRef') sectionRef: string;
  @Input('isNew') isNew: boolean = false;

  @Input('newSectionSortOrder') newSectionSortOrder: number = 0;
  @Input('newSectionParentRef') newSectionParentRef: string = null;
  @Output() saved = new EventEmitter<Section>();
  @Output() deleted = new EventEmitter<Section>();

  saving = false;

  course: Course;
  section: Section;
  parentSectionName: string;

  sectionForm: FormGroup;

  selectedNewResource: string;

  readonly controlStatus = FormControlUtil.controlStatus;

  @ViewChild('editor', {static: false}) editor: QuillEditorComponent;

  @ViewChild('confirmDeleteResourceRelationDialog', {static: true}) confirmDeleteResourceRelationDialog: TemplateRef<any>;
  @ViewChild('selectResourceDialog', {static: true}) selectResourceDialog: TemplateRef<any>;

  constructor(
    private sessionService: SessionService,
    private courseService: CourseService,
    private sectionsStructureService: SectionsStructureService,
    private fb: FormBuilder,
    private dialogService: NbDialogService,
    private errorHandler: GlErrorHandlerService,
  ) { }

  ngOnInit() {
    this.sessionService.getSelectedCourse().subscribe(c => {
      this.course = c;
      this.refreshSection();
    });
    this.constructSectionFormGroup();
  }

  constructSectionFormGroup() {

    this.sectionForm = this.fb.group({
      'name': this.fb.control((this.section || {} as Section).name, [Validators.required]),
      'description': (this.section || {} as Section).description,
      'resourceRelations': this.fb.array(((this.section || {} as Section).resourceRelations || [] as ResourceRelation[])
        .map(r => this.constructResourceRelationFormGroup(r))),
      'parentRef': (this.section || {} as Section).parentRef,
      'parentRelationDescription': (this.section || {} as Section).parentRelationDescription,
      'sortOrder': (this.section || {} as Section).sortOrder,
    });
  }

  constructResourceRelationFormGroup(r: ResourceRelation): FormGroup {
    return this.fb.group({
      'resourceRef': r.resourceRef,
      'description': r.description,
      'optional': r.optional,
      'sortOrder': r.sortOrder,
    });
  }

  ngOnChanges(changes: SimpleChanges): void {

    const sectionRefChange = changes['sectionRef'];
    if (sectionRefChange && sectionRefChange.currentValue !== sectionRefChange.previousValue) {
      // TODO Ask the user if (s)he wants to save changes to the current section (if any)
      this.refreshSection();
    }
  }

  refreshSection(forceRefreshFromServer: boolean = true) {

    if (this.isNew || this.course == null || this.sectionRef == null) {
      this.section = null;
    } else {
      this.section = null;
      this.courseService.getSection(this.course.ref, this.sectionRef, forceRefreshFromServer)
        .then(s => {
          this.section = s;
          if (s.parentRef) {
            this.parentSectionName = this.course.sections.find(ps => ps.ref === s.parentRef).name;
          } else {
            this.parentSectionName = null;
          }

          this.constructSectionFormGroup();
        });
    }
  }

  getResource(resourceRef: string): CourseResource {
    return this.course.resources.find(r => r.ref === resourceRef);
  }

  get resourceRelationsArray(): FormArray {
    return this.sectionForm.controls.resourceRelations as FormArray;
  }

  get unusedResources(): CourseResource[] {

    const usedResources = new Set<string>(this.resourceRelationsArray.controls.map(c => c.value.resourceRef));
    return this.course.resources.filter(r => !usedResources.has(r.ref));
  }

  onDeleteResourceRelationClicked(resourceIndex: number) {

    this.dialogService.open(this.confirmDeleteResourceRelationDialog, {
      context: {
        resourceIndex,
        resourceRef: this.resourceRelationsArray.at(resourceIndex).value.resourceRef,
      },
    });
  }

  onConfirmDeleteResourceRelation(resourceIndex: number, dialogRef: NbDialogRef<any>) {
    this.resourceRelationsArray.removeAt(resourceIndex);
    this.resourceRelationsArray.markAsDirty();
    dialogRef.close();
  }


  onAddResourceClicked() {
    this.dialogService.open(this.selectResourceDialog, {
      context: {},
      closeOnBackdropClick: false,
      closeOnEsc: false,
    });
  }

  onConfirmAddResource(selectedNewResource: string, dialogRef: NbDialogRef<any>) {

    this.resourceRelationsArray.push(this.constructResourceRelationFormGroup({
      resourceRef: selectedNewResource,
      description: '',
      optional: true,
      sortOrder: this.resourceRelationsArray.length * 10,
      }));
    this.resourceRelationsArray.markAsDirty();
    dialogRef.close();
  }


  onDeleteSectionClicked() {
    const dialogRef = this.dialogService.open(ConfirmDeleteDialogComponent, {
      context: {
        title: `Confirm deleting section "${this.section.name}"`,
        content: `Are you sure you want to delete section "${this.section.name}"?
        This will permanently remove the section and it's subsections from the course content`,

      },
    });
    dialogRef.onClose.subscribe(d => {
      if (d) {
        this.deleteSection();
      }
    });
  }

  deleteSection() {

    if (this.isNew || this.section == null) {
      return;
    }

    this.saving = true;

    this.courseService.deleteSection(this.course.ref, this.section.ref)
      .then(s => {
        this.saving = false;
        this.sectionForm.markAsPristine();
        this.sessionService.refreshCourse();
        this.sectionsStructureService.deleteSection(s);
        this.deleted.emit(s);

      })
      .catch(e => {
        this.saving = false;
        this.errorHandler.handle(e);
      });

  }

  saveChanges() {

    this.saving = true;

    if (this.isNew) {

      const newSection: Section = {
        description: null, name: null, parentRelationDescription: null, ref: null, resourceRelations: [],
        parentRef: this.newSectionParentRef,
        sortOrder: this.newSectionSortOrder,
      };
      this.setFormValuesToSection(newSection);

      this.courseService.createSection(this.course.ref, newSection)
        .then(s => {

          this.saving = false;
          this.sectionForm.markAsPristine();
          this.sessionService.refreshCourse();
          this.sectionsStructureService.addSection(s);
          this.saved.emit(s);

        }).catch(e => {
          this.saving = false;
          this.errorHandler.handle(e);
        });

    } else {

      // To ensure that we don't override the order and parent of this section when updating it, we first get the latest section.
      this.courseService.getSection(this.course.ref, this.sectionRef, true)
        .then(s => {

          // TODO: Show an error/warning if someone edited the one of the section form fields while we were on this page
          this.setFormValuesToSection(s);

          return this.courseService.updateSection(this.course.ref, this.sectionRef, s);
        })
        .then(s => {
          this.saving = false;
          this.saved.emit(s);
          this.refreshSection();
          this.sectionsStructureService.updateSection(s);
          this.sessionService.refreshCourse();
        })
        .catch(e => {
          this.saving = false;
          this.errorHandler.handle(e);
        });
    }
  }

  private setFormValuesToSection(section: Section) {
    // Copy the editable fields:
    const sectionFormValue = this.sectionForm.value;
    section.name = sectionFormValue.name;
    section.description = sectionFormValue.description;
    section.resourceRelations = sectionFormValue.resourceRelations;
    section.parentRelationDescription = sectionFormValue.parentRelationDescription;
  }

  canDeactivate(): boolean | Observable<boolean> {
    return this.sectionForm.pristine;
  }

}
