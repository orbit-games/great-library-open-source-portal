import {Component, HostListener, OnInit, ViewChild} from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import {ComponentCanDeactivate} from '../../../../@theme/guards/pending-changes.guard';
import {SectionEditorComponent} from '../section-editor/section-editor.component';
import { Section } from '../../../../@core/api';

@Component({
  selector: 'ngx-routed-section-editor',
  template: `
      <ngx-section-editor 
          [sectionRef]="sectionRef"
          (deleted)="sectionDeleted($event)"
          #sectionEditor>
          
      </ngx-section-editor>`,
})
export class RoutedSectionEditorComponent implements OnInit, ComponentCanDeactivate {

  sectionRef: string;

  @ViewChild('sectionEditor', {static: false}) sectionEditor: SectionEditorComponent;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
  ) {
    this.route.params.subscribe(params => {
      this.sectionRef = params['sectionRef'];
    });
  }

  ngOnInit() {
  }

  // @HostListener allows us to also guard against browser refresh, close, etc.
  @HostListener('window:beforeunload')
  canDeactivate = () => this.sectionEditor.canDeactivate()

  sectionDeleted(section: Section) {
    this.router.navigate(['..'], {relativeTo: this.route});
  }
}
