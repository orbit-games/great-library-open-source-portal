import { Injectable } from '@angular/core';
import {Subject} from 'rxjs';
import {Section} from '../../../@core/api';

@Injectable()
export class SectionsStructureService {

  // Observable sources
  private sectionAddedSource = new Subject<Section>();
  private sectionUpdatedSource = new Subject<Section>();
  private sectionDeletedSource = new Subject<Section>();

  // Observable streams
  sectionAdded$ = this.sectionAddedSource.asObservable();
  sectionUpdated$ = this.sectionUpdatedSource.asObservable();
  sectionDeleted$ = this.sectionDeletedSource.asObservable();

  constructor() { }

  addSection(section: Section) {
    this.sectionAddedSource.next(section);
  }

  updateSection(section: Section) {
    this.sectionUpdatedSource.next(section);
  }

  deleteSection(section: Section) {
    this.sectionDeletedSource.next(section);
  }
}
