import { Component, forwardRef, Input, OnChanges, OnInit, SimpleChanges } from '@angular/core';
import { Section } from '../../../../@core/api';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';

@Component({
  selector: 'ngx-section-selection',
  templateUrl: './section-selection.component.html',
  styleUrls: ['./section-selection.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => SectionSelectionComponent),
      multi: true,
    },
  ],
})
export class SectionSelectionComponent implements OnInit, OnChanges, ControlValueAccessor {

  @Input() sections: Section[];

  selectedSectionRef: string;
  selectedSection: Section;

  disabled: boolean = false;
  private _onChange: (_: any) => void;
  private _onTouched: any;

  constructor() { }

  ngOnInit() {
  }

  ngOnChanges(changes: SimpleChanges): void {

    if (
      (changes['selectedSectionRef'] && changes['selectedSectionRef'].previousValue !== changes['selectedSectionRef'].currentValue)
      || changes['sections']
    ) {
      this.selectedSection = this.sections.find(s => s.ref === this.selectedSectionRef);
    }
  }

  onSectionClicked(sectionRef: string) {
    this.selectedSectionRef = sectionRef;
    this.selectedSection = this.sections.find(s => s.ref === this.selectedSectionRef);

    if (this._onChange) {
      this._onChange(sectionRef);
    }
  }

  writeValue(sectionRef: string): void {
      this.selectedSectionRef = sectionRef;
  }

  registerOnChange(fn: (_: any) => void): void {
    this._onChange = fn;
  }

  registerOnTouched(fn: any): void {
    this._onTouched = fn;
  }

  setDisabledState?(isDisabled: boolean): void {
    this.disabled = isDisabled;
  }
}
