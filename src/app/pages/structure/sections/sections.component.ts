import {ChangeDetectorRef, Component, HostListener, OnInit, ViewChild} from '@angular/core';
import {Course, Section} from '../../../@core/api';
import {SessionService} from '../../../@core/session.service';
import {ActivatedRoute, Router} from '@angular/router';
import {SectionListComponent} from './section-list/section-list.component';
import {CourseService} from '../../../@core/data/course.service';
import {GlErrorHandlerService} from '../../../@theme/error-handler.service';
import {SectionsStructureService} from './sections-structure.service';
import {GlComparer} from '../../../@core/data/gl-comparer';
import {ComponentCanDeactivate} from '../../../@theme/guards/pending-changes.guard';
import {Observable} from 'rxjs';

@Component({
  selector: 'ngx-sections',
  templateUrl: './sections.component.html',
  styleUrls: ['./sections.component.scss'],
  providers: [ SectionsStructureService ],
})
export class SectionsComponent implements OnInit, ComponentCanDeactivate {

  course: Course;
  sectionRef: string;
  sectionsDirty: boolean = false;
  updatingSectionStructure = false;
  editable = false;

  _allSections: Section[];

  set allSections(sections: Section[]) {
    this._allSections = sections.sort((s1, s2) => GlComparer.compareSections(s1, s2));
  }
  get allSections(): Section[] {
    return this._allSections;
  }

  @ViewChild('sectionList', {static: false}) sectionList: SectionListComponent;

  constructor(private sessionService: SessionService,
              private courseService: CourseService,
              private sectionsStructureService: SectionsStructureService,
              private errorHandler: GlErrorHandlerService,
              private changeDetectorRef: ChangeDetectorRef,
              private router: Router, private route: ActivatedRoute) {

    this.route.url.subscribe(() => {
      if (this.route.snapshot.children && this.route.snapshot.children[0]) {
        this.sectionRef = this.route.snapshot.children[0].params['sectionRef'];
      }
    });
  }

  ngOnInit() {
    this.sessionService.getSelectedCourse().subscribe(c => this.setCourse(c));
    this.sessionService.selectedCourseChanged.subscribe(c => this.setCourse(c));

    this.sessionService.getLoggedInUser().subscribe(u => {
      this.editable = u.role !== 'LEARNER';
    });

    this.sessionService.setCurrentPage({
      icon: 'fas fa-book-open',
      title: 'Sections',
    });


    this.sectionsStructureService.sectionAdded$.subscribe(s => {
      if (this.allSections) {

        // We need a new array assignment/instance for change detection to work
        this.allSections = [s, ...this.allSections];

        // HACK: manually trigger change detection because angular doesn't notice the array changes...
        this.changeDetectorRef.detectChanges();
        this.sectionRef = s.ref;
      }
    });

    this.sectionsStructureService.sectionUpdated$.subscribe((s: Section) => {

      const existingSectionIndex = this.allSections.findIndex(es => es.ref === s.ref);
      this.allSections[existingSectionIndex] = s;

      // We need a new array assignment/instance for change detection to work
      this.allSections = [].concat(this.allSections);

      // HACK: manually trigger change detection because angular doesn't notice the array changes...
      this.changeDetectorRef.detectChanges();
      this.sectionRef = s.ref;
    });

    this.sectionsStructureService.sectionDeleted$.subscribe((s: Section) => {

      const existingSectionIndex = this.allSections.findIndex(es => es.ref === s.ref);
      this.allSections = this.allSections.splice(existingSectionIndex, 1);

      // We need a new array assignment/instance for change detection to work
      this.allSections = [].concat(this.allSections);

      // HACK: manually trigger change detection because angular doesn't notice the array changes...
      this.changeDetectorRef.detectChanges();
      this.sectionRef = null;
    });
  }

  setCourse(course: Course) {
    this.course = course;
    this.allSections = course.sections;
  }

  onSectionClicked(sectionRef: string) {
    this.tryOpenSection(sectionRef);
  }


  onCreateSectionClicked() {
    this.tryOpenSection(null);
  }

  tryOpenSection(sectionRef: string) {

    const oldSectionRef = this.sectionRef;
    this.sectionRef = sectionRef;
    this.router.navigate([sectionRef == null ? 'new' : sectionRef], { relativeTo: this.route })
      .then(routed => {
        if (routed) {
          this.sectionRef = sectionRef;
        } else {
          this.sectionRef = oldSectionRef;
        }
      });
  }

  onSectionMoved() {
    this.sectionsDirty = true;
  }

  saveStructureChanges() {

    this.updatingSectionStructure = true;

    const changedSections = this.sectionList.getChangedSections();

    const courseRef = this.course.ref;

    const sectionUpdatePromises = [];
    for (const sectionChange of changedSections) {
      sectionUpdatePromises.push(this.courseService.getSection(courseRef, sectionChange.section.ref, true)
        .then(s => {
          s.sortOrder = sectionChange.newSortOrder;
          s.parentRef = sectionChange.newParentRef;
          return this.courseService.updateSection(courseRef, s.ref, s);
        }));
    }
    Promise.all(sectionUpdatePromises)
      .then(_ => {
        this.sectionsDirty = false;
        this.updatingSectionStructure = false;
      })
      .catch(e => this.errorHandler.handle(e));
  }

  onSaveStructureChangesClicked() {
    this.saveStructureChanges();
  }

  // @HostListener allows us to also guard against browser refresh, close, etc.
  @HostListener('window:beforeunload')
  canDeactivate(): Observable<boolean> | boolean {
    return !this.editable || !this.sectionsDirty;
  }

}
