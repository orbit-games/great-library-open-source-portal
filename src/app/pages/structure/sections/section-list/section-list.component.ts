import {Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges, ViewChild} from '@angular/core';
import {Section} from '../../../../@core/api';
import {TreeComponent, TreeModel, TreeNode} from 'angular-tree-component';

interface SectionNode {
  id: string;
  name: string;
  children: SectionNode[];
  isExpanded: boolean;
}

export interface SectionStructureChange {
  /**
   * The old, existing section
   */
  section: Section;

  /**
   * New sort order
   */
  newSortOrder: number;

  /**
   * New parent (ref)
   */
  newParentRef: string;
}

@Component({
  selector: 'ngx-section-list',
  templateUrl: './section-list.component.html',
  styleUrls: ['./section-list.component.scss'],
})
export class SectionListComponent implements OnInit, OnChanges {

  @Input('sections') sections: Section[];
  @Input('editable') editable: boolean;
  @Input('selectedSectionRef') selectedSectionRef: string;

  @Output() sectionClicked = new EventEmitter<string>();
  @Output() sectionMoved = new EventEmitter<string>();

  @ViewChild('tree', {static: false}) tree: TreeComponent;

  sectionNodes: SectionNode[];

  private _sectionRefToNode: { [key: string]: SectionNode } = {};

  options = {};

  constructor() { }

  ngOnInit() { }

  ngOnChanges(changes: SimpleChanges): void {

    if (changes['sections']) {
      this.sectionNodes = this.buildSectionNodes(null, this.sections);
      this.updateSectionRefToNodeMap();
    }

    if (changes['selectedSectionRef']) {
      this.updateActiveNode();
    }

    this.updateOptions();
  }

  onTreeInitialized() {
    this.updateActiveNode();
  }

  private updateOptions() {
    this.options = {
      allowDrag: this.editable,
      allowDrop: this.editable,
      actionMapping: {
        mouse: {
          // Prevent copying of nodes using "ctrl"...
          drop: (tree: TreeModel, node: TreeNode, $event: any, {from , to}: {from: any, to: any}) => {
            if (from !== to) {
              tree.moveNode(from, to);
              this.sectionMoved.emit(node.id);
            }
          }},
        },
      };
  }

  private updateActiveNode() {

    if (!this.tree || !this.tree.treeModel || !this.tree.treeModel.nodes) return;

    if (!this.selectedSectionRef) {

      this.tree.treeModel.setActiveNode(this.tree.treeModel.getActiveNode(), false);
      this.tree.treeModel.setFocusedNode(null);

    } else {

      const node = this.tree.treeModel.getNodeById(this.selectedSectionRef);
      if (node && node !== this.tree.treeModel.getActiveNode()) {
        this.tree.treeModel.setActiveNode(node, true);
        this.tree.treeModel.setFocusedNode(node);
        const nodePath = this.findPathToNode(this._sectionRefToNode[this.selectedSectionRef], this.sectionNodes);
        if (nodePath) {
          nodePath.forEach(n => this.tree.treeModel.setExpandedNode(this.tree.treeModel.getNodeById(n.id), true));
        }
      }
    }
  }

  findPathToNode(nodeToFind: SectionNode, nodes: any[]): SectionNode[] {

    for (const node of nodes) {
      if (node === nodeToFind) {
        return [node];
      } else if (node.children) {
        const childPath = this.findPathToNode(nodeToFind, node.children);
        if (childPath && childPath.length > 0) {
          return [node, ...childPath];
        }
      }
    }
    return null;
  }

  private buildSectionNodes(parentRef: string, sections: Section[]): SectionNode[] {

    const res: SectionNode[] = [];
    for (const section of sections) {
      if (section.parentRef === parentRef) {
        res.push({
          id: section.ref,
          name: section.name,
          children: this.buildSectionNodes(section.ref, sections),
          isExpanded: false,
        });
      }
    }
    return res;
  }

  private updateSectionRefToNodeMap() {
    this._sectionRefToNode = {};
    this.addSectionRefToNodeMapRecursive(this.sectionNodes);
  }

  private addSectionRefToNodeMapRecursive(nodes: SectionNode[]) {

    if (!nodes || nodes.length <= 0) return;

    for (const node of nodes) {
      this._sectionRefToNode[node.id] = node;
      this.addSectionRefToNodeMapRecursive(node.children);
    }
  }

  getChangedSections(): SectionStructureChange[] {

    // We first build two maps, mapping each section id/ref to the corresponding order and parent ref
    // After that we compare it to the current state to determine the differences
    const newSectionParentRefs = {};
    this.sectionNodes.forEach(n => this.buildSectionParentRefs(n, null, newSectionParentRefs));
    const newSectionSortOrders = {};
    this.buildSectionSortOrders(this.sectionNodes, newSectionSortOrders);

    const changes: SectionStructureChange[] = [];
    for (const section of this.sections) {

      if (section.parentRef !== newSectionParentRefs[section.ref] || section.sortOrder !== newSectionSortOrders[section.ref]) {
        changes.push({
           section,
          newParentRef: newSectionParentRefs[section.ref],
          newSortOrder: newSectionSortOrders[section.ref],
        });
      }
    }

    return changes;
  }

  private buildSectionParentRefs(section: SectionNode, parentRef: string, newSectionParentRefs: {[key: string]: string}) {

    newSectionParentRefs[section.id] = parentRef;

    for (const childSection of section.children) {
      this.buildSectionParentRefs(childSection, section.id, newSectionParentRefs);
    }
  }

  private buildSectionSortOrders(orderedSections: SectionNode[], newSectionSortOrders: {[key: string]: number}) {

    let currentSortOrder = 0;
    for (const section of orderedSections) {

      newSectionSortOrders[section.id] = currentSortOrder;
      currentSortOrder += 10;
      if (section.children && section.children.length > 0) {
        this.buildSectionSortOrders(section.children, newSectionSortOrders);
      }
    }
  }

  onSectionClicked(event) {
    this.sectionClicked.emit(<string>event.node.id);
  }
}
