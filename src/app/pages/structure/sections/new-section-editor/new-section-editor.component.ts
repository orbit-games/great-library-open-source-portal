import {Component, HostListener, OnInit, ViewChild} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {Section} from '../../../../@core/api';
import {SectionEditorComponent} from '../section-editor/section-editor.component';
import {ComponentCanDeactivate} from '../../../../@theme/guards/pending-changes.guard';

@Component({
  selector: 'ngx-new-section-editor',
  template: `<ngx-section-editor [isNew]="true" (saved)="onSectionSaved($event)" #sectionEditor></ngx-section-editor>`,
  styles: [],
})
export class NewSectionEditorComponent implements OnInit, ComponentCanDeactivate {

  @ViewChild('sectionEditor', {static: false}) sectionEditor: SectionEditorComponent;

  constructor(
    private router: Router,
    private route: ActivatedRoute,

  ) { }

  ngOnInit() {

    this.route.queryParams.subscribe(q => {

    });

  }

  onSectionSaved(section: Section) {
    this.router.navigate(['..', section.ref], { relativeTo: this.route });
  }

  // @HostListener allows us to also guard against browser refresh, close, etc.
  @HostListener('window:beforeunload')
  canDeactivate = () => this.sectionEditor.canDeactivate()
}
