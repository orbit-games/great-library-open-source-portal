import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { Course, CourseResource, Section } from '../../../../@core/api';
import { SessionService } from '../../../../@core/session.service';
import { Subscription } from 'rxjs';

interface CourseResourceCollection {
  [resourceRef: string]: CourseResource;
}

@Component({
  selector: 'ngx-section-viewer',
  templateUrl: './section-viewer.component.html',
  styleUrls: ['./section-viewer.component.scss'],
})
export class SectionViewerComponent implements OnInit, OnDestroy {

  @Input() section: Section;
  resources: CourseResourceCollection;

  private courseChangedSubscription: Subscription;

  constructor(
    private sessionService: SessionService,
  ) {}


  ngOnInit() {
    this.sessionService.getSelectedCourse().subscribe(c => this.course = c);
    this.courseChangedSubscription = this.sessionService.selectedCourseChanged.subscribe(c => this.course = c);
  }

  ngOnDestroy(): void {
    this.courseChangedSubscription.unsubscribe();
  }

  set course(course: Course) {
    this.resources = {};
    for (const resource of course.resources) {
      this.resources[resource.ref] = resource;
    }
  }
}
