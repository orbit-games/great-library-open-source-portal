import { Component, OnInit } from '@angular/core';
import { NbMenuService } from '@nebular/theme';

@Component({
  selector: 'ngx-under-construction',
  templateUrl: './under-construction.component.html',
  styleUrls: ['./under-construction.component.scss']
})
export class UnderConstructionComponent implements OnInit {

  constructor(private menuService: NbMenuService) {
  }

  ngOnInit() {
    
  }

  goToHome() {
    this.menuService.navigateHome();
  }

}
