import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';

import {Role} from '../@core/api';
import {ConversationsComponent} from './conversations/conversations.component';
import {RoutedConversationComponent} from './conversations/routed-conversation/routed-conversation.component';
import {CoursesComponent} from './courses/courses.component';
import {DashboardComponent} from './dashboard/dashboard.component';
import {LandingPageComponent} from './landing-page/landing-page.component';
import {PagesComponent} from './pages.component';
import {ProgressTabComponent} from './progress/progress-tab/progress-tab.component';
import {ProgressComponent} from './progress/progress.component';
import {ReviewerRelationDetailComponent} from './progress/reviewer-relation-detail/reviewer-relation-detail.component';
import {GlSelectCourseGuard} from './select-course-guard.service';
import {ServerComponent} from './server/server.component';
import {UnderConstructionComponent} from './under-construction/under-construction.component';
import {UsersComponent} from './users/users.component';
import {ViewUserComponent} from './users/view-user/view-user.component';
import {ReviewStepsComponent} from './structure/review-steps/review-steps.component';
import {SectionsComponent} from './structure/sections/sections.component';
import {RoutedSectionEditorComponent} from './structure/sections/routed-section-editor/routed-section-editor.component';
import {NewSectionEditorComponent} from './structure/sections/new-section-editor/new-section-editor.component';
import {NewResourceEditorComponent} from './structure/resources/new-resource-editor/new-resource-editor.component';
import {RoutedResourceEditorComponent} from './structure/resources/routed-resource-editor/routed-resource-editor.component';
import {ResourcesComponent} from './structure/resources/resources.component';
import {PendingChangesGuard} from '../@theme/guards/pending-changes.guard';
import {DeadlinesComponent} from './structure/review-steps/deadlines/deadlines.component';
import {CourseEditorComponent} from './structure/course/course-editor/course-editor.component';
import {ChaptersComponent} from './structure/chapters/chapters.component';
import {NewChapterEditorComponent} from './structure/chapters/new-chapter-editor/new-chapter-editor.component';
import {RoutedChapterEditorComponent} from './structure/chapters/routed-chapter-editor/routed-chapter-editor.component';
import {RoutedReviewStepEditorComponent} from './structure/review-steps/routed-review-step-editor/routed-review-step-editor.component';
import {NewCourseComponent} from "./courses/new-course/new-course.component";
import {NewCourseFromTemplateComponent} from "./courses/new-course/new-course-from-template/new-course-from-template.component";

const routes: Routes = [{
  path: '',
  component: PagesComponent,
  children: [{
    path: 'courses',
    component: CoursesComponent,
  }, {
    path: 'courses/new',
    component: NewCourseComponent,
  }, {
    path: 'courses/new/:templateId',
    component: NewCourseFromTemplateComponent,
  }, {
    path: 'dashboard',
    component: DashboardComponent,
    canActivate: [GlSelectCourseGuard],
  }, {
    path: 'structure',
    canActivate: [GlSelectCourseGuard],
    children: [
      {
        path: 'course',
        component: CourseEditorComponent,
        canDeactivate: [PendingChangesGuard],
      },
      {
        path: 'chapters',
        component: ChaptersComponent,
        canDeactivate: [PendingChangesGuard],
        children: [
          {
            path: 'new',
            component: NewChapterEditorComponent,
            canDeactivate: [PendingChangesGuard],
          },
          {
            path: ':chapterRef',
            component: RoutedChapterEditorComponent,
            canDeactivate: [PendingChangesGuard],
          },
        ],
      },
      {
        path: 'review-steps',
        component: ReviewStepsComponent,
      }, {
        path: 'chapters/:chapterRef/review-step/:reviewStepRef',
        component: RoutedReviewStepEditorComponent,
        canDeactivate: [PendingChangesGuard],
      }, {
        path: 'deadlines',
        component: DeadlinesComponent,
        canDeactivate: [PendingChangesGuard],
      }, {
        path: 'sections',
        component: SectionsComponent,
        canDeactivate: [PendingChangesGuard],
        children: [
          {
            path: 'new',
            component: NewSectionEditorComponent,
            canDeactivate: [PendingChangesGuard],
          },
          {
            path: ':sectionRef',
            component: RoutedSectionEditorComponent,
            canDeactivate: [PendingChangesGuard],
          },
        ],
      }, {
        path: 'resources',
        component: ResourcesComponent,
        canDeactivate: [PendingChangesGuard],
        children: [
          {
            path: 'new',
            component: NewResourceEditorComponent,
            canDeactivate: [PendingChangesGuard],
          },
          {
            path: ':resourceRef',
            component: RoutedResourceEditorComponent,
            canDeactivate: [PendingChangesGuard],
          },
        ],
      },
    ],
  }, {
    path: 'progress',
    component: ProgressComponent,
    canActivate: [GlSelectCourseGuard],
    children: [{
      path: 'chapter/:chapterRef',
      component: ProgressTabComponent,
      canActivate: [GlSelectCourseGuard],
    }],
  }, {
    path: 'progress/chapter/:chapterRef/reviewer-relation/:reviewerRelationRef',
    component: ReviewerRelationDetailComponent,
    canActivate: [GlSelectCourseGuard],
  }, {
    path: 'users/administrators',
    component: UsersComponent,
    data: { roleFilter: [Role.ADMINISTRATOR] },
    canActivate: [GlSelectCourseGuard],
  }, {
    path: 'users/teachers',
    component: UsersComponent,
    data: { roleFilter: [Role.TEACHER] },
    canActivate: [GlSelectCourseGuard],
  }, {
    path: 'users/students',
    component: UsersComponent,
    data: { roleFilter: [Role.LEARNER] },
    canActivate: [GlSelectCourseGuard],
  }, {
    path: 'users/:userRef',
    component: ViewUserComponent,
    canActivate: [GlSelectCourseGuard],
  }, {
    path: 'conversations',
    component: ConversationsComponent,
    canActivate: [GlSelectCourseGuard],
    children: [{
      path: 'new',
      component: RoutedConversationComponent,
    },
    {
      path: ':conversationRef',
      component: RoutedConversationComponent,
    },
    ],
  }, {
    path: 'server',
    component: ServerComponent,
    data: { roleFilter: [Role.ADMINISTRATOR] },
  }, {
    path: 'course/:courseRef/conversations/:conversationRef',
    component: LandingPageComponent,
  }, {
    path: '',
    redirectTo: 'dashboard',
    pathMatch: 'full',
  }, {
    path: '**',
    component: UnderConstructionComponent,
  }],
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PagesRoutingModule {
}
