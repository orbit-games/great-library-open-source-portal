import { Injectable } from "@angular/core";
import { CanActivate, Router } from "@angular/router";
import { SessionService } from "../@core/session.service";
import { Observable } from "rxjs";
import { map } from "rxjs/operators";

/**
 * Guard that checks if a course is selected
 */
@Injectable()
export class GlSelectCourseGuard implements CanActivate {

    constructor(private sessionService: SessionService, private router: Router) {}

    canActivate(): Observable<boolean> {

        return this.sessionService.getSelectedCourse().pipe(
            map(c => {
                if (!c) {
                    this.router.navigate(['/pages/courses']);
                    return false;
                } else {
                    return true;
                }
            }));
    }
}