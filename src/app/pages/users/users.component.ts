import { Component, OnInit } from '@angular/core';
import { UserService } from '../../@core/data/user.service';
import { SessionService } from '../../@core/session.service';
import { GlErrorHandlerService } from '../../@theme/error-handler.service';
import { Router, ActivatedRoute } from '@angular/router';
import { CourseProgressSummary, User, Course, Role } from '../../@core/api';
import { LocalDataSource } from 'ng2-smart-table';
import { FormatHelper } from '../../@theme/pipes/format-helper';

@Component({
  selector: 'ngx-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss'],
})
export class UsersComponent implements OnInit {

  settings = {
    actions: {
      add: false,
      edit: true,
      delete: false,
    },
    edit: {
      editButtonContent: 'View',
    },
    filter: {
      inputClass: 'gl-filter',
    },
    mode: 'external',
    columns: {
      email: {
        title: 'Email',
      },
      fullName: {
        title: 'Full name',
      },
      displayName: {
        title: 'Avatar name',
      },
      lastActive: {
        title: 'Last active',
        valuePrepareFunction: v => FormatHelper.toHumanReadableDateTime(v),
      },
      created: {
        title: 'Registered',
        valuePrepareFunction: v => FormatHelper.toHumanReadableDateTime(v),
      },
      active: {
        title: 'Active',
        valuePrepareFunction: v => v ? 'Yes' : 'No',
      },
    },
    pager: {
      perPage: 50,
    },
  };

  loading: boolean = true;

  source: LocalDataSource = new LocalDataSource();

  course: Course;
  private roleFilter: Role[];
  mainRole: Role;

  users: User[];

  constructor(private sessionService: SessionService, private userService: UserService, private errorHandler: GlErrorHandlerService,
    private router: Router, private route: ActivatedRoute) { }

  ngOnInit() {

    this.route.data.subscribe(d => this.setRoleFilter(d.roleFilter));
    this.sessionService.getSelectedCourse().subscribe(c => {
      this.course = c;
      this.fetchUsers();
    });

    this.sessionService.setCurrentPage({
      icon: 'fa fa-users',
      title: 'Users',
    });
  }

  setRoleFilter(roles: Role[]) {
    this.roleFilter = roles;
    this.mainRole = roles[0];
    this.fetchUsers();
  }

  fetchUsers() {

    if (!this.course) {
      // We have to wait for the course to load...
      return;
    }

    this.loading = true;
    this.users = [];
    this.userService.getUsers(this.course.ref, this.roleFilter)
      .then(userCollection => {
        this.users = userCollection.users;
        const userData = this.users.map(u => {
          const courseUser = this.course.users.find(cu => cu.userRef === u.ref);
          return {
            ref: u.ref,
            email: u.email,
            fullName: u.fullName,
            displayName: courseUser.displayName,
            lastActive: u.lastActive,
            created: u.created,
            active: courseUser.active,
          };
        });
        this.source.load(userData);
        this.loading = false;
      })
      .catch(e => { this.errorHandler.handle(e); });
  }

  onSelectUser(event) {
    this.router.navigate(['/pages', 'users', event.data.ref]);
  }
}
