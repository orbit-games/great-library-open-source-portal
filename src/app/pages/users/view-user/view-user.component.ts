import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { SessionService } from '../../../@core/session.service';
import { UserService } from '../../../@core/data/user.service';
import { GlErrorHandlerService } from '../../../@theme/error-handler.service';
import { Router, ActivatedRoute } from '@angular/router';
import { User, Role, UserAgreement, CourseSummary, CourseProgressSummary, Course } from '../../../@core/api';
import { UserAgreementService } from '../../../@core/data/user-agreement.service';
import { ConversationService } from '../../../@core/data/conversation.service';
import { CourseService } from '../../../@core/data/course.service';
import { NbDialogService, NbDialogRef } from '@nebular/theme';
import { SwitcherComponent } from '../../../@theme/components/switcher/switcher.component';

@Component({
  selector: 'ngx-view-user',
  templateUrl: './view-user.component.html',
  styleUrls: ['./view-user.component.scss'],
})
export class ViewUserComponent implements OnInit {

  loggedInUser: User;
  userRef: string;
  user: User;
  signedAgreement: UserAgreement;
  selectedCourse: Course;

  startingConversation = false;

  @ViewChild('userActiveSwitch', {static: false}) userActiveSwitch: SwitcherComponent;
  @ViewChild('confirmSetInactiveDialog', {static: false}) confirmSetInactiveDialogTemplateRef: TemplateRef<any>;

  constructor(private sessionService: SessionService,
              private userService: UserService,
              private userAgreementService: UserAgreementService,
              private conversationService: ConversationService,
              private courseService: CourseService,
              private errorHandler: GlErrorHandlerService,
              private router: Router,
              private route: ActivatedRoute,
              private dialogService: NbDialogService)
  { }

  ngOnInit() {

    this.route.params.subscribe(p => {
      this.userRef = p['userRef'];
      this.updateUser();
    });
    this.sessionService.getLoggedInUser().subscribe(l => this.loggedInUser = l);
    this.sessionService.getSelectedCourse().subscribe(c => this.selectedCourse = c);

    this.sessionService.setCurrentPage({
      icon: 'fa fa-users',
      title: 'Users',
      list: {
        visible: true,
        tooltip: 'Back to user overview',
        clickHandler: () => {
          if (!this.user) {
            // Don't know if we should go back to the teachers or users page
            return;
          } else {
            if (this.user.role === Role.LEARNER) {
              this.router.navigate(['/pages', 'users', 'players']);
            } else {
              this.router.navigate(['/pages', 'users', 'teachers']);
            }
          }
        },
      },
    });
  }

  updateUser(forceRefresh = false) {
    this.userService.getUser(this.userRef, forceRefresh)
        .then(u => this.setUser(u))
        .catch(e => this.errorHandler.handle(e));
  }

  setUser(user: User) {
    this.user = user;
    if (user.signedUserAgreementRef) {
      this.userAgreementService.getUserAgreement(this.user.signedUserAgreementRef)
        .then(a => this.signedAgreement = a)
        .catch(e => this.errorHandler.handle(e));
    }
  }

  progress(course: CourseSummary): CourseProgressSummary {
    if (this.user == null || this.user.courseProgressSummary == null) {
      return null;
    }
    return this.user.courseProgressSummary.find(p => p.courseRef === course.ref);
  }

  confirmUserActiveChange(active: boolean) {
    this.dialogService.open(this.confirmSetInactiveDialogTemplateRef, {
      context: {
        userActive: active,
      },
    });
    // Hack: Prevent actually flipping the switch until after it is confirmed
    setTimeout(() => this.userActiveSwitch.value = !active, 0);

  }

  setUserActive(active: boolean, selectedCourse: Course, dialogRef: NbDialogRef<any>) {
    const progress = this.progress(selectedCourse);
    progress.active = active;
    this.courseService.updateCourseUser(selectedCourse.ref, this.userRef, progress)
        .then(p => {
          dialogRef.close();
          this.updateUser(true);
        })
        .catch(e => this.errorHandler.handle(e));
  }

  onStartConversation() {

    if (this.loggedInUser.ref === this.userRef) {
      // You can't start a conversation with yourself
      return;
    }
    // Check if there already is a conversation, and navigate to it if it exists
    this.startingConversation = true;
    this.conversationService.getConversations(this.selectedCourse.ref)
        .then(conversations => {
          const existingConversation = conversations.conversations.find(c =>
            c.members.length === 2 &&
            c.members.includes(this.userRef) &&
            c.members.includes(this.loggedInUser.ref));
          if (existingConversation) {
            this.router.navigate(['pages', 'conversations', existingConversation.ref]);
          } else {
            this.router.navigate(['pages', 'conversations', 'new'], {
              queryParams: {
                'members': this.loggedInUser.ref + ',' + this.user.ref,
              },
            });
          }
        })
        .catch(e => {
          this.errorHandler.handle(e);
          this.startingConversation = false;
        });
  }
}
