import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UsersComponent } from './users.component';
import { NbSpinnerModule, NbDialogModule } from '@nebular/theme';
import { Ng2SmartTableModule } from 'ng2-smart-table';
import { ThemeModule } from '../../@theme/theme.module';
import { ViewUserComponent } from './view-user/view-user.component';

@NgModule({
  imports: [
    CommonModule,
    ThemeModule,
    NbSpinnerModule,
    Ng2SmartTableModule,
    NbDialogModule.forRoot(),
  ],
  declarations: [UsersComponent, ViewUserComponent],
})
export class UsersModule { }
