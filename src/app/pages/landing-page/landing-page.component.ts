import { Component, OnInit } from '@angular/core';
import { SessionService } from '../../@core/session.service';
import { Router, ActivatedRoute } from '@angular/router';
import { CourseService } from '../../@core/data/course.service';

@Component({
  selector: 'ngx-landing-page',
  template: 'Redirecting...',
})
export class LandingPageComponent implements OnInit {

  constructor(
    private sessionService: SessionService,
    private courseService: CourseService,
    private router: Router,
    private activatedRoute: ActivatedRoute) {

  }

  ngOnInit() {

  // We immediately redirect the user to the correct page:
  this.activatedRoute.params.subscribe(p => {
    const courseRef = p['courseRef'];
    this.sessionService.getLoggedInUser().subscribe(u => {
      this.courseService.getCourse(courseRef)
        .then(c => {
          this.sessionService.setSelectedCourse(c);
          // TODO Handle non-conversation URLs
          this.router.navigate(['/pages/conversations', p['conversationRef']]);
        });
      });
    });
  }

}
