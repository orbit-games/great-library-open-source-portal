import {
  Component,
  EventEmitter,
  Input,
  OnChanges,
  OnDestroy,
  OnInit,
  Output,
  SimpleChanges,
  ViewChild,
} from '@angular/core';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';

import {
  Conversation,
  ConversationMessage,
  ConversationPermissionType,
  ConversationRolePermission,
  Course,
  User,
} from '../../../@core/api';
import { ConversationService } from '../../../@core/data/conversation.service';
import { NameType, SessionService } from '../../../@core/session.service';
import { ChatComponent, DisplayMessage } from '../../../@theme/components/chat/chat.component';
import { GlErrorHandlerService } from '../../../@theme/error-handler.service';
import { ConversationHelperService } from '../conversation-helper.service';

declare var Pace;

const MESSAGES_PER_PAGE = 50;

@Component({
  selector: 'gl-conversation',
  templateUrl: './conversation.component.html',
  styleUrls: ['./conversation.component.scss'],
})
export class ConversationComponent implements OnInit, OnDestroy, OnChanges {

  course: Course;
  loggedInUser: User;
  nameType: NameType;

  conversation: Conversation;
  invertedMessages: DisplayMessage[];
  isPostAllowed: boolean = false;
  hasMoreMessages = false;

  displayTitle: string;
  @Input() conversationRef: string;
  @Input() title: string;
  @Input() members: string[];
  @Input() roles: ConversationRolePermission[];

  /**
   * The size of the chat component (xxsmall, xsmall, small, medium, large, xlarge, xxlarge), default = xlarge
   */
  @Input() size: string = 'xlarge';
  @Input() showCloseButton: boolean = false;

  isNew: boolean;

  creatingConversation = false;
  refreshTimer: any;
  markAsReadTimeout: any;

  @Output() conversationCreated = new EventEmitter<Conversation>();
  @Output() closeButtonClicked = new EventEmitter<any>();

  private selectedCourseChangedSubscription: Subscription;

  @ViewChild(ChatComponent, {static: false}) chatWindow: ChatComponent;

  constructor(private sessionService: SessionService, private conversationService: ConversationService,
              private conversationHelper: ConversationHelperService,
              private errorHandler: GlErrorHandlerService,
              private router: Router) { }

  ngOnInit() {

    this.sessionService.getSelectedCourse().subscribe(c => {
      this.course = c;
      this.refreshConversation();
    });
    this.selectedCourseChangedSubscription = this.sessionService.selectedCourseChanged.subscribe(c => {
      this.course = c;
    });

    this.sessionService.getLoggedInUser().subscribe(u => {
      this.loggedInUser = u;
      this.updateIsPostAllowed();
    });
    this.nameType = this.sessionService.nameType;
    this.sessionService.nameTypeChanged.subscribe(n => {
      this.nameType = n;
      this.refreshConversation(true);
    });

    this.refreshTimer = setInterval(() => this.refreshConversation(), 2000);
  }

  ngOnDestroy() {

    clearInterval(this.refreshTimer);
    this.selectedCourseChangedSubscription.unsubscribe();
  }

  ngOnChanges(changes: SimpleChanges) {

    if (changes.conversationRef.previousValue !== changes.conversationRef.currentValue && changes.conversationRef.currentValue) {

      this.isNew = false;
      this.members = [];
      this.roles = [];
      this.invertedMessages = [];
      this.conversation = null;
      this.refreshConversation();
    } else {

      this.isNew = true;
      const newConversation: Conversation = {
        title: this.title,
        hidden: false,
        anonymousMembers: false,
        members: this.members,
        roles: this.roles,
        readUntil: -1,
        messageCount: 0,
        messages: [],
        type: 'TOPIC',
      };
      this.displayTitle = this.conversationHelper.getConversationTitle(newConversation);
      this.conversation = newConversation;
    }
  }


  loadMore() {

    if (this.isNew || !this.conversationRef || !this.course) {
      return;
    }

    Pace.ignore(() => {
      this.conversationService.getConversation(this.course.ref, this.conversationRef, null, MESSAGES_PER_PAGE, this.invertedMessages.length, true)
        .then(c => {
          const newMessages = c.messages
            // Don't add duplicate messages
            .filter(m => this.invertedMessages.findIndex(i => m.ref === i.ref))
            // Reverse and add as display messages
            .slice(0).reverse().map(m => this.toDisplayMessage(m));
          this.hasMoreMessages = this.invertedMessages.length < c.messageCount;
          this.invertedMessages = newMessages.concat(this.invertedMessages);
        });
    });
  }

  refreshConversation(force = false) {
    if (this.isNew || !this.conversationRef || !this.course) {
      return;
    }

    Pace.ignore(() => {
      this.conversationService.getConversation(this.course.ref, this.conversationRef, null, MESSAGES_PER_PAGE, null, true)
        .then(c => {
          // Sometimes the user already switched to another conversation before we get the response, in which case we can discard the response:
          if (c.ref !== this.conversationRef) {
            return;
          }
          const oldConversation = this.conversation;

          const lastKnownMessage = oldConversation != null && oldConversation.messages.length > 0 ? oldConversation.messages[0] : null;
          const newestMessage = c.messages[0];
          const hasNewMessages = c.messages && c.messages.length > 0 && (lastKnownMessage == null || (lastKnownMessage.ref !== newestMessage.ref));

          // console.log("hasNewMessages: " + hasNewMessages);

          if (!this.invertedMessages) {

            // console.log("Reloading inverted messages");
            this.invertedMessages = c.messages.slice(0).reverse().map(m => this.toDisplayMessage(m));

          } else if (hasNewMessages || force) {

            // console.log("Loading messages");

            this.invertedMessages = c.messages.slice(0).reverse().map(m => this.toDisplayMessage(m));
            this.markAsReadTimeout = setTimeout(() => this.markConversationRead(c.ref, c.messageCount - 1), 500);

            this.chatWindow.scrollToBottom();
          }

          this.conversation = c;
          this.hasMoreMessages = this.invertedMessages.length < c.messageCount;
          this.displayTitle = this.conversationHelper.getConversationTitle(c);
          this.members = c.members;
          this.roles = c.roles;
          this.updateIsPostAllowed();

        })
        .catch(e => this.errorHandler.handle(e));
    });
  }

  toDisplayMessage(message: ConversationMessage): DisplayMessage {
    return {
      ref: message.ref,
      message: message.message,
      isReply: message.sender !== this.loggedInUser.ref,
      senderId: message.sender,
      senderName: this.senderName(message),
      date: message.sent,
    };
  }

  markConversationRead(conversationRef, readUntil) {
    if (this.conversationRef !== conversationRef) {
      // Apparently the conversation changed already, so we can't be sure it has been read yet
      return;
    }

    this.conversationService.postConversationRead(this.course.ref, conversationRef, readUntil)
      .then(() => {
        // Conversation successfully marked as read. Nothing to do here
      })
      .catch(e => this.errorHandler.handle(e));
  }

  senderName(message: ConversationMessage) {

    const senderUser = this.course.users.find(u => u.userRef === message.sender);
    return this.nameType === NameType.DISPLAY_NAME ? senderUser.displayName : senderUser.fullName;
  }

  sendMessage(event) {
    // console.log(event);
    if (this.isNew) {
      this.creatingConversation = true;
      this.conversationService.createConversation(this.course.ref, this.conversation).then(newConversation => {
        this.conversationHelper.conversationCreated.next(newConversation);
        this.conversationService.postConversationMessage(this.course.ref, newConversation.ref, event.message)
          .then(m => {
            this.conversationCreated.emit(newConversation);
            this.router.navigate(['pages', 'conversations', newConversation.ref]);
            this.creatingConversation = false;
          })
          .catch(e => {
            this.errorHandler.handle(e);
            this.creatingConversation = false;
          });
      })
        .catch(e => {
          this.errorHandler.handle(e);
          this.creatingConversation = false;
        });
    } else {
      this.conversationService.postConversationMessage(this.course.ref, this.conversationRef, event.message)
        .then(m => {
          this.conversation.messageCount += 1;
          this.conversation.messages.unshift(m);
          this.invertedMessages.push(this.toDisplayMessage(m));
          this.chatWindow.scrollToBottom();
        });
    }
  }

  getLastUpdated() {
    if (!this.invertedMessages || this.invertedMessages.length === 0) {
      return null;
    }

    return this.invertedMessages[this.invertedMessages.length - 1].date;
  }


  updateIsPostAllowed(): boolean {

    if (this.conversation == null || this.loggedInUser == null) {
      this.isPostAllowed = false;
      return;
    }

    this.isPostAllowed =
      (this.conversation.members && this.conversation.members.includes(this.loggedInUser.ref)) ||
      (this.conversation.roles && this.conversation.roles.find(r =>
        r.role === this.loggedInUser.role
        && r.permission === ConversationPermissionType.POST) != null);
  }

  onCloseButtonClicked() {
    this.closeButtonClicked.emit();
  }
}
