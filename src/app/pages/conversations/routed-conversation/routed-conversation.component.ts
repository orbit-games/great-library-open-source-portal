import { Component, OnInit } from '@angular/core';
import { ConversationRolePermission, Role, ConversationPermissionType } from '../../../@core/api';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'ngx-routed-conversation',
  templateUrl: './routed-conversation.component.html',
  styleUrls: ['./routed-conversation.component.scss']
})
export class RoutedConversationComponent implements OnInit {

  conversationRef: string;
  title: string;
  members: string[];
  roles: ConversationRolePermission[];

  constructor(
    private route: ActivatedRoute
  ) { }

  ngOnInit() {

    this.route.params.subscribe(params => {
      if (params["conversationRef"]) {
        this.conversationRef = params["conversationRef"];
        this.members = [];
        this.roles = [];
      }
    })
    this.route.queryParams.subscribe(q => {
      let membersParam: string = q["members"];
      if (!membersParam) {
        this.members = [];
      } else {
        this.members = membersParam.split(',');
      }

      let rolesParam: string = q["roles"];
      if (!rolesParam) {
        this.roles = [];
      } else {
        this.roles = rolesParam.split(',').map(r => {
          let splitRole = r.split(':');
          return {
            role: splitRole[0] as Role,
            permission: splitRole[1] as ConversationPermissionType,
          }
        });
      }
      this.title = q["title"];
    })

  }

}
