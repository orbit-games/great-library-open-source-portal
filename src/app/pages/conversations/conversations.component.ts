import { Component, OnInit, OnDestroy } from '@angular/core';
import { SessionService } from '../../@core/session.service';
import { ConversationService } from '../../@core/data/conversation.service';
import { GlErrorHandlerService } from '../../@theme/error-handler.service';
import { Course, Conversation, User, ConversationRolePermission, Role, ConversationPermissionType } from '../../@core/api';
import { compare, reverse } from '../../@core/data/property-compare';
import { Router, ActivatedRoute, NavigationEnd } from '@angular/router';
import { ConversationHelperService } from './conversation-helper.service';
import { Subscription } from 'rxjs';

declare var Pace;

class ConversationViewItem {
  ref: string;
  title: string;
  subtitle: string;
  members: string[];
  roles: ConversationRolePermission[];
  unreadMessageCount?: number = 0;
  lastMessageSent: Date;
}

@Component({
  selector: 'ngx-conversations',
  templateUrl: './conversations.component.html',
  styleUrls: ['./conversations.component.scss']
})
export class ConversationsComponent implements OnInit, OnDestroy {

  loggedInUser: User;
  course: Course;
  private lastUpdated?: Date = null;
  /**
   * The actual conversations with content
   */
  private conversations: Conversation[] = [];

  /**
   * A virtual list of conversations, that also contains course default conversations (such as announcements and conversations with all other users)
   */
  conversationList: ConversationViewItem[];

  selectedConversationRef: string = null;

  conversationRefreshInterval: any;

  private conversationsUpdatedSubscription: Subscription;
  private selectedCourseChangedSubscription: Subscription;

  constructor(private sessionService: SessionService, private conversationService: ConversationService, 
    private conversationHelper: ConversationHelperService,
    private errorHandler: GlErrorHandlerService,
    private route: ActivatedRoute,
    private router: Router) { }

  ngOnInit() {

    this.sessionService.getSelectedCourse().subscribe(c => {
      this.course = c;
      this.refreshConversations();
    })
    this.selectedCourseChangedSubscription = this.sessionService.selectedCourseChanged.subscribe(c => {
      this.course = c;
      this.refreshConversations();
    })

    this.sessionService.getLoggedInUser().subscribe(l => {
      this.loggedInUser = l;
      this.refreshConversations();
    });

    this.conversationRefreshInterval = setInterval(() => this.refreshConversations(), 10000); 

    this.sessionService.setCurrentPage({
      icon: "fa fa-comments",
      title: "Conversations",
    });

    this.conversationHelper.conversationCreated.subscribe(c => {
      this.refreshConversations();
    })

    this.router.events.forEach(e => {
      if (e instanceof NavigationEnd) {
        this.updateSelectedConversationRef();
      }
    })
    
    this.updateSelectedConversationRef();

    // this.routeParamsSubscription = this.route.firstChild.params.subscribe(params => {
    //   console.log(this.route.snapshot)
    //   console.log("ConversationRef: " + params["conversationRef"])
    //   if (params["conversationRef"]) {
    //     console.log(params["conversationRef"])
    //     this.selectedConversationRef = params["conversationRef"];
    //   }
    // })

    this.conversationsUpdatedSubscription = this.conversationService.conversationsUpdated.subscribe(c => {
      this.conversations = c;
      this.rebuildConversationList();
    });
  }

  ngOnDestroy() {
    clearInterval(this.conversationRefreshInterval);
    this.conversationsUpdatedSubscription.unsubscribe();
    this.selectedCourseChangedSubscription.unsubscribe();
  }

  updateSelectedConversationRef() {
    if (this.route.firstChild) {
      // console.log(this.route.firstChild.snapshot.params["conversationRef"])
      this.selectedConversationRef = this.route.firstChild.snapshot.params["conversationRef"]
    } else {
      this.selectedConversationRef = null;
    }
  }

  refreshConversations() {

    if (!this.course) {
      return;
    }


    Pace.ignore(() => {
      this.conversationService.getConversations(this.course.ref, this.lastUpdated, null, [], true)
          .then(c => {
            this.conversations = c.conversations;
            this.lastUpdated = this.lastMessageDate();
          })
          .catch(e => this.errorHandler.handle(e));
        });
  }

  lastMessageDate(): Date {
    let lastMessagesSent = this.conversations.filter(c => c.messages && c.messages.length > 0).map(c => c.messages[0].sent);
    if (!lastMessagesSent) {
      return null;
    }
    var lastDate = new Date(0);
    for (let messageSent of lastMessagesSent) {
      if (messageSent > lastDate) {
        lastDate = messageSent;
      }
    }
    return lastDate;
  }

  rebuildConversationList() {
    // TODO Some way to detect what messages were seen

    let conversationList:ConversationViewItem[] = [];
    // First, the announcement conversation
    let announcementConversation = this.conversations.find(c => c.title === 'Announcements');
    // console.log(announcementConversation)
    if (announcementConversation) {
      conversationList.push(this.createConversationViewItem(announcementConversation));
    }

    // Now all "other conversations"
    this.conversations
      .filter(c => conversationList.find(l => l.ref === c.ref) == null)
      .sort(reverse(compare(c => this.getLastConversationMessageSent(c), compare(c => c.title, compare(c => c.created, compare(c => c.ref))))))
      .forEach(c => conversationList.push(this.createConversationViewItem(c)));
    
    this.conversationList = conversationList;
  }

  createConversationViewItem(conversation: Conversation): ConversationViewItem {
    let memberSummary = this.conversationHelper.getConversationMembersSummary(conversation);
    return {
      ref: conversation.ref,
      title: conversation.title || memberSummary,
      subtitle: conversation.title ? memberSummary : this.getLastConversationMessage(conversation),
      members: conversation.members,
      roles: conversation.roles,
      unreadMessageCount: this.unreadMessageCount(conversation),
      lastMessageSent: this.getLastConversationMessageSent(conversation),
    };
  }

  getLastConversationMessageSent(conversation: Conversation): Date {
    if (conversation == null || conversation.messages == null || conversation.messages.length == 0) {
      return null;
    }
    return conversation.messages[0].sent;
  }

  getLastConversationMessage(conversation: Conversation): string {
    if (conversation == null || conversation.messages == null || conversation.messages.length == 0) {
      return null;
    }
    let message = conversation.messages[0];
    if (message.sender === this.loggedInUser.ref) {
      return 'You: ' + message.message;
    } else {
      let sender = this.course.users.find(u => u.userRef === message.sender);
      if (sender) {
        return sender.displayName + ": " + message.message;
      } else {
        return message.message;
      }
    }
  }

  unreadMessageCount(conversation: Conversation): number {
    return conversation.messageCount - conversation.readUntil - 1;
  }

  onClickConversation(conv: ConversationViewItem) {
    if (conv.ref) {
      this.router.navigate(['pages', 'conversations', conv.ref]);
    }
  }
}
