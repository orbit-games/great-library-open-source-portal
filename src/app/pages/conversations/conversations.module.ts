import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ConversationsComponent } from './conversations.component';
import { ThemeModule } from '../../@theme/theme.module';
import { NbSpinnerModule, NbChatModule, NbCardModule, NbListModule } from '@nebular/theme';
import { ConversationComponent } from './conversation/conversation.component';
import { RouterModule } from '@angular/router';
import { ConversationHelperService } from './conversation-helper.service';
import { AdminChatComponent } from './admin-chat/admin-chat.component';
import { RoutedConversationComponent } from './routed-conversation/routed-conversation.component';
import { ShortDisplayTimestampPipe } from './short-display-timestamp.pipe';

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    ThemeModule,
    NbCardModule,
    NbSpinnerModule,
    NbChatModule,
    NbListModule,
  ],
  declarations: [ConversationsComponent, ConversationComponent, AdminChatComponent, RoutedConversationComponent, ShortDisplayTimestampPipe],
  providers: [ConversationHelperService],
  exports: [ AdminChatComponent, ConversationsComponent ]
})
export class ConversationsModule { }
