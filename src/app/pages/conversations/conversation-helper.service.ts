import { Injectable, OnDestroy } from "@angular/core";
import { SessionService, NameType } from "../../@core/session.service";
import { Course, Conversation, User, ConversationPermissionType, Role } from "../../@core/api";
import { Subscription, Subject } from "rxjs";


@Injectable()
export class ConversationHelperService implements OnDestroy {

    private loggedInUser: User;
    private loggedInUserChangedSubscription: Subscription;
    private course: Course;
    private courseChangedSubscription: Subscription;

    public conversationCreated: Subject<Conversation> = new Subject<Conversation>();

    constructor(private sessionService: SessionService) {
        this.sessionService.getSelectedCourse().subscribe(c => this.course = c);
        this.courseChangedSubscription = this.sessionService.selectedCourseChanged.subscribe(c => this.course = c);

        this.sessionService.getLoggedInUser().subscribe(u => this.loggedInUser = u);
        this.sessionService.loggedInUserChanged.subscribe(u => this.loggedInUser = u);
    }

    ngOnDestroy() {
        this.courseChangedSubscription.unsubscribe();
    }

    getConversationTitle(conversation: Conversation): string {

        if (conversation.title) {
            return conversation.title;
        }

        return this.getConversationMembersSummary(conversation);
    }

    /**
     * Generates a human-readable summary of the members and roles that are allowed to post and view this conversation
     * @param conversation The conversation
     * @returns A summary of members and roles in this conversation
     */
    public getConversationMembersSummary(conversation: Conversation): string {

        if (!conversation) {
            return null;
        }

      let res = '';
        if (conversation.members) {

            if (conversation.members.indexOf(this.loggedInUser.ref) > -1) {
                res += "you";
            }
            let otherConversationMembers = conversation.members
                .filter(m => m !== this.loggedInUser.ref)
                .map(m => this.course.users.find(u => u.userRef === m))
                .filter(u => u != null);
            if (otherConversationMembers.length > 0) {
                if (res) {
                    res += ", ";
                }
                res += otherConversationMembers
                    .map(u => this.sessionService.nameType === NameType.DISPLAY_NAME ? u.displayName : u.fullName)
                    .sort()
                    .join(", ");
            }
        }

        if (conversation.roles) {
            let postRoles = conversation.roles.filter(r => r.permission === ConversationPermissionType.POST).map(r => r.role);
            if (postRoles.length > 0) {
                if (res) {
                    res += ', ';
                }
                res += postRoles.sort().map(r => this.humanReadableRoleName(r)).join(", ");
            }

            let viewRoles = conversation.roles.filter(r => r.permission === ConversationPermissionType.VIEW).map(r => r.role);
            if (viewRoles.length > 0) {
                if (res) {
                    res += ', ';
                }
                res += 'visible to ';
                res += viewRoles.map(r => this.humanReadableRoleName(r)).join(", ");
            }
        }

        // Capitalize first letter
        res = res.charAt(0).toUpperCase() + res.slice(1);

        return res;
    }

    humanReadableRoleName(role: Role): string {
        switch(role) {
            case "ADMINISTRATOR": return 'developers';
            case "LEARNER": return 'students';
            case "TEACHER": return 'teachers';
        }
    }
}
