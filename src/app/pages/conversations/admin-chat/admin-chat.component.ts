import { Component, OnInit, OnDestroy } from '@angular/core';
import { ConversationService } from '../../../@core/data/conversation.service';
import { SessionService } from '../../../@core/session.service';
import { Course, User, Role, ConversationPermissionType } from '../../../@core/api';
import { Subscription } from 'rxjs';

@Component({
  selector: 'gl-admin-chat',
  templateUrl: './admin-chat.component.html',
  styleUrls: ['./admin-chat.component.scss'],
})
export class AdminChatComponent implements OnInit, OnDestroy {

  loggedInUser: User;
  selectedCourse: Course;
  adminConversationRef: string;

  private loggedInUserChangedSubscription: Subscription;
  private selectedCourseChangedSubscription: Subscription;

  chatOpened = false;

  constructor(private sessionService: SessionService, private conversationService: ConversationService) { }

  ngOnInit() {

    this.sessionService.getLoggedInUser().subscribe(u => {
      this.loggedInUser = u;
      this.updateAdminConversationRef();
    });
    this.loggedInUserChangedSubscription = this.sessionService.loggedInUserChanged.subscribe(u => {
      this.loggedInUser = u;
      this.updateAdminConversationRef();
    });

    this.sessionService.getSelectedCourse().subscribe(c => this.setSelectedCourse(c));
    this.selectedCourseChangedSubscription = this.sessionService.selectedCourseChanged.subscribe(c => this.setSelectedCourse(c));
  }

  ngOnDestroy() {
    this.selectedCourseChangedSubscription.unsubscribe();
    this.loggedInUserChangedSubscription.unsubscribe();
  }

  setSelectedCourse(course: Course) {
    this.selectedCourse = course;
    this.updateAdminConversationRef();
  }

  updateAdminConversationRef() {

    if ((this.loggedInUser == null) || (this.selectedCourse == null)) {
      return;
    }

    this.conversationService.getConversations(this.selectedCourse.ref)
      .then(cs => {
        let adminConversation = cs.conversations.find(c =>
          c.members != null && c.members.length === 1 && c.members[0] === this.loggedInUser.ref
          && c.roles != null && c.roles.length === 1
          && c.roles[0].role === Role.ADMINISTRATOR && c.roles[0].permission === ConversationPermissionType.POST);
        this.adminConversationRef = adminConversation.ref;
      });
  }

  toggleChat() {
    this.chatOpened = !this.chatOpened;
  }

}
