import { Pipe, PipeTransform } from '@angular/core';
import { FormatHelper } from '../../@theme/pipes/format-helper';
import * as moment from 'moment';

@Pipe({
  name: 'shortDisplayTimestamp'
})
export class ShortDisplayTimestampPipe implements PipeTransform {

  transform(value: any, args?: any): any {
    if (!value) {
      return null;
    }

    if (moment().isSame(moment(value), 'd')) {
      return FormatHelper.toHumanReadableTime(value);
    } else {
      return FormatHelper.toHumanReadableDate(value);
    }
  }

}
