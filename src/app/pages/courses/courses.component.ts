import { Component, OnInit, ViewChild, ElementRef, OnDestroy } from '@angular/core';
import { SessionService } from '../../@core/session.service';
import { LocalDataSource } from 'ng2-smart-table';
import { Subscription } from 'rxjs';
import { CourseService } from '../../@core/data/course.service';
import { Router } from '@angular/router';
import { GlErrorHandlerService } from '../../@theme/error-handler.service';
import { User } from '../../@core/api';

@Component({
  selector: 'ngx-courses',
  templateUrl: './courses.component.html',
  styleUrls: ['./courses.component.scss'],
})
export class CoursesComponent implements OnInit, OnDestroy {

  settings = {
    actions: {
      add: false,
      edit: true,
      delete: false,
    },
    edit: {
      editButtonContent: 'Select',
    },
    filter: {
      inputClass: 'gl-filter',
    },
    mode: 'external',
    columns: {
      name: {
        title: 'Name',
      },
      year: {
        title: 'Year',
      },
      quarter: {
        title: 'Quarter',
      },
    },
  };

  source: LocalDataSource = new LocalDataSource();
  loadingCourse = false;
  joiningCourse = false;
  private loggedInUserChangedSubscription: Subscription;
  courseCode: string = '';

  constructor(
    private sessionService: SessionService,
    private courseService: CourseService,
    private errorHandler: GlErrorHandlerService,
    private router: Router) { }

  ngOnInit() {

    const loggedInUser = this.sessionService.getLoggedInUser().subscribe(u => this.setUser(u));
    this.loggedInUserChangedSubscription = this.sessionService.loggedInUserChanged.subscribe(u => this.setUser(u));

    this.sessionService.setCurrentPage({
      icon: 'fa fa-graduation-cap',
      title: 'Courses',
      new: {
        visible: true,
        clickHandler: () => this.router.navigate(['/pages/courses/new']),
        tooltip: 'Add course',
      },
    });
  }

  ngOnDestroy() {
    this.loggedInUserChangedSubscription.unsubscribe();
  }

  private setUser(user: User) {
    if (user) {
      this.source.load(user.courses);
    }
  }

  onSelectCourse(event) {
    this.selectCourse(event.data.ref);
  }

  selectCourse(courseRef: string) {
    this.loadingCourse = true;
    this.courseService.getCourse(courseRef)
      .then(c => {
        this.sessionService.setSelectedCourse(c);
        this.router.navigate(['/pages/dashboard']);
        this.loadingCourse = false;
      }).catch(e => {
        this.errorHandler.handle(e);
      });
  }

  onJoinCourse() {

    const courseCode = this.courseCode;
    if (!courseCode) {
      return;
    }
    this.joiningCourse = true;
    this.courseService.joinCourse(courseCode)
      .then(c => {
        this.sessionService.refreshLoggedInUser();
        this.selectCourse(c.ref);
      })
      .catch(e => {
        this.errorHandler.handle(e);
        this.joiningCourse = false;
      });

  }

  onAddCourse() {
    this.router.navigate(['/pages/courses/new']);
  }
}
