import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CoursesComponent } from './courses.component';
import { Ng2SmartTableModule } from 'ng2-smart-table';
import { ThemeModule } from '../../@theme/theme.module';
import {NbSpinnerModule, NbToastrModule, NbInputModule, NbAccordionModule} from '@nebular/theme';
import { NewCourseComponent } from './new-course/new-course.component';
import {RouterModule} from "@angular/router";
import { NewCourseFromTemplateComponent } from './new-course/new-course-from-template/new-course-from-template.component';

@NgModule({
  imports: [
    ThemeModule,
    CommonModule,
    NbSpinnerModule,
    NbInputModule,
    Ng2SmartTableModule,
    NbAccordionModule,
    RouterModule,
  ],
  declarations: [
    CoursesComponent,
    NewCourseComponent,
    NewCourseFromTemplateComponent,
  ],
  providers: [
  ],
})
export class CoursesModule { }
