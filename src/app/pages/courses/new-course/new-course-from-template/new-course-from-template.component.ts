import { Component, HostListener, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { CourseService } from '../../../../@core/data/course.service';
import { CourseTemplateMetadata } from '../../../../@core/api';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Observable } from 'rxjs';
import { FormControlUtil } from '../../../../@theme/util/FormControlUtil';
import { SessionService } from '../../../../@core/session.service';
import { GlErrorHandlerService } from '../../../../@theme/error-handler.service';
import { RxwebValidators } from '@rxweb/reactive-form-validators';
import { NgbTimeStruct } from '@ng-bootstrap/ng-bootstrap';

import * as moment from 'moment';

@Component({
  selector: 'ngx-new-course-from-template',
  templateUrl: './new-course-from-template.component.html',
  styleUrls: ['./new-course-from-template.component.scss'],
})
export class NewCourseFromTemplateComponent implements OnInit {

  templateId: string;
  template: CourseTemplateMetadata;

  courseCreateForm: FormGroup;
  saving: boolean = false;

  startDate: Date = new Date();

  reviewStepNames: string[];
  reviewStepIds: string[][];

  deadlineTime: NgbTimeStruct = { hour: 22, minute: 0, second: 0 };

  readonly controlStatus = FormControlUtil.controlStatus;

  constructor(
    private courseService: CourseService,
    private sessionService: SessionService,
    private fb: FormBuilder,
    private errorHandler: GlErrorHandlerService,
    private route: ActivatedRoute,
    private router: Router,
  ) {
    this.route.params.subscribe(params => {
      this.templateId = params.templateId;
      this.courseService.getCourseTemplate(this.templateId)
        .then(t => {
          this.template = t;

          const reviewStepNames = [];
          this.template.chapters.flatMap(c => c.reviewSteps)
            .forEach(r => {
              if (!reviewStepNames.includes(r.name)) {
                reviewStepNames.push(r.name);
              }
            });

          this.reviewStepNames = reviewStepNames;

          this.reviewStepIds = [];
          for (let i = 0; i < this.template.chapters.length; i++) {
            this.reviewStepIds.push([]);
            for (let j = 0; j < this.reviewStepNames.length; j++) {
              const reviewStep = this.template.chapters[i].reviewSteps.find(r => r.name === this.reviewStepNames[j]);
              this.reviewStepIds[i].push(reviewStep ? reviewStep.id : null);
            }
          }

          this.buildCourseCreateForm();
        });
    });
  }

  ngOnInit() {

    this.sessionService.setCurrentPage({
      icon: 'fa fa-graduation-cap',
      title: 'New Course',
    });
  }

  private buildCourseCreateForm() {

    this.courseCreateForm = this.fb.group({
      'templateId': this.templateId,
      'courseName': this.fb.control(this.template.name, [Validators.required]),
      'year': this.fb.control(new Date().getFullYear(), [Validators.required, RxwebValidators.numeric({allowDecimal: false})]),
      'quarter': this.fb.control(Math.floor(((new Date().getMonth() + 12 - 6) % 12) / 3) + 1, [
        Validators.required,
        Validators.min(0),
        Validators.max(6),
        RxwebValidators.numeric({
          allowDecimal: false,
        })]),
      'courseCode': this.fb.control('', [Validators.required]),
      'anonymousUsers': this.fb.control(true, [Validators.required]),
      'anonymousReviews': this.fb.control(false, [Validators.required]),
      'deadlinesOverride': this.fb.group(this.calculateDeadlineOverrides(this.startDate)),
    });
  }

  setStartDate() {
    this.courseCreateForm.get('deadlinesOverride').patchValue(this.calculateDeadlineOverrides(this.startDate));
  }

  private calculateDeadlineOverrides(startDate: Date): { [key: string]: Date; } {
    return this.template.chapters
      .flatMap(c => c.reviewSteps)
      .reduce((obj, rs) => { obj[rs.id] = moment(startDate).add(rs.deadlineOffset, 'days').toDate(); return obj; },  {});
  }

  setAllDeadlineTimes() {
    for (const c of Object.values((<FormGroup>this.courseCreateForm.get('deadlinesOverride')).controls)) {
      if (c.value instanceof Date) {
        const date = new Date(c.value);
        date.setHours(this.deadlineTime.hour, this.deadlineTime.minute, this.deadlineTime.second);
        if (NewCourseFromTemplateComponent.datesEqual(c.value, date)) {
          c.setValue(date);
          c.markAsDirty();
        }
      }
    }
  }

  static datesEqual(d1: Date, d2: Date): boolean {
    return Math.abs(d1.valueOf() - d2.valueOf()) > 0;
  }

  //create course with data from this page, then go to dashboard
  createCourse() {

    this.saving = true;
    this.courseService.createCourseFromTemplate(this.courseCreateForm.value)
      .then(c => {
        this.saving = false;
        this.sessionService.setSelectedCourse(c);
        this.sessionService.refreshLoggedInUser();
        this.courseCreateForm.markAsPristine();
        this.router.navigate(['/pages/dashboard']);
      })
      .catch(e => {
        this.saving = false;
        this.errorHandler.handle(e);
      });
  }

  // @HostListener allows us to also guard against browser refresh, close, etc.
  @HostListener('window:beforeunload')
  canDeactivate(): boolean | Observable<boolean> {
    return this.courseCreateForm.pristine;
  }

}
