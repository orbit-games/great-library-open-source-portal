import {Component, OnInit} from '@angular/core';
import {SessionService} from "../../../@core/session.service";
import {CourseTemplateMetadata} from "../../../@core/api";
import {CourseService} from "../../../@core/data/course.service";
import {ActivatedRoute, Router} from "@angular/router";

@Component({
  selector: 'ngx-new-course',
  templateUrl: './new-course.component.html',
  styleUrls: ['./new-course.component.scss'],
})
export class NewCourseComponent implements OnInit {

  templates: CourseTemplateMetadata[];

  constructor(
    private sessionService: SessionService,
    private courseService: CourseService,
  ) {
    this.courseService.getCourseTemplates()
      .then(t => this.templates = t);
  }

  ngOnInit() {
    this.sessionService.setCurrentPage({
      icon: 'fa fa-graduation-cap',
      title: 'New Course',
    });
  }

}
