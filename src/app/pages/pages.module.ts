import { NgModule } from '@angular/core';
import { NbButtonModule } from '@nebular/theme';

import { ThemeModule } from '../@theme/theme.module';
import { ConversationsModule } from './conversations/conversations.module';
import { CoursesModule } from './courses/courses.module';
import { DashboardModule } from './dashboard/dashboard.module';
import { LandingPageComponent } from './landing-page/landing-page.component';
import { MiscellaneousModule } from './miscellaneous/miscellaneous.module';
import { PagesRoutingModule } from './pages-routing.module';
import { PagesComponent } from './pages.component';
import { ProgressModule } from './progress/progress.module';
import { GlSelectCourseGuard } from './select-course-guard.service';
import { ServerModule } from './server/server.module';
import { UnderConstructionComponent } from './under-construction/under-construction.component';
import { UsersModule } from './users/users.module';
import { StructureModule } from './structure/structure.module';

const PAGES_COMPONENTS = [
  PagesComponent,
];

@NgModule({
  imports: [
    PagesRoutingModule,
    ThemeModule,
    NbButtonModule,
    DashboardModule,
    MiscellaneousModule,
    CoursesModule,
    ProgressModule,
    UsersModule,
    ConversationsModule,
    ServerModule,
    StructureModule,
  ],
  declarations: [
    ...PAGES_COMPONENTS,
    UnderConstructionComponent,
    LandingPageComponent,
  ],
  providers: [
    GlSelectCourseGuard,
  ],
})
export class PagesModule {
}
