import { NbMenuItem } from '@nebular/theme';
import { Role } from '../@core/api';

export const MENU_ITEMS: NbMenuItem[] = [
  {
    title: 'DASHBOARD',
    link: '/pages/dashboard',
    icon: 'fa fa-chart-area',
    home: true,
  },
  {
    title: 'STRUCTURE',
    icon: 'fa fa-cogs',
    data: {roles: [Role.TEACHER, Role.ADMINISTRATOR]},
    children: [
      {
        title: 'Course',
        // icon: 'icon ion-md-school',
        link: '/pages/structure/course',
      },
      {
        title: 'Chapters',
        // icon: 'icon ion-md-albums',
        link: '/pages/structure/chapters',
      },
      {
        title: 'Review Steps',
        // icon: 'icon ion-md-albums',
        link: '/pages/structure/review-steps',
      },
      {
        title: 'Deadlines',
        // icon: 'icon ion-md-timer',
        link: '/pages/structure/deadlines',
      },
    ],
  }, {
    title: 'CONTENT',
    icon: 'fas fa-book-open',
    data: { roles: [ Role.TEACHER, Role.ADMINISTRATOR ]},
    children: [
      {
        title: 'Sections',
        // icon: 'icon ion-md-albums',
        link: '/pages/structure/sections',
        pathMatch: 'prefix',
      },
      {
        title: 'Resources',
        // icon: 'icon ion-md-albums',
        link: '/pages/structure/resources',
        pathMatch: 'prefix',
      },
    ],
  },
  {
    title: 'USERS',
    icon: 'fa fa-users',
    data: { roles: [ Role.TEACHER, Role.ADMINISTRATOR ]},
    children: [
      {
        title: 'Administrators',
        // icon: 'icon ion-md-people',
        link: '/pages/users/administrators',
        data: { roles: [ Role.ADMINISTRATOR ]},
      },
      {
        title: 'Teachers',
        // icon: 'icon ion-md-people',
        link: '/pages/users/teachers',
      },
      {
        title: 'Students',
        // icon: 'icon ion-md-albums',
        link: '/pages/users/students',
      },
    ],
  },
  {
    title: 'PROGRESS',
    icon: 'fa fa-tasks',
    link: '/pages/progress',
    data: { roles: [ Role.TEACHER, Role.ADMINISTRATOR ]},
    pathMatch: 'prefix',
  },
  {
    title: 'CONVERSATIONS',
    icon: 'fa fa-comments',
    link: '/pages/conversations',
    pathMatch: 'prefix',
  },
  {
    title: 'SERVER',
    icon: 'fa fa-server',
    link: '/pages/server',
    data: { roles: [ Role.ADMINISTRATOR ]},
  },
];
