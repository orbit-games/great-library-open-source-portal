import {Component, forwardRef, Input, OnInit} from '@angular/core';
import {ControlValueAccessor, NG_VALUE_ACCESSOR} from '@angular/forms';
import {FileUploadMetadata} from '../../../@core/api';
import {FileService} from '../../../@core/data/file.service';
import {HttpEvent} from '@angular/common/http';
import {GlErrorHandlerService} from '../../../@theme/error-handler.service';

@Component({
  selector: 'ngx-file-uploader',
  templateUrl: './file-uploader.component.html',
  styleUrls: ['./file-uploader.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => FileUploaderComponent),
      multi: true,
    },
  ],
})
export class FileUploaderComponent implements OnInit, ControlValueAccessor {

  @Input() status: string;
  // Default to 2GB
  @Input() maxSize: number = 2147483648;

  fileUploadMetadata: FileUploadMetadata;
  file: any;
  baseDropValid: boolean;
  uploading: boolean;

  disabled: boolean = false;

  httpEvent: HttpEvent<FileUploadMetadata>;
  progress: number = 0;

  private onChange: (_: any) => void;
  private onTouched: (_: any) => any;

  constructor(
    private fileService: FileService,
    private errorHandler: GlErrorHandlerService,
  ) { }

  ngOnInit() {
  }

  registerOnChange(fn: (_: any) => void): void {
    this.onChange = fn;
  }

  registerOnTouched(fn: (_: any) => any): void {
    this.onTouched = fn;
  }

  setDisabledState(isDisabled: boolean): void {
    this.disabled = isDisabled;
  }

  writeValue(obj: any): void {
    this.fileUploadMetadata = obj;
  }

  onUploadClicked() {
    if (!this.file) {
      return;
    }

    this.progress = 0;
    this.uploading = true;
    this.fileService.uploadFile(this.file,
        e => {
      this.httpEvent = e;
    })
      .then(f => {
        this.uploading = false;
        this.progress = 100;
        this.fileUploadMetadata = f;
        this.onChange(f);
      })
      .catch(err => {
        this.uploading = false;
        this.errorHandler.handle(err);
      });
  }

  fileChanged(file: File[]) {
    if (this.onTouched) {
      this.onTouched(file);
    }
  }
}
