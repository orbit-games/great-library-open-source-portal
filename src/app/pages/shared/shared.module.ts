import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FileUploaderComponent} from './file-uploader/file-uploader.component';
import {ngfModule} from 'angular-file';
import {ThemeModule} from '../../@theme/theme.module';
import {NbSpinnerModule} from '@nebular/theme';


@NgModule({
  declarations: [
    FileUploaderComponent,
  ],
  imports: [
    CommonModule,
    ngfModule,
    ThemeModule,
    NbSpinnerModule,
  ],
  providers: [
    FileUploaderComponent,
  ],
  exports: [
    FileUploaderComponent,
  ],
})
export class SharedModule { }
