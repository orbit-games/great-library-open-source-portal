import {Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {NbTabsetComponent} from '@nebular/theme';

import {
  Chapter,
  Course,
  CourseProgressExportColumnDataType,
  CourseProgressExportColumnSpecification,
  CourseProgressExportColumnType,
  CourseProgressExportStepExecution,
  StepType,
} from '../../@core/api';
import {SessionService} from '../../@core/session.service';
import {ProgressTabLegendComponent} from './progress-tab/progress-tab-legend/progress-tab-legend.component';
import {CourseService} from '../../@core/data/course.service';
import {GlErrorHandlerService} from '../../@theme/error-handler.service';
import {HttpHeaders, HttpResponse} from '@angular/common/http';
import {ActivatedRoute, Router} from '@angular/router';
import {Subscription} from 'rxjs';
import {ProgressStateService} from './progress-state.service';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'gl-progress',
  templateUrl: './progress.component.html',
  styleUrls: ['./progress.component.scss'],
})
export class ProgressComponent implements OnInit, OnDestroy {

  progressTabGuideComponent = ProgressTabLegendComponent;

  downloadingProgressExport = false;
  course: Course;

  showInactiveUsers: boolean;

  private showInactiveUsersSubscription: Subscription;


  @ViewChild('tabSet', {static: false}) tabSet: NbTabsetComponent;
  tabs: any[] = [];

  constructor(
    private progressStateService: ProgressStateService,
    private courseService: CourseService,
    private errorHandler: GlErrorHandlerService,
    private router: Router,
    private route: ActivatedRoute,
    private sessionService: SessionService) { }

  ngOnInit() {

    this.sessionService.getSelectedCourse().subscribe(c => {
      this.setCourse(c);
    });

    this.sessionService.setCurrentPage({
      icon: 'fa fa-tasks',
      title: 'Progress',
    });

    this.showInactiveUsersSubscription =
      this.progressStateService.showInactiveUsers.subscribe(s => this.showInactiveUsers = s);
  }

  ngOnDestroy(): void {
    this.showInactiveUsersSubscription.unsubscribe();
  }

  setCourse(course: Course) {
    this.course = course;
    const tabs = [];
    for (const chapter of course.chapters) {
      tabs.push({
        title: chapter.name,
        route: '/pages/progress/chapter/' + chapter.ref,
      });
    }
    this.tabs = tabs;
    this.setChapter(course.chapters[0], 0);
  }

  setChapter(chapter: Chapter, chapterIdx: number) {
    // console.log('Navigating to: ' + '/pages/progress/chapter/' + chapter.ref);
    this.router.navigate(['/pages/progress/chapter', chapter.ref]);
  }

  downloadProgressExport() {

    this.downloadingProgressExport = true;
    this.courseService.downloadCourseProgressExport(this.course.ref, {})
      .then((resp: HttpResponse<Blob>) => {

        this.downloadingProgressExport = false;
        ProgressComponent.createAndDownloadBlobFile(resp.body, ProgressComponent.extractFilename(resp.headers));
      })
      .catch(e => {
        this.downloadingProgressExport = false;
        this.errorHandler.handle(e);
      });

  }

  downloadGradingExport() {
    this.downloadingProgressExport = true;
    this.courseService.downloadCourseProgressExport(this.course.ref, {
      columns: this.gradingColumns(),
    })
      .then((resp: HttpResponse<Blob>) => {

        this.downloadingProgressExport = false;
        ProgressComponent.createAndDownloadBlobFile(resp.body, ProgressComponent.extractFilename(resp.headers));
      })
      .catch(e => {
        this.downloadingProgressExport = false;
        this.errorHandler.handle(e);
      });
  }

  gradingColumns(): CourseProgressExportColumnSpecification[] {

    const res: CourseProgressExportColumnSpecification[] = [];

    res.push({
      columnType: CourseProgressExportColumnType.TOTALMINUTESLATE,
    });
    res.push({
      columnType: CourseProgressExportColumnType.TOTALSKIPPEDSTEPS,
    });

    for (const chapter of this.course.chapters) {
      for (const reviewStep of chapter.reviewSteps) {
        // TODO: Probably include a boolean in the review step that indicates if it is used for grading
        if (reviewStep.stepType === StepType.EVALUATION || reviewStep.stepType === StepType.ASSESSMENT) {
          res.push({
            columnType: CourseProgressExportColumnType.STEPDETAIL,
            chapterRef: chapter.ref,
            reviewStepRef: reviewStep.ref,
            dataType: CourseProgressExportColumnDataType.SCORE,
            stepExecution: CourseProgressExportStepExecution.RECEIVED,
          });
        }
      }
    }
    return res;
  }

  static extractFilename(headers: HttpHeaders): string {
    const contentDispositionHeader = headers.get('Content-Disposition');
    const result = contentDispositionHeader.split(';')[1].trim().split('=')[1];
    return result.replace(/"/g, '');
  }

  static createAndDownloadBlobFile(blob: Blob, filename: string) {

    if (navigator.msSaveBlob) {
        // IE 10+
        navigator.msSaveBlob(blob, filename);
    } else {
        const link = document.createElement('a');

        // Browsers that support HTML5 download attribute
        if (link.download !== undefined) {
            const url = URL.createObjectURL(blob);
            link.setAttribute('href', url);
            link.setAttribute('download', filename);
            link.style.visibility = 'hidden';
            document.body.appendChild(link);
            link.click();
            document.body.removeChild(link);
        }
    }
}

  showInactiveUsersChange(value: any) {
    this.progressStateService.setShowInactiveUsers(value);
  }

}
