import { Component, Input, OnInit } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { ViewCell } from 'ng2-smart-table';

import { ReviewStep, ReviewStepResult } from '../../../@core/api';
import { ProgressHelper } from '../progress-helper';

export interface ReviewStepCellData {
    value: any;
    step: ReviewStep;
    result: ReviewStepResult;
}

export function instanceOfReviewStepCellData(object: any) {
    return 'value' in object &&
            'step' in object &&
            'result' in object;
}

@Component({
    template: `
        <div 
                class="review-step-late" 
                [ngClass]="{
                    'bg-danger': !isForgotHandIn() && isStepLatePending(),
                    'bg-warning': isStepLateHandedIn(),
                    'bg-primary': isForgotHandIn()
                    }" 
                [innerHTML]="displayValue">
        </div>
    `,
    styles: [`
        .review-step-late { 
            width: calc(100% + 2.5rem); 
            height: 3rem; 
            margin: -0.875rem -1.25rem -0.875rem -1.25rem;
            padding: 0.875rem 1.25rem 0.875rem 1.25rem;
        }
    `],
})
export class ReviewStepCellComponent implements OnInit, ViewCell {

    @Input() value: any;
    @Input() rowData: any;

    constructor(private domSanitizer: DomSanitizer) {}

    ngOnInit() {

    }

    get displayValue() {
        return this.domSanitizer.bypassSecurityTrustHtml(instanceOfReviewStepCellData(this.value) ? this.value.value : this.value);
    }

    isStepLateHandedIn() {
        if (this.value !== null && typeof this.value === 'object' && instanceOfReviewStepCellData(this.value)) {
            return ProgressHelper.lateHandedIn(this.value.step, this.value.result);
        } else {
            return false;
        }
    }

    isStepLatePending() {
      if (this.value !== null && typeof this.value === 'object' && instanceOfReviewStepCellData(this.value)) {
        return ProgressHelper.lateStillPending(this.value.step, this.value.result);
      } else {
        return false;
      }
    }

    isForgotHandIn() {
        if (this.value !== null && typeof this.value === 'object' && instanceOfReviewStepCellData(this.value)) {
            return ProgressHelper.forgotHandIn(this.value.step, this.value.result);
        } else {
            return false;
        }
    }
}
