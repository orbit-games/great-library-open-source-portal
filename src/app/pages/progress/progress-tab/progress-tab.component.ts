import { Component, Input, OnInit, OnDestroy } from '@angular/core';
import { LocalDataSource } from 'ng2-smart-table';

import { Chapter, Course, DiscussionType, ReviewerRelation, CourseProgressSummary, Role } from '../../../@core/api';
import { CourseService } from '../../../@core/data/course.service';
import { SessionService, NameType } from '../../../@core/session.service';
import { GlErrorHandlerService } from '../../../@theme/error-handler.service';
import { DomSanitizer } from '@angular/platform-browser';
import { ActivatedRoute, Router } from '@angular/router';
import { compare } from '../../../@core/data/property-compare';
import { wordCount } from '../../../@core/utils/word-count';
import { ReviewStepCellComponent, ReviewStepCellData, instanceOfReviewStepCellData } from './review-step-cell.component';
import { Subscription } from 'rxjs';
import { UserService } from '../../../@core/data/user.service';
import { FormatHelper } from '../../../@theme/pipes/format-helper';
import {ProgressStateService} from '../progress-state.service';

@Component({
  selector: 'gl-progress-tab',
  templateUrl: './progress-tab.component.html',
  styleUrls: ['./progress-tab.component.scss'],
})
export class ProgressTabComponent implements OnInit, OnDestroy {

  chapterRef: string;

  source: LocalDataSource = new LocalDataSource();
  settings: any;

  course: Course;
  chapter: Chapter;
  reviewerRelations: ReviewerRelation[];
  teacherRefs: string[] = [];
  
  showInactiveUsers: boolean;

  nameTypeChangedSubscription: Subscription;
  private showInactiveUsersSubscription: Subscription;


  constructor(
    private sessionService: SessionService,
    private courseService: CourseService,
    private userService: UserService,
    private progressStateService: ProgressStateService,
    private errorHandler: GlErrorHandlerService,
    private domSanitizer: DomSanitizer,
    private router: Router,
    private route: ActivatedRoute) {
  }

  ngOnInit() {

    this.route.params.subscribe(p => {
      this.reviewerRelations = null;
      this.chapterRef = p["chapterRef"];
      this.sessionService.getSelectedCourse().subscribe(c => {
        this.course = c;
        this.setChapter(c.chapters.find(ch => ch.ref === this.chapterRef));
        this.userService.getUsers(c.ref, [ Role.ADMINISTRATOR, Role.TEACHER ])
          .then(teachers => {
            this.teacherRefs = teachers.users.map(t => t.ref);
            if (this.reviewerRelations) {
              this.setReviewerRelations(this.reviewerRelations);
            }
          })
          .catch(e => this.errorHandler.handle(e));
        this.courseService.getCourseChapterReviewerRelations(c.ref, this.chapterRef)
          .then(rrs => this.setReviewerRelations(rrs))
          .catch(e => this.errorHandler.handle(e));
      });
    });

    this.nameTypeChangedSubscription = this.sessionService.nameTypeChanged.subscribe(n => {
      if (this.reviewerRelations) {
        this.setReviewerRelations(this.reviewerRelations);
      }
    });

    this.showInactiveUsersSubscription =
      this.progressStateService.showInactiveUsers.subscribe(s => {
        this.showInactiveUsers = s;
        if (this.reviewerRelations) {
          this.setReviewerRelations(this.reviewerRelations);
        }
      });
  }
  
  ngOnDestroy() {
    this.nameTypeChangedSubscription.unsubscribe();
    this.showInactiveUsersSubscription.unsubscribe();
}

  compareReviewStepCell(direction: any, a: any, b: any): number {

    const valueMapper = v => {
      if (instanceOfReviewStepCellData(v)) {
        return (<ReviewStepCellData>v).value;
      } else {
        return v;
      }
    };

    const va = valueMapper(a);
    const vb = valueMapper(b);
    if (va === '' && vb === '') {
      return 0;
    } else if (va === '') {
      return 1;
    } else if (vb === '') {
      return -1;
    }

    const comparator = compare(v => valueMapper(v));
    return direction > 0 ? comparator(a, b) : comparator(b, a);
  }

  setChapter(chapter: Chapter) {

    this.source.empty();

    this.chapter = chapter;
    const columns = {
      submitter: {
        title: 'Submitter',
      },
      reviewer: {
        title: 'Reviewer',
      },
    };
	
    for (const step of chapter.reviewSteps) {

      if (step.disabled) {
        continue;
      }
	  
      if (step.fileUpload) {
        columns[step.ref + '.upload'] = {
          title: step.name,
          type: 'custom',
          renderComponent: ReviewStepCellComponent,
          compareFunction: this.compareReviewStepCell,
        };
      }
      if (step.quiz) {
        columns[step.ref + '.quiz'] = {
          title: step.name + ' (score)',
          type: 'custom',
          renderComponent: ReviewStepCellComponent,
          compareFunction: this.compareReviewStepCell,
          valuePrepareFunction: v => {
            if (!v.result || !v.result.quizResult) {
              return v;
            }
            const quizScore = v.result.quizResult.totalScore;
            if (step.quiz.elements.find(e => e.options.find(o => o.points < 0) != null) != null) {
              // The scores can be below zero, so we display the score as "+2" or "-1" or something like that
              v.value = quizScore > 0 ? '+' + quizScore : quizScore;
            } else {
              v.value = Number(quizScore * 10 / step.quiz.maxScore).toFixed(1);
            }

            return v;
          },
        };
      }
      if (step.discussionType !== DiscussionType.NONE) {
        columns[step.ref + '.discussion'] = {
          title: step.name + ' (words)',
          type: 'custom',
          renderComponent: ReviewStepCellComponent,
          compareFunction: this.compareReviewStepCell,
        };
      }
    }
    this.settings = {
      actions: {
        add: false,
        edit: true,
        delete: false,
      },
      edit: {
        editButtonContent: 'View',
      },
      filter: {
        inputClass: 'gl-filter',
      },
      mode: 'external',
      pager: {
        display: false,
      },
      columns: columns,
    };
  }

  getName(summary: CourseProgressSummary) {
    if (summary == null) {
      return null;
    }

    if (this.sessionService.nameType === NameType.DISPLAY_NAME) {
      return summary.displayName;
    } else {
      return summary.fullName;
    }
  }

  setReviewerRelations(reviewerRelations: ReviewerRelation[]) {
    this.reviewerRelations = reviewerRelations;

    const reviewRelationResults = [];
    for (const reviewerRelation of reviewerRelations) {

      // Ignore teachers in the progress overview
      if (this.teacherRefs.includes(reviewerRelation.submitter)) {
        continue;
      }

      const submitter = this.course.users.find(u => u.userRef === reviewerRelation.submitter);
      const reviewer = this.course.users.find(u => u.userRef === reviewerRelation.reviewer);

      if (!this.showInactiveUsers
        && (submitter == null || !submitter.active)
        && (reviewer == null || !reviewer.active)) {
        continue;
      }

      const result = {
        ref: reviewerRelation.ref,
        submitter: this.getName(submitter),
        reviewer: this.getName(reviewer),
      };
	  
      for (const step of this.chapter.reviewSteps) {

        if (step.disabled) {
          continue;
        }

        let reviewStepResult = reviewerRelation.reviewStepResults.find(rsr => rsr.reviewStepRef === step.ref);
        // if (!reviewStepResult) {
        //   continue;
        // }
        let uploadStepCell: ReviewStepCellData = {
          value: '',
          step: step,
          result: reviewStepResult,
        };
        if (step.fileUpload && reviewStepResult && reviewStepResult.fileUploadMetadata) {
          uploadStepCell.value = `<div style="text-overflow: ellipsis; overflow: hidden; white-space: nowrap; max-width: 15rem;"><a href="${reviewStepResult.fileUploadMetadata.downloadUrl}" target="_blank">${FormatHelper.toHumanReadableDate(reviewStepResult.fileUploadMetadata.created)}</a></div>`;
          result[step.ref + '.upload'] = uploadStepCell;
        } else if (step.fileUpload) {
          result[step.ref + '.upload'] = uploadStepCell;
        }

        const quizStepCell: ReviewStepCellData = {
          value: '',
          step: step,
          result: reviewStepResult,
        };
        if (step.quiz && reviewStepResult && reviewStepResult.quizResult) {
          quizStepCell.value = reviewStepResult.quizResult.totalScore;
          result[step.ref + '.quiz'] = quizStepCell;
        } else if (step.quiz) {
          result[step.ref + '.quiz'] = quizStepCell;
        }
		
        const discussionStepCell: ReviewStepCellData = {
          value: '',
          step: step,
          result: reviewStepResult,
        };
        if (step.discussionType !== DiscussionType.NONE && reviewStepResult && reviewStepResult.arguments) {
          discussionStepCell.value = this.sum(reviewStepResult.arguments.map(a => wordCount(a.message)));
          result[step.ref + '.discussion'] = discussionStepCell;
        } else if (step.discussionType !== DiscussionType.NONE) {
          result[step.ref + '.discussion'] = discussionStepCell;
        }
      }
      reviewRelationResults.push(result);
    }
    reviewRelationResults.sort(compare(r => r.submitter, compare(r => r.reviewer)));

    this.source.reset();
    this.source.load(reviewRelationResults);
  }

  onSelectReviewerRelation(event) {

    const reviewerRelationRef = event.data.ref;
    this.router.navigate(['reviewer-relation', reviewerRelationRef], {relativeTo: this.route});

  }

  private sum(ns: number[]) {
    let sum = 0;
    ns.forEach(n => sum += n);
    return sum;
  }
}
