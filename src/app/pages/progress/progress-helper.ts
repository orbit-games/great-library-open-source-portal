import { ReviewStepResult, ReviewStep } from "../../@core/api";
import * as moment from 'moment';

export class ProgressHelper {

  public static handedIn(reviewStepResult: ReviewStepResult): Date {

    if (!reviewStepResult) {
      return null;
    }

    if (reviewStepResult.markedComplete) {
      // we only count it as handed in if it was marked as complete:
      if (reviewStepResult.markedComplete > reviewStepResult.lastModified) {
        return reviewStepResult.markedComplete;
      } else {
        return reviewStepResult.lastModified;
      }
    }
  }

  public static deadlineExpired(reviewStep: ReviewStep): boolean {
    return moment(reviewStep.deadline) < moment();
  }

  public static lateStillPending(reviewStep: ReviewStep, reviewStepResult: ReviewStepResult) {
    // TODO Use this to turn cells red
    return this.deadlineExpired(reviewStep) && (reviewStepResult == null || !reviewStepResult.markedComplete);
  }

  public static lateHandedIn(reviewStep: ReviewStep, reviewStepResult: ReviewStepResult): boolean {
    return this.deadlineExpired(reviewStep)
      && ((!this.handedIn(reviewStepResult)) || (moment(reviewStep.deadline) < moment(this.handedIn(reviewStepResult))));
  }

  /**
   * Returns true iff the user is ready to hand in a late step, but hasn't completed the step yet (i.e. hasn't clicked the "hand in" button yet)
   * @param reviewStep The step to check
   * @param reviewStepResult The result of the step for the user
   */
  public static forgotHandIn(reviewStep: ReviewStep, reviewStepResult: ReviewStepResult): boolean {
    return this.lateStillPending(reviewStep, reviewStepResult) &&
      reviewStepResult && reviewStepResult.readyToComplete && !reviewStepResult.markedComplete;
  }

}
