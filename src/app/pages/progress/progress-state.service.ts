import { Injectable } from '@angular/core';
import {Observable, ReplaySubject, Subject, Subscribable} from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class ProgressStateService {

  showInactiveUsers = new ReplaySubject<boolean>(1);

  constructor() {
    this.setShowInactiveUsers(false);
  }

  setShowInactiveUsers(val: boolean) {
    this.showInactiveUsers.next(val);
  }
}
