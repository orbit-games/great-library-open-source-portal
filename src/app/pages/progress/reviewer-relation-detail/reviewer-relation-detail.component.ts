import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { ActivatedRoute, Router } from '@angular/router';
import * as moment from 'moment';

import {
  Chapter,
  Course,
  CourseProgressSummary,
  DiscussionType,
  PeerType,
  QuizElement,
  QuizElementOption,
  ReviewerRelation,
  ReviewStep,
  ReviewStepResult,
  Argument,
} from '../../../@core/api';
import { CourseService } from '../../../@core/data/course.service';
import { SessionService } from '../../../@core/session.service';
import { wordCount } from '../../../@core/utils/word-count';
import { GlErrorHandlerService } from '../../../@theme/error-handler.service';
import { ProgressHelper } from '../progress-helper';


@Component({
  selector: 'ngx-reviewer-relation-detail',
  templateUrl: './reviewer-relation-detail.component.html',
  styleUrls: ['./reviewer-relation-detail.component.scss']
})
export class ReviewerRelationDetailComponent implements OnInit {

  submitter: CourseProgressSummary;
  reviewer?: CourseProgressSummary;
  reviewerRelation: ReviewerRelation;
  course: Course;
  chapter: Chapter;

  submitterColor = '#00d977';
  reviewerColor = '#7659ff';

  constructor(private sessionService: SessionService, private courseService: CourseService, private errorHandler: GlErrorHandlerService,
    private router: Router, private route: ActivatedRoute, private domSanitizer: DomSanitizer, private http: HttpClient) {
  }

  ngOnInit() {

    this.route.params.subscribe(params => {

      this.reviewerRelation = null;

      let chapterRef = params["chapterRef"]
      let reviewerRelationRef = params["reviewerRelationRef"]

      this.sessionService.getSelectedCourse().subscribe(course => {
        // TODO Use a call to get just the reviewer relation we need, instead of all of them...
        this.course = course;
        this.chapter = course.chapters.find(ch => ch.ref === chapterRef);
        this.courseService.getCourseChapterReviewerRelation(course.ref, chapterRef, reviewerRelationRef).then(reviewerRelation => {
          this.setReviewerRelation(reviewerRelation);
        })
      });

      this.sessionService.setCurrentPage({
        icon: "fa fa-tasks",
        title: "Progress",
        list: {
          clickHandler: () => this.router.navigate(['../..'], { relativeTo: this.route }),
          tooltip: "Back to the overview",
          visible: true,
        }
      });
    })
  }

  private setReviewerRelation(reviewerRelation: ReviewerRelation) {

    this.reviewerRelation = reviewerRelation;
    this.submitter = this.course.users.find(u => u.userRef === reviewerRelation.submitter)
    this.reviewer = this.course.users.find(u => u.userRef === reviewerRelation.reviewer)
  }

  reviewStep(ref: string): ReviewStep {
    return this.chapter.reviewSteps.find(r => r.ref === ref);
  }

  result(reviewStep: ReviewStep): ReviewStepResult {
    return this.reviewerRelation.reviewStepResults.find(s => s.reviewStepRef === reviewStep.ref);
  }


  fileUploadUrl(reviewStepResult: ReviewStepResult) {
    return this.domSanitizer.bypassSecurityTrustUrl(reviewStepResult.fileUploadMetadata.downloadUrl);
  }

  fileUploadResourceUrl(reviewStepResult: ReviewStepResult) {
    return reviewStepResult.fileUploadMetadata.downloadUrl;
  }
  // fileUploadResourceUrl(reviewStepResult: ReviewStepResult) {
  //   return this.http.get(reviewStepResult.fileUploadMetadata.downloadUrl, { responseType: 'blob' }).pipe(map(res => {
  //     return res.slice(null, null, 'application/pdf' )
  //   }));
  // }

  stepUser(reviewStep: ReviewStep): CourseProgressSummary {
    return reviewStep.peerType == PeerType.REVIEWER ? this.reviewer : this.submitter;
  }

  opponentUser(reviewStep: ReviewStep): CourseProgressSummary {
    return reviewStep.peerType == PeerType.REVIEWER ? this.submitter : this.reviewer;
  }

  name(courseProgressSummary: CourseProgressSummary): string {
    if (!courseProgressSummary) {
      return null;
    }

    if (!courseProgressSummary.displayName) {
      if (this.submitter === courseProgressSummary) {
        return 'The submitter';
      } else {
        return 'The reviewer';
      }
    } else {
      return courseProgressSummary.displayName;
    }
  }

  stepUserDefaultName(reviewStep: ReviewStep): string {
    return reviewStep.peerType === PeerType.SUBMITTER ? 'The submitter' : 'The reviewer';
  }

  opponentUserDefaultName(reviewStep: ReviewStep): string {
    return reviewStep.peerType === PeerType.SUBMITTER ? 'The reviewer' : 'The submitter';
  }

  selectedQuizOptions(reviewStepResult: ReviewStepResult, elem: QuizElement): QuizElementOption[] {

    let answer = reviewStepResult.quizResult.answers.find(a => a.quizElementRef === elem.ref)
    if (!answer) {
      return null;
    } else {
      return answer.answers.map(a => elem.options.find(o => o.ref === a));
    }
  }

  argumentWordCount(reviewStepResult: ReviewStepResult): number {

    if (!reviewStepResult.arguments) {
      return 0;
    } else {
      return this.sum(reviewStepResult.arguments.map(a => wordCount(a.message)))
    }
  }

  handedIn(reviewStepResult: ReviewStepResult): Date {
    return ProgressHelper.handedIn(reviewStepResult);
  }

  late(reviewStep: ReviewStep, reviewStepResult: ReviewStepResult) {
    return ProgressHelper.lateStillPending(reviewStep, reviewStepResult) || ProgressHelper.lateHandedIn(reviewStep, reviewStepResult);
  }

  getArgumentMessage(argumentRef: string) {
    for (let reviewStepResult of this.reviewerRelation.reviewStepResults) {
      if (reviewStepResult.arguments) {
        let argument = reviewStepResult.arguments.find(a => a.ref === argumentRef)
        if (argument) {
          return argument.message;
        }
      }
    }
  }

  argumentIndexInStep(argumentRef: string) {
    for(let reviewStepResult of this.reviewerRelation.reviewStepResults) {
      if (reviewStepResult.arguments) {
        let indexInStep = reviewStepResult.arguments.findIndex(a => a.ref === argumentRef)
        if (indexInStep >= 0) {
          return indexInStep;
        }
      }
    }
    return -1;
  }

  argumentName(step: ReviewStep, argument: Argument): string {

    if (!argument) {
      return 'Discussion'
    }

    if (argument.replyToArgumentRef) {
      let replyTo = this.argumentIndexInStep(argument.replyToArgumentRef)
      return "Reply to argument " + (replyTo + 1)
    }
    var argumentType = step.allowedArguments.find(a => a.ref === argument.argumentTypeRef);
    if (!argumentType) {
      return 'Discussion'
    }
    var title = argumentType.name;
    while (argumentType && argumentType.parentRef) {
      argumentType = step.allowedArguments.find(a => a.ref === argumentType.parentRef);
      title = argumentType.name + " > " + title;
    }
    return title;
  }

  private sum(ns: number[]) {
    var sum = 0;
    ns.forEach(n => sum += n);
    return sum;
  }

  deadlineExpired(step: ReviewStep): boolean {
    return ProgressHelper.deadlineExpired(step);
  }

  now() {
    return moment();
  }

  moment(date: Date) {
    return moment(date);
  }
}
