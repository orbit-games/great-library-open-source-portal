import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { NbBadgeModule, NbChatModule, NbSpinnerModule, NbPopoverModule, NbButtonModule } from '@nebular/theme';
import { PdfViewerModule } from 'ng2-pdf-viewer';
import { Ng2SmartTableModule } from 'ng2-smart-table';

import { ThemeModule } from '../../@theme/theme.module';
import { ProgressTabComponent } from './progress-tab/progress-tab.component';
import { ProgressComponent } from './progress.component';
import { ReviewerRelationDetailComponent } from './reviewer-relation-detail/reviewer-relation-detail.component';
import { ReviewStepCellComponent } from './progress-tab/review-step-cell.component';
import { ProgressTabLegendComponent } from './progress-tab/progress-tab-legend/progress-tab-legend.component';
import {ProgressStateService} from './progress-state.service';

@NgModule({
  imports: [
    ThemeModule,
    CommonModule,
    NbSpinnerModule,
    NbChatModule,
    NbBadgeModule,
    NbPopoverModule,
    NbButtonModule,
    Ng2SmartTableModule,
    PdfViewerModule,
  ],
  declarations: [
    ProgressComponent,
    ProgressTabComponent,
    ReviewerRelationDetailComponent,
    ReviewStepCellComponent,
    ProgressTabLegendComponent,
  ],
  providers: [
    ProgressStateService,
  ],
  entryComponents: [
    ReviewStepCellComponent,
    ProgressTabLegendComponent,
  ],
})
export class ProgressModule { }
