import {Component, OnDestroy, OnInit} from '@angular/core';
import { NbThemeService } from '@nebular/theme';
import { takeWhile } from 'rxjs/operators/takeWhile' ;
import { SessionService } from '../../@core/session.service';
import { Course } from '../../@core/api';
import { Subscription } from 'rxjs';

interface CardSettings {
  title: string;
  iconClass: string;
  type: string;
}

@Component({
  selector: 'ngx-dashboard',
  styleUrls: ['./dashboard.component.scss'],
  templateUrl: './dashboard.component.html',
})
export class DashboardComponent implements OnInit, OnDestroy {

  course: Course;
  courseChangedSubscription: Subscription;

  constructor(private sessionService: SessionService) {
  }

  ngOnInit() {

    this.sessionService.setCurrentPage({
      icon: "fa fa-cogs",
      title: "Dashboard"
    });

    this.sessionService.getSelectedCourse().subscribe(c => this.course = c);
    this.courseChangedSubscription = this.sessionService.selectedCourseChanged.subscribe(c => this.course = c);
    
  }

  ngOnDestroy() {
  }
}
