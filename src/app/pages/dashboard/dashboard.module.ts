import { NgModule } from '@angular/core';
import { NbSpinnerModule } from '@nebular/theme';

import { ThemeModule } from '../../@theme/theme.module';
import { DashboardComponent } from './dashboard.component';


@NgModule({
  imports: [
    ThemeModule,
    NbSpinnerModule,
  ],
  declarations: [
    DashboardComponent,
  ],
})
export class DashboardModule { }
