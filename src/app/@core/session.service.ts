import {Course, Role, User} from './api';
import {Injectable} from '@angular/core';
import {UserService} from './data/user.service';
import {CourseService} from './data/course.service';
import {Observable, of as observableOf, Subject} from 'rxjs';
import {Router} from '@angular/router';
import {NbAuthService, NbAuthToken} from '@nebular/auth';
import {map, share} from 'rxjs/operators';

export const LAST_SELECTED_COURSE_PROPERTY = 'P_LastSelectedCourse';
export const SELECTED_NAME_TYPE_PROPERTY = 'P_SelectedNameType';

export interface PageOptions {
    icon?: string;
    title: string;
    list?: ActionOptions;
    new?: ActionOptions;
    view?: ActionOptions;
    edit?: ActionOptions;
    remove?: ActionOptions;
}
export interface ActionOptions {
    visible: boolean;
    clickHandler: () => any;
    tooltip?: string;
}

export enum NameType {
    DISPLAY_NAME = 'DISPLAY_NAME',
    FULL_NAME = 'FULL_NAME',
}

@Injectable()
export class SessionService {

    private initialized = new Subject<boolean>();
    private initializing = true;

    private loggedInUser: User = null;
    public loggedInUserChanged = new Subject<User>();
    private selectedCourse: Course = null;
    public selectedCourseChanged = new Subject<Course>();
    private currentPage: PageOptions;
    public pageChanged = new Subject<PageOptions>();

    /**
     * What type of name to show.
     * Should only be changed using the "toggleNameType()" method
     */
    public nameType: NameType = NameType.DISPLAY_NAME;
    public nameTypeChanged = new Subject<NameType>();

    constructor(private userService: UserService, private courseService: CourseService, private router: Router, private authService: NbAuthService) {
        this.init();
    }

    private init() {
        this.initialized.subscribe(initialized => this.initializing = !initialized);

        this.authService.getToken().subscribe(t => this.tokenChangedHandler(t));
        this.authService.onTokenChange().subscribe(t => this.tokenChangedHandler(t));
    }

    private tokenChangedHandler(token: NbAuthToken) {
        if (token.isValid()) {
            this.userService.getLoggedInUser()
            .then(u => {
                this.setLoggedInUser(u);
                if (u.properties && u.properties[LAST_SELECTED_COURSE_PROPERTY]) {
                    this.courseService.getCourse(u.properties[LAST_SELECTED_COURSE_PROPERTY])
                        .then(c => {
                            this.selectedCourse = c;
                            this.selectedCourseChanged.next(c);
                            this.initialized.next(true);
                        })
                        .catch(e => {
                            this.selectedCourse = null;
                            this.selectedCourseChanged.next(null);
                            this.initialized.next(true);
                        });
                } else {
                    this.initialized.next(true);
                }

                if (u.role !== Role.LEARNER && u.properties && u.properties[SELECTED_NAME_TYPE_PROPERTY]) {
                    this.nameType = NameType[u.properties[SELECTED_NAME_TYPE_PROPERTY]];
                    if (!this.nameType) {
                        this.nameType = NameType.DISPLAY_NAME;
                    }
                    this.nameTypeChanged.next(this.nameType);
                } else {
                    this.nameType = NameType.DISPLAY_NAME;
                    this.nameTypeChanged.next(this.nameType);
                }
            }).catch(e => {
                this.setLoggedInUser(null);
                this.initialized.next(true);
            });
        } else {
            this.initialized.next(true);
            this.setLoggedInUser(null);
            this.setSelectedCourse(null);
        }
    }

    public setLoggedInUser(user: User) {
        if (this.loggedInUser !== user) {
            this.loggedInUser = user;
            this.loggedInUserChanged.next(user);
        }
    }

    public setSelectedCourse(course: Course) {
        if (this.selectedCourse !== course) {

            if (this.loggedInUser && (!this.selectedCourse || !course || this.selectedCourse.ref !== course.ref)) {
                this.userService.setUserProperty(this.loggedInUser.ref, LAST_SELECTED_COURSE_PROPERTY,
                  {key: LAST_SELECTED_COURSE_PROPERTY, value: course.ref});
            }

            this.selectedCourse = course;
            this.selectedCourseChanged.next(course);

        }
    }

    public getLoggedInUser(): Observable<User>  {
        if (this.initializing) {
            return this.initialized.pipe(
                map(i => this.loggedInUser),
            );
        }
        return observableOf(this.loggedInUser);
    }

    public getSelectedCourse(): Observable<Course> {
        if (this.initializing) {
            return this.initialized.pipe(
                map(i => this.selectedCourse),
            );
        }
        return observableOf(this.selectedCourse);
    }

    public refreshLoggedInUser() {
        this.userService.getLoggedInUser(true)
            .then(u => this.setLoggedInUser(u));
    }

    public refreshCourse(forceRemoteRefresh = false) {
      this.courseService.getCourse(this.selectedCourse.ref, forceRemoteRefresh)
        .then(c => {
          this.selectedCourse = c;
          this.selectedCourseChanged.next(c);
        });
    }

    public logout() {
        this.router.navigate(['/auth/logout']);
    }

    public setNameType(nameType: NameType) {

        this.nameType = nameType;
        this.nameTypeChanged.next(this.nameType);
        if (this.loggedInUser && this.loggedInUser.role !== Role.LEARNER) {
            this.userService.setUserProperty(this.loggedInUser.ref, SELECTED_NAME_TYPE_PROPERTY,
              {key: SELECTED_NAME_TYPE_PROPERTY, value: this.nameType.toString() });
        }
    }

    public toggleNameType() {
        if (this.nameType === NameType.DISPLAY_NAME) {
            this.setNameType(NameType.FULL_NAME);
        } else {
            this.setNameType(NameType.DISPLAY_NAME);
        }
    }

    public setCurrentPage(page: PageOptions) {
        this.currentPage = page;
        this.pageChanged.next(page);
    }

    public getCurrentPage(): PageOptions {
        return this.currentPage;
    }
}
