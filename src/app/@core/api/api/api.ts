export * from './gLRawGame.service';
import { GLRawGameService } from './gLRawGame.service';
export * from './gLRawPortal.service';
import { GLRawPortalService } from './gLRawPortal.service';
export const APIS = [GLRawGameService, GLRawPortalService];
