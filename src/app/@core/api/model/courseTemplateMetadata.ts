/**
 * Your Great Library
 * The API specification for both the portal and the game back-end of the great library project 
 *
 * OpenAPI spec version: 1.16
 * Contact: info@orbitgames.nl
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */
import { CourseTemplateChapterMetadata } from './courseTemplateChapterMetadata';


export interface CourseTemplateMetadata { 
    /**
     * Unique reference to this template
     */
    id?: string;
    /**
     * Name of the template
     */
    name: string;
    /**
     * Version number for the template. Note that only the latest versions of templates are returned.
     */
    version: number;
    /**
     * Description of the template (e.g. what kind of course it is)
     */
    description?: string;
    chapters: Array<CourseTemplateChapterMetadata>;
}
