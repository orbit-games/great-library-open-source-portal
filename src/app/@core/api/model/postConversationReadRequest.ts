/**
 * Your Great Library
 * The API specification for both the portal and the game back-end of the great library project 
 *
 * OpenAPI spec version: 1.16
 * Contact: info@orbitgames.nl
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */


export interface PostConversationReadRequest { 
    /**
     * The index of the last messsage that the user read (e.g. until where did the user read the conversation).
     */
    readUntil: number;
}
