/**
 * Your Great Library
 * The API specification for both the portal and the game back-end of the great library project 
 *
 * OpenAPI spec version: 1.16
 * Contact: info@orbitgames.nl
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */


/**
 * Metadata of a user agreement
 */
export interface UserAgreement { 
    /**
     * A unique reference to the user agreement
     */
    ref?: string;
    /**
     * The URL of the user agreement PDF
     */
    pdfUrl?: string;
    /**
     * The moment since when this user agreement is active
     */
    startDate?: Date;
}
