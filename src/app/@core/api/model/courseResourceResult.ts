/**
 * Your Great Library
 * The API specification for both the portal and the game back-end of the great library project 
 *
 * OpenAPI spec version: 1.16
 * Contact: info@orbitgames.nl
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */
import { QuizResult } from './quizResult';


export interface CourseResourceResult { 
    /**
     * The unique reference to the resource to which this is the result
     */
    resourceRef?: string;
    quizResult?: QuizResult;
}
