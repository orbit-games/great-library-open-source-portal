/**
 * Your Great Library
 * The API specification for both the portal and the game back-end of the great library project 
 *
 * OpenAPI spec version: 1.16
 * Contact: info@orbitgames.nl
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */
import { Platform } from './platform';


export interface ClientVersion { 
    platform: Platform;
    buildNumber: number;
    version: string;
    /**
     * The URL this version can be download from
     */
    url: string;
    releaseNotes?: string;
    /**
     * Indicates if users on the platform must install this version to continue playing
     */
    forced: boolean;
    releaseDate?: Date;
    revoked?: boolean;
    revokeDate?: Date;
    /**
     * The reason why this version was revoked
     */
    revokeReason?: string;
}
