/**
 * Your Great Library
 * The API specification for both the portal and the game back-end of the great library project 
 *
 * OpenAPI spec version: 1.16
 * Contact: info@orbitgames.nl
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */
import { CourseTemplateReviewStepMetadata } from './courseTemplateReviewStepMetadata';


export interface CourseTemplateChapterMetadata { 
    /**
     * Unique ID for the chapter
     */
    id: string;
    /**
     * The order in which this chapter appears in the course
     */
    sortOrder: number;
    /**
     * Name of the chapter
     */
    name: string;
    reviewSteps: Array<CourseTemplateReviewStepMetadata>;
}
