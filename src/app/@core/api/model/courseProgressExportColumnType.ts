/**
 * Your Great Library
 * The API specification for both the portal and the game back-end of the great library project 
 *
 * OpenAPI spec version: 1.16
 * Contact: info@orbitgames.nl
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */


export type CourseProgressExportColumnType = 'TOTAL_MINUTES_LATE' | 'TOTAL_SKIPPED_STEPS' | 'STEP_DETAIL';

export const CourseProgressExportColumnType = {
    TOTALMINUTESLATE: 'TOTAL_MINUTES_LATE' as CourseProgressExportColumnType,
    TOTALSKIPPEDSTEPS: 'TOTAL_SKIPPED_STEPS' as CourseProgressExportColumnType,
    STEPDETAIL: 'STEP_DETAIL' as CourseProgressExportColumnType
};
