/**
 * Your Great Library
 * The API specification for both the portal and the game back-end of the great library project 
 *
 * OpenAPI spec version: 1.16
 * Contact: info@orbitgames.nl
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */


export interface CourseAmendFromTemplateRequest { 
    /**
     * A unique reference to the wanted template
     */
    templateId?: string;
    /**
     * The version of the template to use. Defaults to the latest version
     */
    templateVersion?: number;
    /**
     * Maps the section refs to the template IDs of the sections they should be amended to
     */
    sections?: { [key: string]: string; };
}
