import { Injectable } from '@angular/core';
import { EntityCache } from './entity-cache';
import { UserAgreement, GLRawPortalService } from '../api';
import { environment } from '../../../environments/environment';
import { compare } from './property-compare';
import { HttpHelperService } from './http-helper.service';
import { NbAuthService } from '@nebular/auth';

@Injectable()
export class UserAgreementService {
    private cache = new EntityCache<UserAgreement, string>(environment.cacheTimeout,
        u => u.ref,
        compare(u => u.startDate));

    constructor(private api: GLRawPortalService, private httpHelper: HttpHelperService, private auth: NbAuthService) {}

    public getLatestUserAgreement(forceRefresh: boolean = false): Promise<UserAgreement> {

        return this.httpHelper.execute(token => this.api.getLatestUserAgreement(), u => {
            this.cache.put(u);
        });
    }

    public getUserAgreement(userAgreementRef: string, forceRefresh: boolean = false) {

        if (!forceRefresh && this.cache.isItemValid(userAgreementRef)) {
            return new Promise((resolve, reject) => resolve(this.cache.get(userAgreementRef)));
        }

        return this.httpHelper.execute(token => this.api.getUserAgreement(token, userAgreementRef), r => {
            this.cache.put(r);
        });
    }
}
