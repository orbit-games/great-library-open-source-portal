import {Quiz} from '../api';

export class GlHelper {

  public static calculateQuizMaxScore(quiz: Quiz): number {
    if (!quiz || !quiz.elements) {
      return 0;
    }

    let maxScore = 0;
    for (const element of quiz.elements) {
      if (element.questionType === 'SELECT_ONE') {
        maxScore += (element.options || []).map(o => o.points).reduce((prev: number, cur: number) => Math.max(cur, prev), 0);
      } else if (element.questionType === 'SELECT_MULTIPLE') {
        maxScore += (element.options || []).map(o => o.points).reduce((prev: number, cur: number) => cur + prev, 0);
      }
    }

    return maxScore;
  }
}
