import { Injectable } from '@angular/core';
import {FileUploadMetadata, GLRawPortalService} from '../api';
import {HttpHelperService} from './http-helper.service';
import {HttpClient, HttpEvent, HttpProgressEvent, HttpResponse} from '@angular/common/http';

@Injectable({
  providedIn: 'root',
})
export class FileService {

  private postName = 'file';

  constructor(
    private api: GLRawPortalService,
    private httpHelper: HttpHelperService,
    private http: HttpClient,
  ) { }

  uploadFile(file: File, httpEventHandler: ((event: HttpEvent<FileUploadMetadata>) => void)): Promise<FileUploadMetadata> {

    return new Promise<FileUploadMetadata>((resolve, reject) => {
      this.httpHelper.executeWithEvents(
        t => {
          // Unfortunately the generated API doesn't handle files correctly...
          // this.api.uploadFile()
          let headers = this.api.defaultHeaders;
          headers = headers.set('X-Token', t);
          headers = headers.set('Accept', 'application/json');
          return this.http.post(`${this.api.configuration.basePath}/file`, this.buildFormData(file, this.postName),
            {
              headers,
              observe: 'events',
              reportProgress: true,
              withCredentials: this.api.configuration.withCredentials,
            },
          );
        },
            httpEventHandler,
      )
        .then(r => resolve(r.body))
        .catch(err => reject(err));
    });
  }

  buildFormData(file: File, postName: string): FormData {

    const formData  = new FormData();
    formData.append(postName, file, file.name);
    return formData;
  }

}
