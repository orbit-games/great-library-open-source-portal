export interface Comparator<T> {
    (a: T, b: T): number;
}

export function compare<T, CT>(f: (m: T) => CT, childComparator: Comparator<T> = null): Comparator<T> {
    return (t1: T, t2: T) => {
        var ft1 = f(t1);
        var ft2 = f(t2);
        if (ft1 == null || ft2 == null) {
            return ((ft1 == null) && (ft2 == null)) ? 0 : ((ft1 == null) ? -1 : 1);
        } else {
            return ft1 < ft2 ? -1 : (ft1 === ft2 ? (childComparator ? childComparator(t1, t2) : 0) : 1)
        }
    };
}

export function reverse<T>(comp: Comparator<T>): Comparator<T> {
    return (t1: T, t2: T) => -comp(t1, t2)
}

// export function thenComparing<T, CT>(this: Comparator<T>, t: (m: T) => CT): Comparator<T> {
//     return (t1: T, t2: T) => {
//         const compRes = this(t1, t2);
//         if (compRes === 0) {
//             return t(t1) < t(t2) ? -1 : (t(t1) === t(t2) ? 0 : 1);
//         } else {
//             return compRes;
//         }
//     }
// }

