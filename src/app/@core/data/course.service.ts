import {Injectable} from '@angular/core';

import {environment} from '../../../environments/environment.prod';
import {
  Chapter,
  Course, CourseDeadlines,
  CourseProgressExportSpecification,
  CourseProgressSummary, CourseResource,
  CourseSummary, CourseTemplateMetadata, CourseTemplates, CourseUpdateRequest, CreateCourseFromTemplateRequest,
  GLRawPortalService,
  ReviewerRelation, ReviewStep,
  Section, Skill,
} from '../api';
import {EntityCache} from './entity-cache';
import {HttpHelperService} from './http-helper.service';
import {compare} from './property-compare';
import {HttpResponse} from '@angular/common/http';
import { map, tap } from 'rxjs/operators';

export const LOGGED_IN_USER_REF = 'logged-in';

@Injectable()
export class CourseService {

  private cache = new EntityCache<Course, string>(environment.cacheTimeout,
      c => c.ref,
      compare(c => c.year, compare(c => c.quarter, compare(c => c.name))),
  );

  private courseTemplateCache = new EntityCache<CourseTemplateMetadata, string>(environment.cacheTimeout,
    c => c.id,
    compare(c => c.name),
  );

  constructor(private api: GLRawPortalService, private httpHelper: HttpHelperService) { }

  public getCourse(courseRef: string, forceRefresh: boolean = false): Promise<Course> {
      if (!forceRefresh && this.cache.isItemValid(courseRef)) {
          return Promise.resolve(this.cache.get(courseRef));
      } else {
          return this.httpHelper.execute(t => this.api.getCourse(t, courseRef, true), c => {
              this.cache.put(c);
          });
      }
  }

  public updateCourse(courseRef: string, request: CourseUpdateRequest): Promise<Course> {

    return this.httpHelper.execute(t => this.api.updateCourse(t, courseRef, request), c => {
      this.cache.put(c);
    });
  }

  public getCourseTemplates(forceRefresh: boolean = false): Promise<CourseTemplateMetadata[]> {

    if (!forceRefresh && this.courseTemplateCache.isValid()) {
      return Promise.resolve(this.courseTemplateCache.getAll());
    } else {
      return this.httpHelper.execute(t => this.api.getCourseTemplates(t).pipe(map(resp => resp.templates)),
        t => {
        this.courseTemplateCache.set(t);
      });
    }
  }

  public getCourseTemplate(templateId: string): Promise<CourseTemplateMetadata> {
    if (this.courseTemplateCache.isItemValid(templateId)) {
      return Promise.resolve(this.courseTemplateCache.get(templateId));
    } else {
      return new Promise((resolve, reject) =>
        this.getCourseTemplates()
          .then(r => resolve(r.find(t => t.id === templateId)))
          .catch(e => reject(e)));
    }
  }

  public createCourseFromTemplate(request: CreateCourseFromTemplateRequest): Promise<Course> {
    return this.httpHelper.execute(t => this.api.createCourseFromTemplate(t, request), c => this.cache.put(c));
  }

  public getCourseChapterReviewerRelations(courseRef: string, chapterRef: string): Promise<ReviewerRelation[]> {
      return this.httpHelper.execute(token => this.api.getCourseChapterReviewerRelations(token, courseRef, chapterRef));
  }

  public getCourseChapterReviewerRelation(courseRef: string, chapterRef: string, reviewerRelationRef: string): Promise<ReviewerRelation> {
      return this.httpHelper.execute(token => this.api.getCourseChapterReviewerRelation(token, courseRef, chapterRef, reviewerRelationRef));
  }

  public joinCourse(courseCode: string, userRef: string = LOGGED_IN_USER_REF): Promise<CourseSummary> {
      return this.httpHelper.execute(token => this.api.postUserCourseJoin(token, userRef, { courseCode: courseCode }));
  }

  public updateCourseUser(courseRef: string, userRef: string, courseProgressSummaryWithUpdates: CourseProgressSummary)
    : Promise<CourseProgressSummary> {
      return this.httpHelper.execute(token => this.api.updateCourseUser(token, courseRef, userRef, courseProgressSummaryWithUpdates));
  }

  public downloadCourseProgressExport(courseRef: string, specification: CourseProgressExportSpecification, userRef: string = LOGGED_IN_USER_REF)
    : Promise<HttpResponse<Blob>> {

      return this.httpHelper.executeWithResponse(token => this.api.getCourseProgressSummaryFile(token, courseRef, specification, 'response'));
  }

  public getSection(courseRef: string, sectionRef: string, forceRefresh: boolean): Promise<Section> {
      if (!forceRefresh && this.cache.isItemValid(courseRef)) {
          return Promise.resolve(this.cache.get(courseRef).sections.find(s => s.ref === sectionRef));
      } else {
          return this.httpHelper.execute(t => this.api.getSection(t, courseRef, sectionRef)
              .pipe(
                  // Update the cached course section
                  tap((s: Section) => this.clearIfSectionCacheInvalid(courseRef, s)),
              ),
          );
      }
  }

  public updateSection(courseRef: string, sectionRef: string, sectionWithUpdates: Section): Promise<Section> {
    return this.httpHelper.execute(t => this.api.updateSection(t, courseRef, sectionRef, sectionWithUpdates)
      .pipe(
        // Update the cached course section
        tap((s: Section) => this.cache.remove(courseRef)),
      ),
    );
  }


  public createSection(courseRef: string, section: Section): Promise<Section> {

    return this.httpHelper.execute(t => this.api.createSection(t, courseRef, section)
      .pipe(
        tap((s: Section) => this.cache.remove(courseRef)),
      ),
    );
  }

  public deleteSection(courseRef: string, sectionRef: string): Promise<Section> {

    return this.httpHelper.execute(t => this.api.deleteSection(t, courseRef, sectionRef)
      .pipe(
        tap(s => this.cache.remove(courseRef)),
      ));
  }

  private clearIfSectionCacheInvalid(courseRef: string, section: Section) {

    this.clearIfItemCacheInvalid(courseRef, cachedCourse => cachedCourse.sections.find(s => s.ref === section.ref), section);
  }

  public getCourseResource(courseRef: string, resourceRef: string, forceRefresh: boolean = false) {

    if (!forceRefresh && this.cache.isItemValid(courseRef)) {
      return Promise.resolve(this.cache.get(courseRef).resources.find(r => r.ref === resourceRef));
    } else {
      return this.httpHelper.execute(t => this.api.getCourseResource(t, courseRef, resourceRef)
        .pipe(
          tap((r: CourseResource) => this.clearIfResourceCacheInvalid(courseRef, r)),
        ),
      );
    }
  }

  public updateCourseResource(courseRef: string, resourceRef: string, resourceWithUpdates: CourseResource) {

    return this.httpHelper.execute(t => this.api.updateCourseResource(t, courseRef, resourceRef, resourceWithUpdates)
      .pipe(
        // Update the cached course section
        tap((r: CourseResource) => this.cache.remove(courseRef)),
      ),
    );
  }

  public createCourseResource(courseRef: string, newResource: CourseResource) {

    return this.httpHelper.execute(t => this.api.createCourseResource(t, courseRef, newResource)
      .pipe(
        tap((r: CourseResource) => this.cache.remove(courseRef)),
      ));
  }

  private clearIfResourceCacheInvalid(courseRef: string, resource: CourseResource) {

    this.clearIfItemCacheInvalid(courseRef, cachedCourse => cachedCourse.resources.find(r => r.ref === resource.ref), resource);
  }

  public getSkill(courseRef: string, skillRef: string, forceRefresh: boolean = false): Promise<Skill> {

    if (!forceRefresh && this.cache.isItemValid(courseRef)) {
      return Promise.resolve(this.cache.get(courseRef).skills.find(r => r.ref === skillRef));
    } else {
      return this.httpHelper.execute(t => this.api.getSkill(t, courseRef, skillRef)
        .pipe(
          tap((s: Skill) => this.clearIfSkillCacheInvalid(courseRef, s)),
        ),
      );
    }
  }

  public updateSkill(courseRef: string, skillRef: string, skillWithUpdates: Skill) {

    return this.httpHelper.execute(t => this.api.updateSkill(t, courseRef, skillRef, skillWithUpdates)
      .pipe(
        // Update the cached course section
        tap((s: Skill) => this.cache.remove(courseRef)),
      ),
    );
  }

  public createSkill(courseRef: string, newSkill: Skill) {

    return this.httpHelper.execute(t => this.api.createSkill(t, courseRef, newSkill)
      .pipe(
        tap((s: Skill) => this.cache.remove(courseRef)),
      ));
  }

  private clearIfSkillCacheInvalid(courseRef: string, skill: Skill) {
    this.clearIfItemCacheInvalid(courseRef, c => c.skills.find(s => s.ref === skill.ref), skill);
  }

  public getReviewStep(courseRef: any, chapterRef: string, reviewStepRef: string, forceRefresh: boolean = false): Promise<ReviewStep> {

    if (!forceRefresh && this.cache.isItemValid(courseRef)) {
      return Promise.resolve(this.getReviewStepInCourse(this.cache.get(courseRef), chapterRef, reviewStepRef));
    } else {
      return this.httpHelper.execute(t => this.api.getReviewStep(t, courseRef, chapterRef, reviewStepRef)
        .pipe(
          tap((r: ReviewStep) => this.clearIfReviewStepCacheInvalid(courseRef, chapterRef, r)),
        ),
      );
    }
  }

  public updateReviewStep(courseRef: any, chapterRef: string, reviewStepRef: string, reviewStepWithUpdates: ReviewStep): Promise<ReviewStep> {

    return this.httpHelper.execute(t => this.api.updateReviewStep(t, courseRef, chapterRef, reviewStepRef, reviewStepWithUpdates).pipe(
      // Update the cached course section
      tap((s: ReviewStep) => this.cache.remove(courseRef)),
    ));
  }

  private clearIfReviewStepCacheInvalid(courseRef: string, chapterRef: string, reviewStep: ReviewStep): void {
    this.clearIfItemCacheInvalid(courseRef, c => this.getReviewStepInCourse(c, chapterRef, reviewStep.ref), reviewStep);
  }

  private getReviewStepInCourse(course: Course, chapterRef: string, reviewStepRef: string) {

    if (!course) return null;
    const chapter = course.chapters.find(c => c.ref === chapterRef);
    if (!chapter) return null;
    return chapter.reviewSteps.find(r => r.ref === reviewStepRef);
  }

  public updateDeadlines(courseRef: string, courseDeadlines: CourseDeadlines): Promise<CourseDeadlines> {

    return this.httpHelper.execute(t => this.api.updateDeadlines(t, courseRef, courseDeadlines)
      .pipe(tap(d => this.cache.remove(courseRef))));
  }

  getChapter(courseRef: string, chapterRef: string, forceRefresh: boolean = false) {

    if (!forceRefresh && this.cache.isItemValid(courseRef)) {
      return Promise.resolve(this.cache.get(courseRef).chapters.find(r => r.ref === chapterRef));
    } else {
      return this.httpHelper.execute(t => this.api.getCourseChapter(t, courseRef, chapterRef, true)
        .pipe(
          tap((c: Chapter) => this.clearIfChapterInvalid(courseRef, c)),
        ),
      );
    }
  }

  updateChapter(courseRef: string, chapterRef: string, chapterWithUpdates: Chapter) {

    return this.httpHelper.execute(t => this.api.updateCourseChapter(t, courseRef, chapterRef, chapterWithUpdates).pipe(
      tap(c => this.cache.remove(courseRef)),
    ));
  }

  private clearIfChapterInvalid(courseRef: string, chapter: Chapter) {
    this.clearIfItemCacheInvalid(courseRef, (course) => this.getChapterInCourse(course, chapter.ref), chapter);
  }

  private getChapterInCourse(course: Course, chapterRef: string) {

    return course.chapters.find(c => c.ref === chapterRef);
  }

  private clearIfItemCacheInvalid<T>(courseRef: string, retrieveItemFromCachedCourse: ((course: Course) => T), newItem: T) {

    const cachedCourse = this.cache.get(courseRef);
    if (cachedCourse) {
      const cachedItem = retrieveItemFromCachedCourse(cachedCourse);

      // We explicitly want to check using double-equals here, not triple, because we won't have the same object
      // tslint:disable-next-line:triple-equals
      if (cachedItem != newItem) {
        this.cache.remove(courseRef);
      }
    }
  }

}
