import { NgModule, ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UserService } from './user.service';
import { HttpHelperService } from './http-helper.service';
import { CourseService } from './course.service';
import { UserAgreementService } from './user-agreement.service';
import { ConversationService } from './conversation.service';
import { ServerService } from './server.service';
import {FileService} from './file.service';

const SERVICES = [
  HttpHelperService,
  UserService,
  CourseService,
  UserAgreementService,
  ConversationService,
  ServerService,
  FileService,
];

@NgModule({
  imports: [
    CommonModule,
  ],
  providers: [
    ...SERVICES,
  ],
})
export class DataModule {
  static forRoot(): ModuleWithProviders {
    return <ModuleWithProviders>{
      ngModule: DataModule,
      providers: [
        ...SERVICES,
      ],
    };
  }
}
