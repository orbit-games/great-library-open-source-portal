import { Injectable } from "@angular/core";
import { NbAuthService } from "@nebular/auth";
import { HttpHelperService } from "./http-helper.service";
import {GLRawPortalService, ServerInfo} from "../api";

@Injectable()
export class ServerService {

    constructor(private api: GLRawPortalService, private httpHelper: HttpHelperService, private auth: NbAuthService) {}

    /**
     * Clear the server-side cache
     */
    public clearCache(): Promise<any> {
        return this.httpHelper.execute((token) => this.api.deleteServerCache(token));
    }

    public getServerInfo(): Promise<ServerInfo> {
      return this.httpHelper.execute((token) => this.api.getServerInfo());
    }
}
