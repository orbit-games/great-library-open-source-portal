import { Injectable } from "@angular/core";
import { GLRawPortalService, Conversations, Conversation, ConversationMessage } from "../api";
import { HttpHelperService } from "./http-helper.service";
import { Subject } from "rxjs";
import { EntityCache } from "./entity-cache";
import { compare } from "./property-compare";

@Injectable()
export class ConversationService {

    // Conversation cache is only valid for 10 seconds, and is mainly used to share conversations between different components without reloading them
    private cache = new EntityCache<Conversation, string>(10, 
        c => c.ref,
        compare(c => c.ref)
    );
    conversationsUpdated = new Subject<Conversation[]>();

    constructor(private api: GLRawPortalService, private httpHelper: HttpHelperService) {}

    getConversations(courseRef: string, updated: Date = null, messageLimit: number = null, conversations: string[] = [], forceUpdate = false): Promise<Conversations> {
        if (!forceUpdate && this.cache.isValid()) {
            return new Promise((resolve, reject) => {
                resolve({
                    conversations: this.cache.getAll()
                });
            });
        } else {
            return this.httpHelper.execute(t => this.api.getConversations(t, courseRef, updated, messageLimit, conversations.join(',')),
            r => {
                this.cache.set(r.conversations)
                this.conversationsUpdated.next(this.cache.getAll());
            }
            );
        }
    }

    getConversation(courseRef: string, conversationRef: string, updated?: Date, messageLimit?: number, messageSkip?: number, forceUpdate = false): Promise<Conversation> {
        if (!forceUpdate && this.cache.isItemValid(conversationRef)) {
            return new Promise((resolve, reject) => {
                resolve(this.cache.get(conversationRef));
            });
        } else {
            return this.httpHelper.execute(t => this.api.getConversation(t, courseRef, conversationRef, updated, messageLimit, messageSkip), 
            r => {
                if (messageSkip == null) {
                    this.cache.put(r)
                    this.conversationsUpdated.next(this.cache.getAll());
                }
            });
        }
    }

    createConversation(courseRef: string, conversation: Conversation): Promise<Conversation> {
        return this.httpHelper.execute(t => this.api.createConversation(t, courseRef, conversation));
    }

    postConversationMessage(courseRef: string, conversationRef: string, message: string): Promise<ConversationMessage> {
        return this.httpHelper.execute(t => this.api.postMessage(t, courseRef, conversationRef, {
            message: message
        }));
    }

    postConversationRead(courseRef: string, conversationRef: string, readUntil: number): Promise<Conversation> {
        return this.httpHelper.execute(t => this.api.postConversationRead(t, courseRef, conversationRef, { readUntil: readUntil }));
    }

}