import naturalSort from 'natural-sort';

/**
 * Cache to be used for a collection of entities.
 * This cache assumes that (when it is valid) all known entities are in it,
 * i.e. that getting this whole cache is the same as reading every row of the original entity table
 */
export class EntityCache<T, K> {

    private cache: T[] = [];
    private cacheValidity: Map<K, Date> = new Map<K, Date>();
    private cacheUpdated = new Date(0);
    readonly maxCacheAge;

    private getPrimaryKey: (item: T) => K;
    private compare: (a: T, b: T) => number;

    /**
     *
     * @param maxCacheAge Maximum cache age in seconds
     * @param getPrimaryKey A function to get the primary key from an entity
     * @param compare (Optional) comparison function to sort items in the cache.
     */
    constructor(maxCacheAge: number,
        getPrimaryKey: (item: T) => K,
        compare: (a: T, b: T) => number = naturalSort()) {

        this.maxCacheAge = maxCacheAge;
        this.getPrimaryKey = getPrimaryKey;
        this.compare = compare;
    }

    /**
     * Removes the item with the given key from the cache
     * @param key The primary key of the item to remove
     */
    public remove(key: K): T {
        this.cacheValidity.delete(key);
        const cacheIndex = this.cache.findIndex(i => this.getPrimaryKey(i) === key);
        if (cacheIndex >= 0) {
            const removedItems = this.cache.splice(cacheIndex, 1);
            return removedItems[0];
        } else {
            return null;
        }
    }

    /**
     * Adds the given item to the cache (or updates it if an item with the same primary key already exists)
     * @param item The item to add
     */
    public put(item: T): void {
        const key = this.getPrimaryKey(item);
        const cacheIndex = this.cache.findIndex(i => this.getPrimaryKey(i) === key);
        if (cacheIndex >= 0) {
            this.cache[cacheIndex] = item;
            this.cacheValidity.set(this.getPrimaryKey(item), new Date());
        } else {
            this.cache.push(item);
        }
        this.cacheValidity.set(key, new Date());
        this.sort();
    }

    /**
     * Sorts the cache
     */
    public sort(): void {
        this.cache.sort(this.compare);
    }

    public invalidateIfNeeded(): void {
        if (!this.isValid()) {
            this.cache = null;
        }
    }

    public get(key: K): T {
        return this.cache.find(i => this.getPrimaryKey(i) === key);
    }

    public getAll(): T[] {
        if (!this.isValid()) {
            console.warn('Trying to retrieve an invalid cache!');
        }
        return this.cache.slice();
    }

    /**
     * Set the entire cache (removing what was previously there)
     * @param allItems The list of all items to set
     */
    public set(allItems: T[]) {
        if (!allItems) {
            allItems = [];
        }
        const now = new Date();
        this.cache = allItems.slice();
        this.cacheUpdated = now;
        this.cache.forEach(i => this.cacheValidity.set(this.getPrimaryKey(i), now));
        this.sort();
    }

    public isValid(): boolean {
        return this.isValidAge(this.cacheUpdated) && this.cache !== undefined;
    }

    private isValidAge(cacheUpdated: Date) {

        if (!cacheUpdated) {
            return false;
        }

        let dif = Date.now() - cacheUpdated.getTime();
        dif = dif / 1000;
        return dif <= this.maxCacheAge;
    }

    public isItemValid(key: K) {
        // this.cacheValidity.forEach((v, k) => console.log('Cache validity ' + k + ': ' + v ));
        // console.log(typeof (key) + ' ' + key + ': ' + this.cacheValidity.get(key));
        return this.isValidAge(this.cacheValidity.get(key));
    }
}
