import {CourseResource, Section} from '../api';

export class GlComparer {

  public static compareSections(s1: Section, s2: Section): number {
    if (s1.parentRef == null && s2.parentRef == null) {
      // If it is a root section, we sort them based on sort order
      return GlComparer.compare(s1.sortOrder, s2.sortOrder);
    } else if (s1.parentRef == null) {
      return -1;
    } else if (s2.parentRef == null) {
      return 1;
    } else {
      // Apparently both parent refs are "not null", so this is a child
      if (s1.parentRef === s2.parentRef) {
        return GlComparer.compare(s1.sortOrder, s2.sortOrder);
      } else {
        return GlComparer.compare(s1.parentRef, s2.parentRef);
      }
    }
  }

  static compareResources(r1: CourseResource, r2: CourseResource) {
    if (r1 == null && r2 == null) {
      return 0;
    } else if (r1 == null) {
      return -1;
    } else if (r2 == null) {
      return 1;
    } else {
      return this.compare(r1.name, r2.name);
    }
  }

  public static compare(o1, o2): number {
    if (o1 > o2) {
      return 1;
    } else if (o1 < o2) {
      return -1;
    } else {
      return 0;
    }
  }

}
