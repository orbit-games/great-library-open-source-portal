import {Injectable} from '@angular/core';
import {GLRawPortalService, Property, Role, User, UserCollection} from '../api';
import {EntityCache} from './entity-cache';
import {environment} from '../../../environments/environment.prod';
import {NbAuthService} from '@nebular/auth';
import {HttpHelperService} from './http-helper.service';
import {compare} from './property-compare';

export const LOGGED_IN_USER_REF = 'logged-in';

@Injectable()
export class UserService {

  private cache = new EntityCache<User, string>(environment.cacheTimeout,
    u => u.ref,
    compare(u => u.username));

  constructor(private api: GLRawPortalService, private httpHelper: HttpHelperService, private auth: NbAuthService) {
  }

  public getLoggedInUser(forceRefresh: boolean = false): Promise<User> {

    return this.httpHelper.execute(token => this.api.getUser(token, LOGGED_IN_USER_REF), r => {
      this.cache.put(r);
    });
  }

  public getUser(userRef: string, forceRefresh: boolean = false) {

    if (!forceRefresh && this.cache.isItemValid(userRef)) {
      return new Promise((resolve, reject) => resolve(this.cache.get(userRef)));
    }

    return this.httpHelper.execute(token => this.api.getUser(token, userRef), r => {
      this.cache.put(r);
    });
  }

  public getUsers(courseRef: string, roleFilter: Role[]): Promise<UserCollection> {

    return this.httpHelper.execute(token => this.api.getUsers(token, courseRef, roleFilter.join(',')), u => {
      for (const user of u.users) {
        this.cache.put(user);
      }
    });

  }

  public setUserProperty(userRef: string, propertyKey: string, property: Property): Promise<Property> {

    return this.httpHelper.execute(t => this.api.updateUserProperty(t, userRef, propertyKey, property), r => {
      // We need to update the cached user
      const cachedUser = this.cache.get(userRef);
      if (cachedUser) {
        if (!cachedUser.properties) {
          cachedUser.properties = {};
        }
        cachedUser.properties[propertyKey] = property.value;
      }
    });
  }
}
