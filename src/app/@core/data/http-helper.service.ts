import {HttpErrorResponse, HttpEvent, HttpResponse} from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { NbAuthService } from '@nebular/auth';
import { Observable } from 'rxjs';
import { flatMap, map } from 'rxjs/operators';

import * as models from '../api/model/models';

@Injectable()
export class HttpHelperService {

    constructor(private auth: NbAuthService, private router: Router) {}

    public execute<T>(
        requestFactory: (token: string) => Observable<T>,
        responseProcessor: ((obj: T) => void) = t => {},
        logoutOn401 = true)
        : Promise<T> {
        return new Promise<T>((resolve, reject) => {
            this.buildRequest(requestFactory).subscribe(
                (res: T) => {
                    responseProcessor(res);
                    resolve(res);
                },
                (err: HttpErrorResponse) => this.handleError(err, reject, logoutOn401),
            );
        });
    }

    public executeWithResponse<T>(
        requestFactory: (token: string) => Observable<HttpResponse<T>>,
        responseProcessor: ((obj: HttpResponse<T>) => void) = t => {},
        logoutOn401 = true)
        : Promise<HttpResponse<T>> {
        return new Promise<HttpResponse<T>>((resolve, reject) => {
          this.buildRequest(requestFactory).subscribe(
                (res: HttpResponse<T>) => {
                    responseProcessor(res);
                    resolve(res);
                },
                (err: HttpErrorResponse) => this.handleError(err, reject, logoutOn401),
            );
        });
    }

  public executeWithEvents<T>(
      requestFactory: (token: string) => Observable<HttpEvent<T>>,
      eventHandler: ((obj: HttpEvent<T>) => void) = t => {},
      logoutOn401 = true)
      : Promise<HttpResponse<T>> {

    return new Promise<HttpResponse<T>>((resolve, reject) => {
      this.buildRequest(requestFactory)
        .subscribe(e => {
            eventHandler(e);
            if (e instanceof HttpResponse) {
              resolve(e);
            }
            if (e instanceof HttpErrorResponse) {
              this.handleError(e, reject, logoutOn401);
            }
          },
          (err: HttpErrorResponse) => this.handleError(err, reject, logoutOn401),
        );
      });
    }

  private buildRequest<T>(requestFactory: (token: string) => Observable<T>) {
    return this.auth.getToken().pipe(
      map(t => t.getValue()),
      flatMap(requestFactory),
    );
  }

    /**
     * Handles error responses, extracting the error details
     * @param err: The HttpErrorResponse returned from HttpClient
     * @param reject: Reject function to call
     * @param logoutOn401 Indicates if a logout should be performed in case a status code 401 was returned
     */
    private handleError(err: HttpErrorResponse, reject: (reason?: any) => void, logoutOn401: boolean) {

      // HttpErrors are quite interesting so we log them to the console
      // tslint:disable-next-line:no-console
      console.log(err);

      let errorDetails: models.ErrorDetails;
      if (err.error instanceof Error) {
        errorDetails = {
            code: 'error.unknown',
            message: err.error.message,
        };
      } else {
          let errResp: models.ErrorResponse;
          if ((typeof err.error) === 'string') {
              errResp = <models.ErrorResponse>JSON.parse(err.error);
          } else {
              errResp = <models.ErrorResponse>err.error;
          }
          errorDetails = errResp.error;
      }
      if (logoutOn401 && err.status === 401) {
          this.router.navigate(['/auth/logout']);
      }
      reject(errorDetails);
    }
}
