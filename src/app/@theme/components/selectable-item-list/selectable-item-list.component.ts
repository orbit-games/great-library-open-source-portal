import {
  AfterViewInit,
  Component,
  ElementRef,
  EventEmitter,
  Input,
  OnChanges,
  OnInit,
  Output,
  QueryList, SimpleChanges,
  ViewChildren,
} from '@angular/core';
import { CdkDragDrop, moveItemInArray } from '@angular/cdk/drag-drop';

export interface SelectableItem<T> {
  label: string;
  ref: string;
  originalItem: T;
}

@Component({
  selector: 'ngx-selectable-item-list',
  templateUrl: './selectable-item-list.component.html',
  styleUrls: ['./selectable-item-list.component.scss'],
})
export class SelectableItemListComponent<T> implements OnInit, OnChanges, AfterViewInit {

  static readonly SIZE_SMALL = 'small';
  static readonly SIZE_MEDIUM = 'medium';
  static readonly SIZE_LARGE = 'large';

  @Input() items: SelectableItem<T>[];
  @Input() selectedItemRef: string;

  @Input() size: string = 'large';
  @Input() dragEnabled: boolean = false;

  @Output() itemClicked = new EventEmitter<SelectableItem<T>>();

  @ViewChildren('listItem') listItemViews: QueryList<ElementRef>;

  disabled = false;
  filter: string = '';
  filterPredicate: ((item: SelectableItem<T>, filter: string) => boolean) = (item, filter) => item.label.toLowerCase().includes(filter);

  filteredItems: SelectableItem<T>[];

  get small() { return this.size === SelectableItemListComponent.SIZE_SMALL; }
  get medium() { return this.size === SelectableItemListComponent.SIZE_MEDIUM; }
  get large() { return this.size === SelectableItemListComponent.SIZE_LARGE; }

  constructor() { }

  ngOnInit() {
  }

  ngOnChanges(changes: SimpleChanges): void {
    this.updateFilteredItems();

    if (changes['selectedItemRef'] != null) {
      this.tryScrollToSelectedItem();
    }
  }

  ngAfterViewInit() {
    // this.tryScrollToSelectedItem();
  }

  setFilterPredicate(predicate: ((item: SelectableItem<T>, filter: string) => boolean)) {
    this.filterPredicate = predicate;
  }

  tryScrollToSelectedItem() {

    if (this.listItemViews == null || this.selectedItemRef == null) return;

    const selectedItemIndex = this.filteredItems.findIndex(s => s.ref === this.selectedItemRef);
    if (selectedItemIndex > 0 && selectedItemIndex < this.listItemViews.length) {
      this.listItemViews.toArray()[selectedItemIndex].nativeElement.scrollIntoView({block: 'center'});
    }
  }


  private updateFilteredItems() {
    if (this.filter == null) {
      this.filteredItems = this.items;
    } else {
      const lowerCaseFilter = this.filter.toLowerCase();
      this.filteredItems = this.items.filter(r => this.filterPredicate(r, lowerCaseFilter));
    }
  }

  onItemClicked(item: SelectableItem<T>) {
    this.itemClicked.emit(item);
  }


  drop(event: CdkDragDrop<T, any>) {
    moveItemInArray(this.items, event.previousIndex, event.currentIndex);
    this.updateFilteredItems();
  }
}
