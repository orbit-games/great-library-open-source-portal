import { Component, forwardRef, OnInit } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import { NgbTimeStruct } from '@ng-bootstrap/ng-bootstrap';
import { DisplayDatePipe } from '../../pipes';
import { FormatHelper } from '../../pipes/format-helper';
import { environment } from '../../../../environments/environment.prod';
import * as moment from 'moment';
import { Moment } from 'moment';

@Component({
  selector: 'ngx-date-time-picker',
  templateUrl: './date-time-picker.component.html',
  styleUrls: ['./date-time-picker.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => DateTimePickerComponent),
      multi: true,
    },
  ],
})
export class DateTimePickerComponent implements OnInit, ControlValueAccessor {

  private onChange: (_: any) => void;

  dateComponent: moment.Moment;
  timeComponent: NgbTimeStruct;

  dateFormat = environment.displayDateFormat;

  disabled: boolean = false;
  onTouched: any;

  constructor() { }

  ngOnInit() {
  }

  registerOnChange(fn: (_: any) => void): void {
    this.onChange = fn;
  }

  registerOnTouched(fn: any): void {
    this.onTouched = fn;
  }

  setDisabledState(isDisabled: boolean): void {
    this.disabled = isDisabled;
  }

  writeValue(obj: any): void {

    let date;
    if (!(obj instanceof Date)) {
      date = new Date(obj);
    } else {
      date = obj;
    }

    this.dateComponent = moment(date);
    this.timeComponent = {
      hour: date.getHours(),
      minute: date.getMinutes(),
      second: date.getSeconds(),
    };
  }

  onTimeChanged() {
    this.onChange(this.value);
  }

  onDateChanged(date: Moment) {
    this.dateComponent = date;
    this.onChange(this.value);
  }

  get value() {
    return new Date(
      this.dateComponent.year(),
      this.dateComponent.month(),
      this.dateComponent.date(),
      this.timeComponent.hour,
      this.timeComponent.minute,
      this.timeComponent.second,
    );
  }

  onDateTextChanged(value: string) {

    this.dateComponent = moment(value, this.dateFormat);
    this.onChange(this.value);
  }
}
