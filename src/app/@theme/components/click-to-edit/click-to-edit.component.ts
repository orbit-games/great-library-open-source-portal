import {Component, forwardRef, Input, OnInit} from '@angular/core';
import {ControlValueAccessor, NG_VALUE_ACCESSOR} from '@angular/forms';

@Component({
  selector: 'ngx-click-to-edit',
  templateUrl: './click-to-edit.component.html',
  styleUrls: ['./click-to-edit.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => ClickToEditComponent),
      multi: true,
    },
  ],
})
export class ClickToEditComponent implements OnInit, ControlValueAccessor {

  @Input('placeholder') placeholder: string = 'Enter value...';
  @Input('editing') editing: boolean = false;
  @Input() status: string;

  disabled: boolean = false;

  val: string;

  onChange: any = () => {};
  onTouch: any = () => {};

  constructor() { }

  ngOnInit() {
  }

  set value(val) {  // this value is updated by programmatic changes if( val !== undefined && this.val !== val){
    this.val = val;
    this.onChange(val);
    this.onTouch();
  }

  get value() {
    return this.val;
  }

  registerOnChange(fn: any): void {
    this.onChange = fn;
  }

  registerOnTouched(fn: any): void {
    this.onTouch = fn;
  }

  setDisabledState(isDisabled: boolean): void {
    if (isDisabled) {
      this.editing = false;
    }
    this.disabled = isDisabled;
  }

  writeValue(obj: any): void {
    this.value = obj;
  }

  edit() {
    if (!this.disabled) {
      this.editing = !this.editing;
    } else {
      this.editing = false;
    }
    this.onTouch();
  }
}
