import {Component, Input} from '@angular/core';
import {NbAdjustableConnectedPositionStrategy, NbAdjustment, NbPosition, NbSelectComponent} from '@nebular/theme';

@Component({
  selector: 'ngx-gl-select',
  template: '',
})
export class GlSelectComponent<T> extends NbSelectComponent<T> {

  @Input() position: NbPosition = NbPosition.BOTTOM;
  @Input() forcePosition: boolean = false;

  protected createPositionStrategy(): NbAdjustableConnectedPositionStrategy {
    return this.positionBuilder
      .connectedTo(this.hostRef)
      .position(this.position)
      .offset(0)
      .adjustment(NbAdjustment.NOOP);
  }
}
