import {Component, Input, OnInit} from '@angular/core';
import { NbDialogRef } from '@nebular/theme';

@Component({
  selector: 'ngx-confirm-delete-dialog',
  templateUrl: './confirm-delete-dialog.component.html',
  styleUrls: ['./confirm-delete-dialog.component.scss'],
})
export class ConfirmDeleteDialogComponent implements OnInit {

  /**
   * Reference to the item that should be deleted on confirm
   */
  @Input() itemRef: any = true;

  @Input() title: string = 'Please confirm deleting this item';
  @Input() content: string = 'Are you sure you want to delete this?';
  @Input() confirmButtonText: string = 'Yes';
  @Input() cancelButtonText: string = 'Cancel';

  constructor(protected dialogRef: NbDialogRef<ConfirmDeleteDialogComponent>) {
  }

  ngOnInit() {
  }

  cancel() {
    this.dialogRef.close();
  }

  confirmDelete() {
    this.dialogRef.close(this.itemRef);
  }
}
