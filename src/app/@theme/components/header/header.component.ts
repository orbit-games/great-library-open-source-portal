import { Component, Input, OnInit, OnDestroy, ViewChildren } from '@angular/core';

import { NbMenuService, NbSidebarService, NbTooltipDirective } from '@nebular/theme';
import { UserService } from '../../../@core/data/user.service';
import { AnalyticsService } from '../../../@core/utils/analytics.service';
import { filter, map } from 'rxjs/operators';
import { Subscription } from 'rxjs';
import { Router } from '@angular/router';
import { User, Role } from '../../../@core/api';
import { SessionService, PageOptions, NameType } from '../../../@core/session.service';

@Component({
  selector: 'gl-header',
  styleUrls: ['./header.component.scss'],
  templateUrl: './header.component.html',
})
export class HeaderComponent implements OnInit, OnDestroy {

  user: User;
  userMayToggleNameType: boolean = false;
  private loggedInUserChangedSubscription: Subscription;

  currentPage: PageOptions;
  private pageChangeSubscription: Subscription;

  static readonly PROFILE_ITEM_TITLE = 'Profile';
  static readonly LOGOUT_ITEM_TITLE = 'Log out';
  userMenu = [{ title: HeaderComponent.PROFILE_ITEM_TITLE }, { title: HeaderComponent.LOGOUT_ITEM_TITLE }];

  nameTypeChangedSubscription: Subscription;
  nameType: NameType;
  nameTypeString: string;
  nameTypeAvatarName = NameType.DISPLAY_NAME;
  nameTypeRealName = NameType.FULL_NAME;

  @ViewChildren(NbTooltipDirective) allTooltips: [NbTooltipDirective];

  constructor(private sidebarService: NbSidebarService,
              private menuService: NbMenuService,
              private sessionService: SessionService,
              private analyticsService: AnalyticsService,
              private router: Router) {
  }

  ngOnInit() {

    this.sessionService.getLoggedInUser().subscribe(u => this.setUser(u));
    this.loggedInUserChangedSubscription = this.sessionService.loggedInUserChanged.subscribe(u => this.setUser(u));

    this.currentPage = this.sessionService.getCurrentPage();
    this.pageChangeSubscription = this.sessionService.pageChanged.subscribe(p => {
      this.currentPage = p;
      this.allTooltips.forEach(t => t.hide());
    });

    this.menuService.onItemClick()
      .pipe(
        filter(({ tag }) => tag === 'user-menu'),
        map(({ item: { title }}) => title ),
      )
      .subscribe(title => {
        if (title === HeaderComponent.PROFILE_ITEM_TITLE) {
          this.router.navigate(['/pages', 'users', this.user.ref]);
        }
        if (title === HeaderComponent.LOGOUT_ITEM_TITLE) {
          this.sessionService.logout();
        }
      });

      this.nameType = this.sessionService.nameType;
      this.nameTypeChangedSubscription = this.sessionService.nameTypeChanged.subscribe(n => this.nameType = n);
  }

  getNameTypeLabel(nameType: NameType): string {
    return nameType === NameType.DISPLAY_NAME ? 'Avatar names' : 'Real names';
  }

  private setUser(user: User) {
    this.user = user;
    this.userMayToggleNameType = (user && user.role !== Role.LEARNER);
  }

  ngOnDestroy() {
    this.loggedInUserChangedSubscription.unsubscribe();
    this.pageChangeSubscription.unsubscribe();
    this.nameTypeChangedSubscription.unsubscribe();
  }

  toggleSidebar(): boolean {
    this.sidebarService.toggle(false, 'menu-sidebar');
    return false;
  }

  toggleSettings(): boolean {
    this.sidebarService.toggle(false, 'settings-sidebar');
    return false;
  }

  goToHome() {
    this.menuService.navigateHome();
  }

  startSearch() {
    this.analyticsService.trackEvent('startSearch');
  }

  setNameType(nameType: NameType) {
    this.sessionService.setNameType(nameType);
  }

  toggleNameType() {
    this.sessionService.toggleNameType();
  }

  openManual() {
    window.open('/assets/manual.pdf', '_blank');
  }
}
