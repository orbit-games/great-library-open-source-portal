import { DeltaStatic } from 'quill';

const Delta = require('quill-delta');

export class TextMeshProRichTextTagMapping {

  /**
   * matcher in which the first group matches an opening tag, and the second group a closing tag
   */
  readonly matcher: RegExp;
  readonly openingTag: string;
  readonly closingTag: string;

  constructor(
    public readonly attribute: string,
    public readonly tag: string) {

    // Generate a regex like '(<\s*[bB]\s*>)|(<\s*/\s*[bB]\s*>)'
    this.matcher = new RegExp('(<\s*[' + tag.toLowerCase() + tag.toUpperCase() + ']\s*>)' +
      '|(<\s*/\s*[' + tag.toLowerCase() + tag.toUpperCase() + ']\s*>)');
    this.openingTag = '<' + tag + '>';
    this.closingTag = '</' + tag + '>';
  }

}

export class TextMeshProRichTextToQuillDeltaConverter {

    tagMappings: TextMeshProRichTextTagMapping[] = [
        new TextMeshProRichTextTagMapping('bold', 'b'),
        new TextMeshProRichTextTagMapping('italic', 'i'),
        new TextMeshProRichTextTagMapping('strike', 's'),
        new TextMeshProRichTextTagMapping('underline', 'u'),
    ];

    convertToTextMeshProRichText(value: DeltaStatic): string {

        let res = '';
        for (const op of value.ops) {

            if (op.attributes) {
                this.tagMappings.filter(t => op.attributes[t.attribute])
                    .forEach(t => res += t.openingTag);
            }

            res += op.insert;

            if (op.attributes) {
                this.tagMappings.filter(t => op.attributes[t.attribute])
                    .reverse()
                    .forEach(t => res += t.closingTag);
            }
        }

        return res;
    }

    convertToQuillDelta(value: string): DeltaStatic {
        // Converts to Quill delta contents; see: https://quilljs.com/docs/delta/
      if (value == null) {
        return new Delta();
      }

        let valueRemaining = value;
        const currentAttributes: string[] = [];

        const content: DeltaStatic = new Delta();

        while (valueRemaining !== '') {

            let closestMatchingTagMapping: TextMeshProRichTextTagMapping = null;
            let closestMatchingTagMappingMatch: RegExpMatchArray = null;

            for (const tagMapping of this.tagMappings) {

                const tagMatch = valueRemaining.match(tagMapping.matcher);
                if (tagMatch && (!closestMatchingTagMappingMatch || closestMatchingTagMappingMatch.index > tagMatch.index)) {
                    closestMatchingTagMapping = tagMapping;
                    closestMatchingTagMappingMatch = tagMatch;
                }
            }

            if (closestMatchingTagMappingMatch) {

                // Add the content until the match
                content.insert(valueRemaining.substring(0, closestMatchingTagMappingMatch.index), this.getAttributeMap(currentAttributes));
                // Add the attribute and update "valueRemaining"
                valueRemaining = valueRemaining.substring(closestMatchingTagMappingMatch.index + closestMatchingTagMappingMatch[0].length);

                if (closestMatchingTagMappingMatch[1]) {
                    // Opening tag matched
                    currentAttributes.push(closestMatchingTagMapping.attribute);

                } else if (closestMatchingTagMappingMatch[2]) {
                    // Closing tag matched
                    if (currentAttributes.includes(closestMatchingTagMapping.attribute)) {
                        currentAttributes.splice(currentAttributes.indexOf(closestMatchingTagMapping.attribute), 1);
                    }
                }
            } else {

                content.insert(valueRemaining);
                valueRemaining = '';
            }
        }
        return content;
    }

    private getAttributeMap(currentAttributes: string[]): any {

        const res = {};
        for (const attribute of currentAttributes) {
            res[attribute] = true;
        }
        return res;
    }
}


