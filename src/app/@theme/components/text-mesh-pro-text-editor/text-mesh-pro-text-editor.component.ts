import {Component, OnInit, ViewChild, forwardRef, ViewEncapsulation, Input, OnChanges, SimpleChanges, AfterViewInit} from '@angular/core';
import { QuillEditorComponent, QuillModules } from 'ngx-quill';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import { TextMeshProRichTextToQuillDeltaConverter } from './text-mesh-pro-rich-text-to-quill-delta-converter';
import {DeltaStatic} from 'quill';

@Component({
  selector: 'ngx-text-mesh-pro-text-editor',
  templateUrl: './text-mesh-pro-text-editor.component.html',
  styleUrls: ['./text-mesh-pro-text-editor.component.scss'],
  encapsulation: ViewEncapsulation.None,
  providers: [
    {
      multi: true,
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => TextMeshProTextEditorComponent),
    },
  ],
})
export class TextMeshProTextEditorComponent implements OnInit, OnChanges, AfterViewInit, ControlValueAccessor {

  onChange = (_: any) => {};
  onTouched = () => {};

  @Input() readOnly = false;
  @Input() value;
  @Input() placeholder: string;
  @Input() status: string;

  editorContent: DeltaStatic;
  disabled: boolean;

  quillModulesConfig: QuillModules = {
    'toolbar': [['bold', 'italic', 'strike', 'underline']], // , [{ 'color': [] }]],
  };

  valueConverter = new TextMeshProRichTextToQuillDeltaConverter();

  @ViewChild('editor', {static: false}) editor: QuillEditorComponent;

  constructor() {
  }

  ngOnInit() {}

  ngAfterViewInit(): void {

    this.editor.registerOnChange(v => {
      if (this.onChange) {
        this.onChange(this.valueConverter.convertToTextMeshProRichText(v));
      }
    });
    this.editor.registerOnTouched(() => {
      if (this.onTouched) {
        this.onTouched();
      }
    });

    if (this.editorContent) {
      this.editor.writeValue( this.editorContent);
    }

    this.editor.setDisabledState(this.disabled);
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes['value']) {
      this.editor.content = this.writeValue(this.value);
    }
  }

  writeValue(obj: any): void {

    this.editorContent = this.valueConverter.convertToQuillDelta(obj);
    if (this.editor) {
      this.editor.writeValue(this.editorContent);
    }
  }

  registerOnChange(fn: (_: any) => {}): void {
    this.onChange = fn;
  }

  registerOnTouched(fn: any): void {
    this.onTouched = fn;
  }

  setDisabledState?(isDisabled: boolean): void {

    this.disabled = isDisabled;

    if (this.editor) {
      this.editor.setDisabledState(isDisabled);
    }
  }
}
