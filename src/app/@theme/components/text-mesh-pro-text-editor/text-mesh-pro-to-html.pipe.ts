import {Pipe, PipeTransform, SecurityContext} from '@angular/core';
import {TextMeshProRichTextToQuillDeltaConverter} from './text-mesh-pro-rich-text-to-quill-delta-converter';
import {DomSanitizer} from '@angular/platform-browser';

const QuillDeltaToHtmlConverter = require('quill-delta-to-html').QuillDeltaToHtmlConverter;

@Pipe({
  name: 'textMeshProToHtml',
})
export class TextMeshProToHtmlPipe implements PipeTransform {


  private valueConverter = new TextMeshProRichTextToQuillDeltaConverter();

  constructor(private sanitizer: DomSanitizer) {}

  transform(value: any, args?: any): any {

    if (!value) {
      return value;
    }
    const delta = this.valueConverter.convertToQuillDelta(value);
    const deltaToHtmlConverter = new QuillDeltaToHtmlConverter(delta.ops, {});
    const html = deltaToHtmlConverter.convert();
    return this.sanitizer.sanitize(SecurityContext.HTML, html);
  }

}
