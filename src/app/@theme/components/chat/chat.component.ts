import { Component, OnInit, Input, EventEmitter, Output, ElementRef, ViewChild } from '@angular/core';
import * as moment from 'moment';

export interface DisplayMessage {

  ref: string;
  message: string;
  isReply: boolean;
  senderId: string;
  senderName: string;
  date: Date;

}

@Component({
  selector: 'gl-chat',
  templateUrl: './chat.component.html',
  styleUrls: ['./chat.component.scss'],
})
export class ChatComponent implements OnInit {

  @Input() title: string;
  @Input() messages: DisplayMessage[] = [];
  @Input() showLoadMore: boolean = true;
  @Input() showForm: boolean = true;
  @Input() showCloseButton: boolean = false;

  @Output() loadMore: EventEmitter<any> = new EventEmitter<any>();
  @Output() sendMessage = new EventEmitter<any>();
  @Output() closeButtonClicked = new EventEmitter<any>();

  @ViewChild('chatHistory', {static: false}) private chatHistoryContainer: ElementRef;

  newMessageText: string = '';

  @Input() selfColor: string = '#222222';

  senderColorSaturation = 0.20;
  senderColorValue = 1;

  constructor() { }

  ngOnInit() {
  }

  onSendMessageClicked() {
    if (!this.newMessageText) {
      return;
    }
    this.sendMessage.emit({
      message: this.newMessageText,
    });
    this.newMessageText = '';
  }

  onLoadMoreClicked(): void {
    this.loadMore.emit();
  }

  onEnter(event) {
    if (event.shiftKey) {
      return;
    } else {
      this.onSendMessageClicked();
    }
  }

  scrollToBottom(): void {
    // console.log("Scrolling down")
    setTimeout(() => this._scrollToBottom(), 0);

  }

  private _scrollToBottom(): void {
    try {
      this.chatHistoryContainer.nativeElement.scrollTop = this.chatHistoryContainer.nativeElement.scrollHeight;
    } catch (err) {
      // We don't really care if the scrolling down fails
      console.log(err);
    }
  }

  messageColor(message: DisplayMessage): string {

    if (!message.isReply) {
      return this.selfColor;
    }

    let hue = (Math.abs(this.hashCode(message.senderId)) % 360) / 360.0;
    // console.log(hue)
    return ChatComponent.RGBtoHEX(ChatComponent.HSVtoRGB(hue, this.senderColorSaturation, this.senderColorValue));
  }

  private hashCode(str: string): number {
    let hash = 0, i, chr;
    if (str.length === 0) return hash;
    for (i = 0; i < str.length; i++) {
      chr   = str.charCodeAt(i);
      hash  = ((hash << 5) - hash) + chr;
      hash |= 0; // Convert to 32bit integer
    }
    return hash;
  }

  showHeader(messageIndex: number) {
    if (messageIndex === 0) {
      return true;
    }
    let previousMessage = this.messages[messageIndex - 1];
    let message = this.messages[messageIndex];
    return previousMessage && message
      && ((previousMessage.senderId !== message.senderId)
        || ((moment(message.date).subtract(1, 'hour').isAfter(moment(previousMessage.date)))));
  }

  static HSVtoRGB(h: number, s: number, v: number): { r: number, g: number, b: number } {

    let r, g, b, i, f, p, q, t;

    i = Math.floor(h * 6);
    f = h * 6 - i;
    p = v * (1 - s);
    q = v * (1 - f * s);
    t = v * (1 - (1 - f) * s);
    switch (i % 6) {
        case 0: r = v, g = t, b = p; break;
        case 1: r = q, g = v, b = p; break;
        case 2: r = p, g = v, b = t; break;
        case 3: r = p, g = q, b = v; break;
        case 4: r = t, g = p, b = v; break;
        case 5: r = v, g = p, b = q; break;
    }
    return {
        r: Math.round(r * 255),
        g: Math.round(g * 255),
        b: Math.round(b * 255),
    };
  }

  static RGBtoHEX(rgb: { r: number, g: number, b: number }): string {
    return '#' + ChatComponent.hex(rgb.r) + ChatComponent.hex(rgb.g) + ChatComponent.hex(rgb.b);
  }

  static hex(n: number): string {
    let hex = Number(n).toString(16);
    if (hex.length < 2) {
         hex = "0" + hex;
    }
    return hex;
  }

  onCloseButtonClicked() {
    this.closeButtonClicked.emit();
  }
}
