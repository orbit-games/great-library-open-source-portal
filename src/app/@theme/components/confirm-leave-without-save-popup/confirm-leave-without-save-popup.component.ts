import { Component, OnInit } from '@angular/core';
import {NbDialogRef} from '@nebular/theme';
import {Subject} from 'rxjs';

@Component({
  selector: 'ngx-confirm-leave-without-save-popup',
  templateUrl: './confirm-leave-without-save-popup.component.html',
  styleUrls: ['./confirm-leave-without-save-popup.component.scss'],
})
export class ConfirmLeaveWithoutSavePopupComponent implements OnInit {

  private confirm: boolean = false;

  confirmed = new Subject<boolean>();

  constructor(public dialogRef: NbDialogRef<any>) {}

  ngOnInit() {

    this.dialogRef.onClose.subscribe(c => {
      this.confirmed.next(this.confirm);
    });
  }

  onConfirmLeaveWithoutSaving() {
    this.confirm = true;
    this.dialogRef.close();
  }
}
