import {Injectable} from '@angular/core';
import {CanDeactivate} from '@angular/router';
import {Observable} from 'rxjs';
import {NbDialogRef, NbDialogService} from '@nebular/theme';
import {ConfirmLeaveWithoutSavePopupComponent} from '../components/confirm-leave-without-save-popup/confirm-leave-without-save-popup.component';


// Based on: https://stackoverflow.com/questions/35922071/warn-user-of-unsaved-changes-before-leaving-page

export interface ComponentCanDeactivate {
  canDeactivate: () => boolean | Observable<boolean>;
}

@Injectable()
export class PendingChangesGuard implements CanDeactivate<ComponentCanDeactivate> {

  static confirmDialogRef: NbDialogRef<ConfirmLeaveWithoutSavePopupComponent>;

  constructor(private dialogService: NbDialogService) {}

  canDeactivate(component: ComponentCanDeactivate): Observable<boolean> |  boolean {

    if ((component.canDeactivate == null) || component.canDeactivate()) {
      return true;
    } else {
      // Sometimes, the guard is called twice, so in that case we check if there is already a "confirm changes" dialog open.
      // If there isn't a dialog already open, we open it:
      if (PendingChangesGuard.confirmDialogRef == null) {
        PendingChangesGuard.confirmDialogRef = this.dialogService.open(ConfirmLeaveWithoutSavePopupComponent, {});

        PendingChangesGuard.confirmDialogRef.onClose.subscribe(s => {
          PendingChangesGuard.confirmDialogRef = null;
        });
      }

      const confirmDialogComponent = PendingChangesGuard.confirmDialogRef.componentRef.instance;
      return confirmDialogComponent.confirmed;
    }
  }
}
