import { ModuleWithProviders, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import {
  NbActionsModule,
  NbButtonModule,
  NbCardModule,
  NbCheckboxModule,
  NbContextMenuModule,
  NbDatepickerModule,
  NbDialogModule,
  NbInputModule,
  NbLayoutModule,
  NbListModule,
  NbMenuModule,
  NbPopoverModule,
  NbProgressBarModule,
  NbRouteTabsetModule,
  NbSearchModule,
  NbSidebarModule,
  NbTabsetModule,
  NbThemeModule,
  NbToastrModule,
  NbTooltipModule,
  NbUserModule,
} from '@nebular/theme';

import { NbSecurityModule } from '@nebular/security';

import { FooterComponent, HeaderComponent, SearchInputComponent, TinyMCEComponent, } from './components';
import {
  CapitalizePipe,
  DisplayDatePipe,
  DisplayDateTimePipe,
  NumberWithCommasPipe,
  PluralPipe,
  RoundPipe,
  TimingPipe,
} from './pipes';
import { GlMainLayoutComponent, } from './layouts';
import { DEFAULT_THEME } from './styles/theme.default';
import { RouterModule } from '@angular/router';
import { GlErrorHandlerService } from './error-handler.service';
import { NewlineToBrPipe } from './pipes/newline-to-br.pipe';
import { ShowNamePipe } from './pipes/show-name.pipe';
import { ChatComponent } from './components/chat/chat.component';
import { SwitcherComponent } from './components/switcher/switcher.component';
import { TextMeshProTextEditorComponent } from './components/text-mesh-pro-text-editor/text-mesh-pro-text-editor.component';
import { QuillModule } from 'ngx-quill';
import { TextMeshProToHtmlPipe } from './components/text-mesh-pro-text-editor/text-mesh-pro-to-html.pipe';
import { ClickToEditComponent } from './components/click-to-edit/click-to-edit.component';
import { ConfirmLeaveWithoutSavePopupComponent } from './components/confirm-leave-without-save-popup/confirm-leave-without-save-popup.component';
import { PendingChangesGuard } from './guards/pending-changes.guard';
import { ConfirmDeleteDialogComponent } from './components/confirm-delete-dialog/confirm-delete-dialog.component';
import { FileIconDirective } from './directives/file-icon.directive';
import { StatusIndicatorDirective } from './directives/status-indicator.directive';
import { GlSelectComponent } from './components/gl-select/gl-select.component';
import { Ng2CompleterModule } from 'ng2-completer';
import { DisplayTimePipe } from './pipes/display-time.pipe';
import { DateTimePickerComponent } from './components/date-time-picker/date-time-picker.component';
import { NbMomentDateModule } from '@nebular/moment';
import { SelectableItemListComponent } from './components/selectable-item-list/selectable-item-list.component';
import { DragDropModule } from '@angular/cdk/drag-drop';

const BASE_MODULES = [CommonModule, FormsModule, ReactiveFormsModule];

const NB_MODULES = [
  NbCardModule,
  NbLayoutModule,
  NbTabsetModule,
  NbRouteTabsetModule,
  NbMenuModule,
  NbUserModule,
  NbActionsModule,
  NbSearchModule,
  NbSidebarModule,
  NbCheckboxModule,
  NbPopoverModule,
  NbContextMenuModule,
  NbTooltipModule,
  NbButtonModule,
  NbSecurityModule, // *nbIsGranted directive,
  NbInputModule,
  NbProgressBarModule,
  NbDatepickerModule,
  NbMomentDateModule,
  NbListModule,
  NgbModule,
  QuillModule,
  Ng2CompleterModule,
  DragDropModule,
];

const COMPONENTS = [
  HeaderComponent,
  FooterComponent,
  SearchInputComponent,
  TinyMCEComponent,
  SwitcherComponent,
  TextMeshProTextEditorComponent,
  ConfirmLeaveWithoutSavePopupComponent,
  ConfirmDeleteDialogComponent,

  GlMainLayoutComponent,
  ChatComponent,
  ClickToEditComponent,
  GlSelectComponent,
  DateTimePickerComponent,
];

const DIRECTIVES = [
  FileIconDirective,
  StatusIndicatorDirective,
];

const ENTRY_COMPONENTS = [
  GlMainLayoutComponent,
  ConfirmLeaveWithoutSavePopupComponent,
  ConfirmDeleteDialogComponent,
];

const PIPES = [
  CapitalizePipe,
  PluralPipe,
  RoundPipe,
  TimingPipe,
  NumberWithCommasPipe,
  DisplayDatePipe,
  DisplayTimePipe,
  DisplayDateTimePipe,
  NewlineToBrPipe,
  ShowNamePipe,
  TextMeshProToHtmlPipe,
];

const NB_THEME_PROVIDERS = [
  ...NbThemeModule.forRoot(
    {
      name: 'default',
    },
    [DEFAULT_THEME],
  ).providers,
  ...NbSidebarModule.forRoot().providers,
  ...NbMenuModule.forRoot().providers,
  PendingChangesGuard,
  GlErrorHandlerService,
];

@NgModule({
  imports: [...BASE_MODULES, ...NB_MODULES, NbToastrModule.forRoot(), NbDialogModule.forRoot(), RouterModule],
  exports: [...BASE_MODULES, ...NB_MODULES, ...COMPONENTS, ...DIRECTIVES, ...PIPES, SelectableItemListComponent],
  declarations: [...COMPONENTS, ...DIRECTIVES, ...PIPES, SelectableItemListComponent],
  entryComponents: [...ENTRY_COMPONENTS],
})
export class ThemeModule {
  static forRoot(): ModuleWithProviders {
    return <ModuleWithProviders>{
      ngModule: ThemeModule,
      providers: [...NB_THEME_PROVIDERS],
    };
  }
}
