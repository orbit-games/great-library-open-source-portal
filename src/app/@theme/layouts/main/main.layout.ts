import { Component, OnDestroy, OnInit } from '@angular/core';
import { delay, withLatestFrom, takeWhile } from 'rxjs/operators';
import {
  NbMediaBreakpoint,
  NbMediaBreakpointsService,
  NbMenuItem,
  NbMenuService,
  NbSidebarService,
  NbThemeService,
} from '@nebular/theme';
import { Course } from '../../../@core/api';
import { SessionService } from '../../../@core/session.service';
import { Subscription } from 'rxjs';

// TODO: move layouts into the framework
@Component({
  selector: 'gl-main-layout',
  styleUrls: ['./main.layout.scss'],
  template: `
    <nb-layout [center]="layout.id === 'center-column'" windowMode>
      <nb-layout-header fixed>
        <gl-header></gl-header>
      </nb-layout-header>

      <nb-sidebar class="menu-sidebar"
                   tag="menu-sidebar"
                   responsive
                   [compactedBreakpoints]	= "[]"
                   [collapsedBreakpoints]	= "['xs', 'is', 'sm', 'md', 'lg']"
                   [end]="sidebar.id === 'end'">
        <nb-sidebar-header>
          <a [routerLink]="'/pages/courses'" class="btn btn-hero-success main-btn">
            <i class="fa fa-graduation-cap"></i><br><span>Select<br>Course</span>
          </a>
        </nb-sidebar-header>
        <h5 class="text-center" *ngIf="selectedCourse">{{selectedCourse.name}}</h5>
        <ng-content *ngIf="selectedCourse" select="nb-menu"></ng-content>
      </nb-sidebar>

      <nb-layout-column class="main-content">
        <ng-content select="router-outlet"></ng-content>
      </nb-layout-column>

    </nb-layout>
  `,
})
export class GlMainLayoutComponent implements OnDestroy, OnInit {

  layout: any = {};
  sidebar: any = {};

  private alive = true;

  currentTheme: string;

  selectedCourse: Course;
  selectedCourseChangedSubscription: Subscription;

  constructor(
              protected sessionService: SessionService,
              protected menuService: NbMenuService,
              protected themeService: NbThemeService,
              protected bpService: NbMediaBreakpointsService,
              protected sidebarService: NbSidebarService) {

    const isBp = this.bpService.getByName('is');
    this.menuService.onItemSelect()
      .pipe(
        takeWhile(() => this.alive),
        withLatestFrom(this.themeService.onMediaQueryChange()),
        delay(20),
      )
      .subscribe(([item, [bpFrom, bpTo]]: [any, [NbMediaBreakpoint, NbMediaBreakpoint]]) => {

        if (bpTo.width <= isBp.width) {
          this.sidebarService.toggle(false, 'menu-sidebar');
        }
      });

    this.themeService.getJsTheme()
      .pipe(takeWhile(() => this.alive))
      .subscribe(theme => {
        this.currentTheme = theme.name;
    });
  }

  ngOnInit() {
    this.sessionService.getSelectedCourse().subscribe(c => this.selectedCourse = c);
    this.selectedCourseChangedSubscription = this.sessionService.selectedCourseChanged.subscribe(c => this.selectedCourse = c);
  }


  ngOnDestroy() {
    this.selectedCourseChangedSubscription.unsubscribe();
    this.alive = false;
  }
}
