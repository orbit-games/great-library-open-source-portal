import {AbstractControl} from '@angular/forms';

export class FormControlUtil {

  /**
   * Determines the control status based on the current state of the control
   * @param controlReference A reference to the control
   * @param requireTouched Indicates if the control should have been touched before indicating any status (errors)
   * @param parent The parent of the control. Needed if the control reference is a string
   */
  public static controlStatus(controlReference: any, parent: AbstractControl = null, requireTouched: boolean = true): string {

    let control: AbstractControl;
    if (controlReference instanceof AbstractControl) {
      control = controlReference;
    } else {
      control = parent.get(controlReference);
    }

    if (!control) {
      return 'warning';
    }

    return (!requireTouched || control.touched || control.dirty) && control.invalid ? 'danger' : '';
  }
}
