import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'newlineToBr'
})
export class NewlineToBrPipe implements PipeTransform {

  transform(value: any, args?: any): any {
    return value.replace(/\n/g, "<br />");
  }

}
