import { Pipe, PipeTransform } from '@angular/core';
import { FormatHelper } from './format-helper';

@Pipe({
  name: 'displayDate',
})
export class DisplayDatePipe implements PipeTransform {

  transform(value: any, args?: any): any {
    return FormatHelper.toHumanReadableDate(value);
  }

}
