import { Pipe, PipeTransform } from '@angular/core';
import { FormatHelper } from './format-helper';

@Pipe({
  name: 'displayDateTime',
})
export class DisplayDateTimePipe implements PipeTransform {

  transform(value: any, args?: any): any {
    return FormatHelper.toHumanReadableDateTime(value);
  }

}
