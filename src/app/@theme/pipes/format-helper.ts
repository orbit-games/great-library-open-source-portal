import * as moment from 'moment';
import { environment } from '../../../environments/environment';

export class FormatHelper {

  /**
   * Formats an ISO-8601 date to a human-readable format
   * @param date The date to format
   */
  public static toHumanReadableDate(date: any) {
    if (!date) {
      return null;
    }
    return moment(date).format(environment.displayDateFormat);
  }

  public static toHumanReadableDateTime(date: any) {
    if (!date) {
      return null;
    }
    return moment(date).format(environment.displayDateTimeFormat);
  }

  public static toHumanReadableTime(date: any) {
    if (!date) {
      return null;
    }

    return moment(date).format(environment.displayTimeFormat);
  }

  public static fromHumanReadableDate(humanReadableDate: string): Date {
    return moment(humanReadableDate, environment.displayDateFormat).toDate();
  }
}
