import { Pipe, PipeTransform } from '@angular/core';
import { CourseProgressSummary } from '../../@core/api';
import { SessionService, NameType } from '../../@core/session.service';

@Pipe({
  name: 'showName',
  pure: false,
})
export class ShowNamePipe implements PipeTransform {

  constructor(private sessionService: SessionService) {
  }

  transform(value: CourseProgressSummary, args?: any): any {

    if (!value) {
      return args ? args : null;
    }

    if (this.sessionService.nameType === NameType.DISPLAY_NAME) {
      return value.displayName || args;
    } else {
      return value.fullName || args;
    }
  }
}
