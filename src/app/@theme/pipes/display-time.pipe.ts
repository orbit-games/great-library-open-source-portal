import { Pipe, PipeTransform } from '@angular/core';
import { FormatHelper } from './format-helper';

@Pipe({
  name: 'displayTime',
})
export class DisplayTimePipe implements PipeTransform {

  transform(value: any, args?: any): any {
    return FormatHelper.toHumanReadableTime(value);
  }

}
