import { Injectable } from '@angular/core';
import { NbToastrService } from '@nebular/theme';

import { ErrorDetails } from '../@core/api';

@Injectable()
export class GlErrorHandlerService {

  constructor(private toasterService: NbToastrService) {}

  public handle(error: ErrorDetails) {
    // Rare case where we actually want to log the error plainly
    // tslint:disable-next-line:no-console
    console.log(error);
    let errorText = 'An unknown error occured';
    if (error && error.message) {
        errorText = `An error occured: ${error.message}`;
    }
    this.toasterService.danger(errorText, 'Error', {
        duration: 10000,
    });
  }

}
