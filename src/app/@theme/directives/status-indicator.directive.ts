import {Directive, ElementRef, HostBinding, HostListener, Input, Renderer2} from '@angular/core';

// TODO Fix me!

@Directive({
  selector: '[ngxStatusIndicator]',
})
export class StatusIndicatorDirective {

  static readonly STATUS_PRIMARY = 'primary';
  static readonly STATUS_INFO = 'info';
  static readonly STATUS_SUCCESS = 'success';
  static readonly STATUS_WARNING = 'warning';
  static readonly STATUS_DANGER = 'danger';

  @Input() status: string;

  constructor(private renderer: Renderer2,
              private elementRef: ElementRef) {
    renderer.addClass(elementRef.nativeElement, 'status-indicator');
  }

  @HostBinding('class.input-primary')
  get primary() {
    return this.status === StatusIndicatorDirective.STATUS_PRIMARY;
  }

  @HostBinding('class.input-info')
  get info() {
    return this.status === StatusIndicatorDirective.STATUS_INFO;
  }

  @HostBinding('class.input-success')
  get success() {
    return this.status === StatusIndicatorDirective.STATUS_SUCCESS;
  }

  @HostBinding('class.input-warning')
  get warning() {
    return this.status === StatusIndicatorDirective.STATUS_WARNING;
  }

  @HostBinding('class.input-danger')
  get danger() {
    return this.status === StatusIndicatorDirective.STATUS_DANGER;
  }
}
