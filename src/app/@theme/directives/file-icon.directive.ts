import {Directive, HostBinding, Input, OnChanges, SimpleChanges} from '@angular/core';

@Directive({
  selector: 'i[ngxFileIcon]',
})
export class FileIconDirective implements OnChanges {

  defaultIcon = 'fa fa-file';

  @Input() fileName: string;

  @HostBinding('class') iconClass = this.defaultIcon;

  filenameToIcon: {
    matcher: RegExp,
    icon: string,
  }[] = [
    { matcher: /^.*\.(7z|7zip|gzip|zip|rar|tar|gz|pzip|saz|tgz)$/, icon: 'fa fa-file-archive'},
    { matcher: /^.*\.(mp3|aac|aiff|au|flac|m4a|mpc|mp[0-9]+|mpp|ogg|oga|opus|ra|rm|wav|wma|snd|mid|cda)$/, icon: 'fa fa-file-audio'},
    { matcher: /^.*\.(cs|py|java|js|html|s?css|sass|ts|xml|json|php|script|c|cpp|h|hpp|kt|kts|less|lisp|lua|matlab|conf|pas(cal)?|perl|pl|pm|r|rs|rust|scala|sql|swagger|vue|vuex|yml|yaml)$/,
      icon: 'fa fa-file-code'},
    { matcher: /^.*\.(xls|xlsx|xlm|xlt|xlsb|odx|csv)/, icon: 'fa fa-file-excel'},
    { matcher: /^.*\.(gif|png|jpg|jpeg|bmp|svg)$/, icon: 'fa fa-file-image'},
    { matcher: /^.*\.(pdf)$/, icon: 'fa fa-file-pdf'},
    { matcher: /^.*\.(ppt|pptx|pps|ppsx)$/, icon: 'fa fa-file-powerpoint'},
    { matcher: /^.*\.(doc|docx|dotm|dotx|wri|docxml|odt|odtx)$/, icon: 'fa fa-file-word'},
    { matcher: /^.*\.(avi|mp4|mpg|mpeg|mov|mkv|flv|h264|webm|wmv|ogv|3gp|m4v|3gpp|ogm|dvd|divx)$/, icon: 'far fa-file-video'},

  ];


  constructor() { }

  ngOnChanges(changes: SimpleChanges): void {

    if (changes.fileName) {
      this.updateIconClass();
    }
  }

  private updateIconClass() {

    this.iconClass = this.findIconClass(this.fileName);

  }

  private findIconClass(fileName: string): string {

    if (fileName == null || fileName === '') {
      return this.defaultIcon;
    }

    const matchedIcon = this.filenameToIcon.find(f2i => f2i.matcher.test(fileName));
    if (matchedIcon) {
      return matchedIcon.icon;
    } else {
      return this.defaultIcon;
    }
  }



}
