import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { NbAuthJWTToken, NbAuthModule } from '@nebular/auth';
import {
  NbAlertModule,
  NbButtonModule,
  NbCardModule,
  NbCheckboxModule,
  NbInputModule,
  NbLayoutModule,
} from '@nebular/theme';

import { environment } from '../../environments/environment.prod';
import { GlAuthGuard } from './gl-auth-guard.service';
import { GlLoginComponent } from './gl-login-page/gl-login.component';
import { GlPasswordAuthStrategy } from './gl-password-strategy';
import { RouterModule } from '@angular/router';
import { GlRequestPasswordComponent } from './gl-request-password/gl-request-password.component';
import { GlResetPasswordComponent } from './gl-reset-password/gl-reset-password.component';
import { GlRegisterComponent } from './gl-register/gl-register.component';

export const AUTH_STRATEGY_NAME = 'gl-api-auth';


@NgModule({
  imports: [
    CommonModule,
    NbLayoutModule,
    NbCardModule,
    NbCheckboxModule,
    NbAlertModule,
    NbInputModule,
    NbButtonModule,
    FormsModule,
    HttpClientModule,
    RouterModule,
  ],
  exports: [
    NbAuthModule,
    GlLoginComponent,
    GlRequestPasswordComponent,
  ],
  providers: [
    GlPasswordAuthStrategy,
    GlAuthGuard,
    ...NbAuthModule.forRoot({
      strategies: [
        GlPasswordAuthStrategy.setup({
          name: AUTH_STRATEGY_NAME,
          baseEndpoint: '',
          register:  {
            endpoint: environment.apiBaseUri + '/user/register',
          },
          login: {
            endpoint: environment.apiBaseUri + '/user/login',
          },
          requestPass: {
            endpoint: environment.apiBaseUri + '/user/forgot-password',
          },
          resetPass: {
            endpoint: environment.apiBaseUri + '/user/reset-password',
            method: 'post',
          },
          logout: {
            method: null,
            endpoint: '',
            redirect: {
              success: '/',
              failure: '/',
            },
          },
          token: {
            key: 'sessionToken',
            class: NbAuthJWTToken,
          },
        }),
      ],
      forms: {
        register: {
          redirectDelay: 0,
          strategy: AUTH_STRATEGY_NAME,
        },
        login: {
          redirectDelay: 0,
          strategy: AUTH_STRATEGY_NAME,
        },
        logout: {
          redirectDelay: 0,
          strategy: AUTH_STRATEGY_NAME,
        },
        requestPassword: {
          redirectDelay: 0,
          strategy: AUTH_STRATEGY_NAME,
        },
        resetPassword: {
          redirectDelay: 0,
          strategy: AUTH_STRATEGY_NAME,
        },
      },
    }).providers,
  ],
  declarations: [
    GlLoginComponent,
    GlRequestPasswordComponent,
    GlResetPasswordComponent,
    GlRegisterComponent,
  ],
})
export class GlAuthModule {}
