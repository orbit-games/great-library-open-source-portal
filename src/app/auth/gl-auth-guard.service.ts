import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { NbAuthService, NbAuthJWTToken, NbTokenService } from '@nebular/auth';
import { tap, map } from 'rxjs/operators';
import { Observable, Observer } from 'rxjs';

@Injectable()
export class GlAuthGuard implements CanActivate {

    constructor(private tokenService: NbTokenService, private router: Router) {
    }
  
    canActivate(): Observable<boolean> {
        return this.tokenService.get()
            .pipe(
                tap((token: NbAuthJWTToken) => {
                    if (!token || !token.isValid()) {
                        this.router.navigate(['/auth/login']);
                    }
                }),
                map(token => token && token.isValid()),
            );
    }
}
