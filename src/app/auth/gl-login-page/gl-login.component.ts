/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
import { ChangeDetectionStrategy, Component } from '@angular/core';
import { NbLoginComponent } from '@nebular/auth';

@Component({
  selector: 'gl-login',
  templateUrl: './gl-login.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class GlLoginComponent extends NbLoginComponent {
}
