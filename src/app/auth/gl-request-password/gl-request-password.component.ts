import { ChangeDetectionStrategy, Component } from '@angular/core';
import { NbRequestPasswordComponent } from '@nebular/auth';

@Component({
  selector: 'gl-request-password-page',
  styleUrls: ['./gl-request-password.component.scss'],
  templateUrl: './gl-request-password.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class GlRequestPasswordComponent extends NbRequestPasswordComponent {}
