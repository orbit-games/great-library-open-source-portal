import { Component, OnInit, Inject, ChangeDetectorRef } from '@angular/core';
import { NbRegisterComponent, NB_AUTH_OPTIONS, NbAuthService } from '@nebular/auth';
import { userInfo } from 'os';
import { UserAgreementService } from '../../@core/data/user-agreement.service';
import { Router } from '@angular/router';
import { UserAgreement } from '../../@core/api';

@Component({
  selector: 'gl-register',
  templateUrl: './gl-register.component.html',
  styleUrls: ['./gl-register.component.scss'],
})
export class GlRegisterComponent extends NbRegisterComponent implements OnInit {

  // Only needed so it can be stored/modeled somewhere
  repeatPassword: string;
  termsAccepted: any;

  latestAgreement: UserAgreement;

  constructor(
      protected service: NbAuthService,
      @Inject(NB_AUTH_OPTIONS) protected options = {},
      protected cd: ChangeDetectorRef,
      protected router: Router,
      private userAgreementService: UserAgreementService) {

    super(service, options, cd, router);
  }

  ngOnInit(): void {
    this.userAgreementService.getLatestUserAgreement()
      .then(ua => {
        this.latestAgreement = ua;
      });
  }

  onTermsAgreementChanged(values: any) {
    if (values.target.checked) {
      this.user.signedUserAgreementRef = this.latestAgreement.ref;
    } else {
      this.user.signedUserAgreementRef = null;
    }
  }

  onEmailChanged(email: string) {
    this.user.username = email;
  }



}
