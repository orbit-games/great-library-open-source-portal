import { Component, OnInit } from '@angular/core';
import { NbResetPasswordComponent } from '@nebular/auth';

@Component({
  selector: 'gl-reset-password',
  templateUrl: './gl-reset-password.component.html',
  styleUrls: ['./gl-reset-password.component.scss']
})
export class GlResetPasswordComponent extends NbResetPasswordComponent implements OnInit {

  confirmPass: string;

  ngOnInit() {
  }

}
